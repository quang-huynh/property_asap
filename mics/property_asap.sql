-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2017 at 03:15 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `property_asap`
--

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `slug`, `name`, `type`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'australia', 'Australia', 5, 0, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(2, 'victoria', 'Victoria', 4, 1, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(3, 'melbourne', 'Melbourne', 3, 2, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(4, 'collins-street', 'Collins Street', 2, 3, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(5, '2', '2', 1, 4, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(6, 'south-australia', 'South Australia', 4, 1, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(7, 'millicent', 'Millicent', 3, 6, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(8, 'princes-highway', 'Princes Highway', 2, 7, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(9, '30346', '30346', 1, 8, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(10, 'australian-capital-territory', 'Australian Capital Territory', 4, 1, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(11, 'canberra', 'Canberra', 3, 10, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(12, 'n-a', '', 2, 11, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(13, 'n-a', '', 1, 12, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(14, 'south-wharf', 'South Wharf', 3, 2, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(15, 'convention-centre-place', 'Convention Centre Place', 2, 14, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(16, '2', '2', 1, 15, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(17, '3', '3', 1, 4, '2017-06-12 10:01:05', '2017-06-12 10:01:05'),
(18, 'new-south-wales', 'New South Wales', 4, 1, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(19, 'narooma', 'Narooma', 3, 18, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(20, 'princes-highway', 'Princes Highway', 2, 19, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(21, '2', '2', 1, 20, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(22, 'western-australia', 'Western Australia', 4, 1, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(23, 'kingsley', 'Kingsley', 3, 22, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(24, 'creaney-drive', 'Creaney Drive', 2, 23, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(25, 'n-a', '', 1, 24, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(26, 'bolwarra', 'Bolwarra', 3, 2, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(27, 'princes-highway', 'Princes Highway', 2, 26, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(28, '104', '104', 1, 27, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(29, 'woolloomooloo', 'Woolloomooloo', 3, 18, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(30, 'crown-street', 'Crown Street', 2, 29, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(31, '2', '2', 1, 30, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(32, 'n-a', '', 5, 0, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(33, 'n-a', '', 4, 32, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(34, 'n-a', '', 3, 33, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(35, 'n-a', '', 2, 34, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(36, 'n-a', '', 1, 35, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(37, 'queensland', 'Queensland', 4, 1, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(38, 'park-ridge-south', 'Park Ridge South', 3, 37, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(39, 'mount-lindesay-highway', 'Mount Lindesay Highway', 2, 38, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(40, '4111', '4111', 1, 39, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(41, 'alexandria', 'Alexandria', 3, 18, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(42, 'n-a', '', 2, 41, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(43, 'n-a', '', 1, 42, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(44, 'richmond', 'Richmond', 3, 2, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(45, 'victoria-street', 'Victoria Street', 2, 44, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(46, 'n-a', '', 1, 45, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(47, 'n-a', '', 2, 3, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(48, 'n-a', '', 1, 47, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(49, 'swanston-street', 'Swanston Street', 2, 3, '2017-06-25 02:17:14', '2017-06-25 02:17:14'),
(50, '25', '25', 1, 49, '2017-06-25 02:17:14', '2017-06-25 02:17:14'),
(51, 'bourke-street', 'Bourke Street', 2, 3, '2017-07-05 11:32:38', '2017-07-05 11:32:38'),
(52, '45-49', '45-49', 1, 51, '2017-07-05 11:32:38', '2017-07-05 11:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('image','video','document','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `slug`, `name`, `size`, `extension`, `type`, `post_id`, `is_featured`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_5955642eb7e29.jpg', '19_5955642eb7e29.jpg', '74934', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:50', '2017-06-29 13:33:50'),
(2, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_595564337e606.jpg', '19_595564337e606.jpg', '89720', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:55', '2017-06-29 13:33:55'),
(3, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_595564371f79e.jpg', '19_595564371f79e.jpg', '70399', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:59', '2017-06-29 13:33:59'),
(4, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_5955643ba7935.jpg', '19_5955643ba7935.jpg', '58090', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:34:03', '2017-06-29 13:34:03'),
(5, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f900cc29.jpg', '18_595d2f900cc29.jpg', '848626', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:28', '2017-07-05 11:27:28'),
(6, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f955af10.jpg', '18_595d2f955af10.jpg', '131520', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:33', '2017-07-05 11:27:33'),
(7, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f9c144e3.jpg', '18_595d2f9c144e3.jpg', '423544', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:40', '2017-07-05 11:27:40'),
(8, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960658505add.jpg', '20_5960658505add.jpg', '160258', 'jpg', 'image', 20, 1, 2, '2017-07-07 21:54:29', '2017-07-08 08:15:03'),
(11, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960659242db9.jpg', '20_5960659242db9.jpg', '848626', 'jpg', 'image', 20, 0, 2, '2017-07-07 21:54:42', '2017-07-08 08:15:03'),
(16, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960f576392bb.jpg', '20_5960f576392bb.jpg', '52392', 'jpg', 'image', 20, 0, 2, '2017-07-08 08:08:38', '2017-07-08 08:15:03'),
(17, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960f74bbda1b.jpg', '20_5960f74bbda1b.jpg', '91051', 'jpg', 'image', 20, 0, 2, '2017-07-08 08:16:27', '2017-07-08 08:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_04_03_160915_create_role_table', 1),
('2017_04_03_162222_create_role_user_table', 1),
('2017_04_03_162313_create_permission_table', 1),
('2017_04_03_162333_create_permission_role_table', 1),
('2017_04_03_162850_create_setting_table', 1),
('2017_04_03_163119_create_report_table', 1),
('2017_04_03_163144_create_notification_table', 1),
('2017_04_03_163159_create_notification_user_table', 1),
('2017_04_03_163231_create_setting_user_table', 1),
('2017_04_03_163527_create_location_table', 1),
('2017_04_04_155744_create_media_table', 1),
('2017_04_04_171955_create_tag_table', 1),
('2017_04_04_172025_create_post_table', 1),
('2017_04_04_172151_create_property_table', 1),
('2017_04_04_173827_create_post_setting_table', 1),
('2017_04_05_120048_create_post_tag_table', 1),
('2017_04_05_120401_create_post_property_table', 1),
('2017_04_06_052132_create_log_table', 1),
('2017_05_27_154417_create_wishlist_table', 1),
('2017_05_29_160145_create_post_location_table', 1),
('2017_06_06_042805_add_column_property_text_in_property_post_table', 2),
('2017_06_06_044301_add_column_type_in_post_table', 3),
('2017_06_06_044301_add_column_type_and_price_in_post_table', 4),
('2017_06_18_032117_create_published_date_column_in_post_table', 5),
('2017_06_25_130300_create_profile_picture_for_user', 6),
('2017_06_25_131539_create_column_activation_code_for_user', 7),
('2017_06_24_054637_add_cloumn_to_user', 8),
('2017_06_25_020517_add_picture_to_user', 9),
('2017_06_25_025533_add_active_code_to_user', 9);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_notification` enum('system','theme','user','post','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_message` enum('success','warning','information','danger','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `for_role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `lifetime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_user`
--

CREATE TABLE IF NOT EXISTS `notification_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_message` enum('success','warning','information','danger','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `for_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `lifetime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE IF NOT EXISTS `password_reset` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_reset_email_index` (`email`),
  KEY `password_reset_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`email`, `token`, `created_at`) VALUES
('test123@test.com', 'ajNNTAmQWqLuohNYGYYicyOg4LGY2a7n', '2017-06-30 10:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `full_location` text COLLATE utf8_unicode_ci NOT NULL,
  `street_number` int(11) DEFAULT NULL,
  `street` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `name_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package` int(11) DEFAULT NULL,
  `property_type` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `car_spaces` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting` text COLLATE utf8_unicode_ci,
  `viewers` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `published_date` timestamp NULL DEFAULT NULL,
  `published_date_count` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `slug`, `title`, `description`, `full_location`, `street_number`, `street`, `city`, `state`, `country`, `status`, `type`, `price`, `name_contact`, `email_contact`, `address_contact`, `phone_number`, `home_number`, `package`, `property_type`, `bedrooms`, `bathrooms`, `car_spaces`, `size`, `video_link`, `setting`, `viewers`, `created_by`, `published_date`, `published_date_count`, `created_at`, `updated_at`) VALUES
(16, 'i-want-to-sell-my-house-at-aus-16', 'I want to sell my house at AUS', 'I want to sell my house I want to sell my house I want to sell my house', '104 Princes Highway, Bolwarra, Victoria, Australia', 28, 27, 26, 2, 1, 1, 1, 200000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 2, 2, 2, 2, NULL, '', 8, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 08:40:19', '2017-06-25 02:02:32'),
(17, 'house-for-rent-in-melbourne-victoria-17', 'House for rent in Melbourne, Victoria', 'House for rent in Melbourne, Victoria House for rent in Melbourne, Victoria', 'Alexandria, New South Wales, Australia', 43, 42, 41, 18, 1, 1, 2, 1500000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 1, 1, 1, 222, NULL, '', 82, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 11:00:09', '2017-07-10 12:33:15'),
(18, 'sell-land-in-victoria-just-200-1m-18', 'Sell land in Victoria, just 200$/1m', 'Sell land in Victoria, just 20$/1m Sell land in Victoria, just 20$/1m Sell land in Victoria, just 20$/1m', 'Victoria Street, Richmond, Victoria, Australia', 46, 45, 44, 2, 1, 1, 1, 300000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 5, 2, 2, 2, 150, NULL, '', 6, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 11:55:10', '2017-07-05 11:29:01'),
(19, 'sell-a-new-house-full-control-and-full-furniture-19', 'Sell a new house. Full control and full furniture', 'Sell a new house. Full control and full furnitureSell a new house. Full control and full furniture Sell a new house. Full control and full furniture', '2 Collins Street, Melbourne, Victoria, Australia', 5, 4, 3, 2, 1, 1, 2, 3500000, 'Admin', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '0966664602', 3, 2, 2, 2, 2, 750, 'https://www.youtube.com/embed/7590aIHO2aY', '', 8, 2, '2017-06-29 13:11:44', 1498767104, '2017-06-21 21:16:36', '2017-06-29 13:12:21'),
(20, 'outdoor-event-outdoor-event-outdoor-event-outdoor-event-20', 'Outdoor event Outdoor event Outdoor event Outdoor event', 'Outdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor event', '45 Bourke Street, Melbourne, Victoria, Australia', 52, 51, 3, 2, 1, 1, 2, 2000000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 4, 4, 5, 555, NULL, '', 2, 2, '2017-07-05 11:37:27', 1499279847, '2017-07-05 11:32:38', '2017-07-08 10:00:31');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 3, NULL, NULL),
(5, 3, NULL, NULL),
(6, 3, NULL, NULL),
(7, 1, NULL, NULL),
(7, 3, NULL, NULL),
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 1, NULL, NULL),
(16, 4, NULL, NULL),
(16, 5, NULL, NULL),
(16, 6, NULL, NULL),
(17, 7, NULL, NULL),
(19, 4, NULL, NULL),
(20, 10, NULL, NULL),
(21, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE IF NOT EXISTS `report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `target` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Root', 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.', NULL, NULL),
(2, 'Administrator', 'Accept admin location and managing setting backend', NULL, NULL),
(3, 'User', 'User Registration', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(5, 3, NULL, NULL),
(5, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('system','theme','user','post','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_user`
--

CREATE TABLE IF NOT EXISTS `setting_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `setting` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting_user_user_id_unique` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `popular` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `slug`, `name`, `popular`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'sample', 'sample', 4, 0, '2017-06-08 22:01:10', '2017-06-12 08:23:50'),
(4, 'house', 'house', 3, NULL, NULL, '2017-06-21 21:16:36'),
(5, 'aus', 'aus', 1, 0, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(6, 'victoria', 'victoria', 1, 0, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(7, 'apartment', 'apartment', 1, 0, '2017-06-21 11:00:09', '2017-06-21 11:00:09'),
(8, 'land', 'land', 2, 0, '2017-06-21 11:55:10', '2017-06-25 02:17:14'),
(9, 'town-house', 'town house', 1, 0, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(10, 'giant', 'giant', 1, 0, '2017-07-05 11:32:38', '2017-07-05 11:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci,
  `birthday` date DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_member` int(11) NOT NULL DEFAULT '1',
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_json` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_json` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `email`, `password`, `address`, `birthday`, `phone_number`, `gender`, `status`, `remember_token`, `created_at`, `updated_at`, `profile_picture`, `is_member`, `is_admin`, `facebook_id`, `facebook_token`, `facebook_json`, `google_id`, `google_token`, `google_json`, `picture`, `activation_code`) VALUES
(1, 'Root', 'root@root.com', '$2y$10$buknCJpAflTmqwXVxgCjS.1rpA5/lpVIHs8rwZdJnKiWe9h6CIrDa', NULL, NULL, NULL, 1, 0, NULL, '2017-04-05 01:47:50', '2017-04-05 01:47:50', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(2, 'ADMIN', 'admin@admin.com', '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', '1054 Vo Van Kiet, District 5', '1993-03-06', '0966664602', 1, 0, 'vNwNNTr8n4qasyqVxmaxygB2VNCjfXbUoocHegIq4VMqBxJdLdBqWbcyy9Ox', '2017-04-05 01:47:50', '2017-07-12 10:07:41', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, 'c81e728d9d4c2f636f067f89cc14862c1499355134.png', ''),
(3, 'User Test', 'user@propertyasap.com', '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', NULL, NULL, NULL, 1, 0, NULL, '2017-04-05 01:47:50', '2017-04-04 23:51:17', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(11, 'HUYNH HUY QUANG', 'test@test.com', '$2y$10$lHEpTuM5CdqyiNSmeoBldesXPGU6.OCFYfcJXDiqIIY/gKIsuJrQu', '1054 Vo Van Kiet, District 5', NULL, '966664602', 1, 1, NULL, '2017-07-05 11:25:17', '2017-07-05 11:25:17', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `noted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `user_id`, `post_id`, `noted`, `status`, `created_at`, `updated_at`) VALUES
(14, 2, 20, '', 1, '2017-07-08 10:01:32', '2017-07-08 10:01:32'),
(16, 2, 16, '', 1, '2017-07-10 12:32:20', '2017-07-10 12:32:20'),
(17, 2, 17, '', 1, '2017-07-10 12:32:24', '2017-07-10 12:32:24'),
(19, 2, 19, '', 1, '2017-07-10 12:32:46', '2017-07-10 12:32:46');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
