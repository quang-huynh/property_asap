<?php

return [
	
	'upload_post_url'                           => 'uploads' . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR,
	'upload_post_path'                          => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR,

	'upload_profile_picture_url'				=> 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR,
	'upload_profile_picture_path'				=> public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR,

	'profile_picture_url'						=> 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR,
	'default_profile_picture'					=> 'http://placehold.it/40X40',
	'default_post_picture_name_path'			=> 'property_asap_cover.jpg',

	'default_detail_post'						=> 'assets' . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'default_image_post.jpg',

	'post_status'								=> array(
														'published' => 1, //published
														'pending' => 2, //pending
														'draft' => 3, //draft
														'preview' => 4, //preview
														'closed' => 5, //closed
														'deleted' => 6, //deleted
														),


	'package'									=> array('premium' => 1, 'platinum' => 2, 'ultimate' => 3),

	'location_type'								=> array('location_number_street' => 1, //number
														'location_street' => 2, //street
														'location_city' => 3, //city
														'location_state' => 4, //state
														'location_country' => 5, //country
														),

	'property_type_label'								=> array('house' => 'House',
														'apartment_or_unit' => 'Apartment of Unit',
														'town_house' => 'Town House',
														'villa' => 'Villa',
														'land' => 'Land',
														'acreage' => 'Acreage'
														),

	'property_type_value'								=> array('house' => 1,
														'apartment_or_unit' => 2,
														'town_house' => 3,
														'villa' => 4,
														'land' => 5,
														'acreage' => 6
														),

	'property_type_display'								=> array(1 => 'House',
														2 => 'Apartment of Unit',
														3 => 'Town House',
														4 => 'Villa',
														5 => 'Land',
														6 => 'Acreage'
														),

	'bedrooms'									=> array('1+' => 1,
														'2+' => 2,
														'3+' => 3,
														'4+' => 4,
														'5+' => 5,
														'6+' => 6,
														'7+' => 7,
														'8+' => 8,
														'9+' => 9
														),

	'bathrooms'									=> array('1+' => 1,
														'2+' => 2,
														'3+' => 3,
														'4+' => 4,
														'5+' => 5,
														'6+' => 6,
														'7+' => 7,
														'8+' => 8,
														'9+' => 9
														),

	'car_spaces'									=> array('1+' => 1,
														'2+' => 2,
														'3+' => 3,
														'4+' => 4,
														'5+' => 5,
														'6+' => 6,
														'7+' => 7,
														'8+' => 8,
														'9+' => 9
														),

	'min_price'										=> 10000,
	'max_price'										=> 10000000,

	'sort_by'										=> array(
														'Release Date' 	=> 1,
														'Price Low'		=> 2,
														'Price High'	=> 3,
														'Top'			=> 4
													),

	'new_time_life'									=> 3, //days

	'limit_post_search'								=> 10,

	'limit_post_nearly'								=> 10,

	'limit_post_viewed'								=> 10,

	'pin_feature_days'								=> 14,
	'pin_new_days'									=> 3,


	'status_default'								=> array(
														'active' => 1,
														'deactive' => 2
														),

	'user_status'									=> array(
														'inactive' => 0,
														'active' => 1,
														'disabled' => 2,
														'deleted'	=> 3
														),


	'email_confirm'									=> false,

	'paginator'                     				=> 20,

	'default_password'              				=> 'user123!@#',

	'max_images_of_package'							=> array(
															'1' => 5,
															'2' => 7,
															'3' => 10
														),

	'thank_you_create_post'							=> "Congratulations! You have successfully submitted your application. We will review your application and be in contact with you within the next 24 hours. Feel free to preview your property by clicking the button below."

];