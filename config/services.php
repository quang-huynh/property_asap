<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
      'key' => env('SES_KEY'),
      'secret' => env('SES_SECRET'),
      'region' => 'us-east-1',
    ],

    'sparkpost' => [
      'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
      'model' => App\User::class,
      'key' => env('STRIPE_KEY'),
      'secret' => env('STRIPE_SECRET'),
    ],

    /*
     * |---------------------------------------------------------------
     * | CONFIG SERVIER LOGIN SOCIAL
     * |---------------------------------------------------------------
     * | @facebook :
     * | - client_id : cliend_id is app of fb
     * | - client_secret : secret of client app fb
     * | - redirect : url redirect ...
     * |---------------------------------------------------------------
     */
    'facebook' => [
      'client_id' => env('SOCIAL_FACEBOOK_CLIENT_ID', ''),
      'client_secret' => env('SOCIAL_FACEBOOK_CLIENT_SECRET', ''),
      'redirect' => env('SOCIAL_FACEBOOK_REDIRECT', ''),
    ],


    /*
     * |---------------------------------------------------------------
     * | CONFIG SERVIER LOGIN SOCIAL
     * |---------------------------------------------------------------
     * | @google :
     * | - client_id : cliend_id is app of google
     * | - client_secret : secret of client app google
     * | - redirect : url redirect ...
     * |---------------------------------------------------------------
     */
    'google' => [
      'client_id' => env('SOCIAL_GOOGLE_CLIENT_ID', ''),
      'client_secret' => env('SOCIAL_GOOGLE_CLIENT_SECRET', ''),
      'redirect' => env('SOCIAL_GOOGLE_REDIRECT', '')
    ],

];
