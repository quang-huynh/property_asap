<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloumnToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->integer('is_member')->default(1); // 1: true; 0 => false
            $table->integer('is_admin')->default(0);    // 1: true; 0 => false
            $table->string('facebook_id', 255);
            $table->string('facebook_token')->nullable();
            $table->string('facebook_json')->nullable();
            $table->string('google_id')->nullable();
            $table->string('google_token')->nullable();
            $table->string('google_json')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(Blueprint $table)
    {
        $table->dropColumn('is_member');
        $table->dropColumn('is_admin');
        $table->dropColumn('facebook_id');
        $table->dropColumn('facebook_token');
        $table->dropColumn('facebook_json');
        $table->dropColumn('google_id');
        $table->dropColumn('google_token');
        $table->dropColumn('google_json');
    }
}
