<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('name');
            $table->enum(
                        'type_notification',
                        array('system','theme','user','post', 'other')
                    )->nullable();
            $table->enum(
                        'type_message',
                        array('success','warning','information','danger', 'other')
                    )->nullable();
            $table->longText('message');
            $table->string('for_role_id'); //role_id
            $table->integer('status');
            $table->dateTime('lifetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification');
    }
}
