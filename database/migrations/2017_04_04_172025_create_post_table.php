<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('description');
            $table->text('full_location');

            $table->integer('street_number')->nullable();
            $table->integer('street')->nullable();
            $table->integer('city')->nullable();
            $table->integer('state')->nullable();
            $table->integer('country')->nullable();

            $table->integer('type')->default(1);
            $table->integer('price')->default(0);
            $table->string('name_contact')->nullable();
            $table->string('email_contact')->nullable();
            $table->string('address_contact')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('home_number')->nullable();

            $table->integer('package')->nullable();
            $table->integer('property_type')->nullable();
            $table->integer('bedrooms')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->integer('car_spaces')->nullable();
            $table->integer('size')->nullable();
            $table->string('video_link')->nullable();

            $table->integer('status');

            $table->timestamp('published_date')->nullable();
            $table->integer('published_date_count')->nullable();

            $table->text('setting')->nullable();
            $table->integer('viewers')->default(0);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
