<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_location', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->string('noted')->nullable();
            $table->primary(array('post_id', 'location_id'), 'post_location_primary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_location');
    }
}
