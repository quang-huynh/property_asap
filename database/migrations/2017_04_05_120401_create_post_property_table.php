<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_property', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->string('text')->nullable();
            $table->string('value');
            $table->primary(array('post_id', 'property_id'), 'post_property_primary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_property');
    }
}
