<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
class RoleUserTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('role_user')->truncate();

        $role_user = array(
            ['user_id' => '1', 'role_id' => '1'],
            ['user_id' => '2', 'role_id' => '2'],
            ['user_id' => '3', 'role_id' => '3'],
            );
 
        // Uncomment the below to run the seeder
        DB::table('role_user')->insert($role_user);
    }
 
}
