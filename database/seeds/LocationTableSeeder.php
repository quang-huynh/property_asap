<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LocationTableSeeder extends Seeder{

    public function run()
    {

        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('location')->truncate();

        $location = array(
            [
                'id'            => 1,
                'slug'          => 'australia',
                'name'          => 'Australia',
                'type'          => 5,
                'parent_id'     => ''
            ],
            [
                'id'            => 2,
                'slug'          => 'victoria',
                'name'          => 'Victoria',
                'type'          => 4,
                'parent_id'     => 5
            ],
            [
                'id'            => 3,
                'slug'          => 'melbourne',
                'name'          => 'Melbourne',
                'type'          => 3,
                'parent_id'     => 4
            ],
            [
                'id'            => 4,
                'slug'          => 'collins-street',
                'name'          => 'Collins Street',
                'type'          => 2,
                'parent_id'     => 3
            ],
            [
                'id'            => 5,
                'slug'          => '2',
                'name'          => '2',
                'type'          => 1,
                'parent_id'     => 2
            ]
            );
 
        // Uncomment the below to run the seeder
        DB::table('location')->insert($location);
    }

}