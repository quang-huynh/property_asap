<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Tag;

class TagTableSeeder extends Seeder{

    public function run()
    {

        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('tag')->truncate();

        $tag = array(
            [
                'id'            => 1,
                'slug'          => 'sample',
                'name'          => 'sample',
                'popular'       => 1
            ],
            [
                'id'            => 2,
                'slug'          => 'house',
                'name'          => 'house',
                'popular'       => 2
            ]
            );
 
        // Uncomment the below to run the seeder
        DB::table('tag')->insert($tag);
    }

}
