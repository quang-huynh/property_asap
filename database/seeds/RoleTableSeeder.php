<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Role;

class RoleTableSeeder extends Seeder{

    public function run()
    {

        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('role')->truncate();

        $role = array(
            [
                'id'            => 1,
                'name'          => 'Root',
                'description'   => 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.'
            ],
            [
                'id'            => 2,
                'name'          => 'Administrator',
                'description'   => 'Accept admin location and managing setting backend',
            ],
            [
                'id'            => 3,
                'name'          => 'User',
                'description'   => 'User Registration'
            ],
            );
 
        // Uncomment the below to run the seeder
        DB::table('role')->insert($role);
    }

}