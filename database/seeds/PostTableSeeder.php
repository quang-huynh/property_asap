<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Post;

class PostTableSeeder extends Seeder{

    public function run()
    {

        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('post')->truncate();

        $post = array(
            [
                'id'                => 1,
                'slug'              => 'sample-post-test-1',
                'title'             => 'Sample Post Test',
                'description'       => 'Sample Post Test Sample Post Test Sample Post Test Sample Post Test',
                'full_location'     => '2 Collins Street, Melbourne, Victoria, Australia',
                'street_number'     => 5,
                'street'            => 4,
                'city'              => 3,
                'state'             => 2,
                'country'           => 1,
                'status'            => 2,
                'type'              => 1,
                'price'             => 22000,
                'name_contact'      => 'Quang Huynh',
                'email_contact'     => 'nbd.hhq@gmail.com',
                'address_contact'   => 'Viet Nam',
                'phone_number'      => '0966664602',
                'home_number'       => '083983337',
                'package'           => 3,
                'property_type'     => 1,
                'bedrooms'          => 2,
                'bathrooms'         => 2,
                'car_spaces'        => 2,
                'size'              => 200,
                'video_link'        => '',
                'setting'           => '',
                'viewers'           => 1,
                'created_by'        => 1
            ]
            );
 
        // Uncomment the below to run the seeder
        DB::table('post')->insert($post);
    }

}
