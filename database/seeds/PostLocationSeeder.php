<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
class PostLocationTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('post_location')->truncate();

        $post_location = array(
            ['post_id' => '1', 'location_id' => '1'],
            ['post_id' => '1', 'location_id' => '2'],
            ['post_id' => '1', 'location_id' => '3'],
            ['post_id' => '1', 'location_id' => '4'],
            ['post_id' => '1', 'location_id' => '5'],
            );
 
        // Uncomment the below to run the seeder
        DB::table('post_location')->insert($post_location);
    }
 
}
