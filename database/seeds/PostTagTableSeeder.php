<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Post;

class PostTagTableSeeder extends Seeder{

    public function run()
    {

        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('post_tag')->truncate();

        $post_tag = array(
            ['post_id' => '1', 'tag_id' => '1']
            );
 
        // Uncomment the below to run the seeder
        DB::table('post_tag')->insert($post_tag);
    }

}
