<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //Model::unguard();
        DB::table('user')->truncate();

        $user = array(
            ['id' => 1, 'fullname' => 'Root', 'email' => 'root@root.com', 'password' => '$2y$10$buknCJpAflTmqwXVxgCjS.1rpA5/lpVIHs8rwZdJnKiWe9h6CIrDa', 'created_at' => '2017-04-5 08:47:50', 'updated_at' => '2017-04-5 08:47:50'],
            ['id' => 2, 'fullname' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', 'created_at' => '2017-04-5 08:47:50', 'updated_at' => '2017-04-5 06:51:17'],
            ['id' => 3, 'fullname' => 'User Test', 'email' => 'user@propertyasap.com', 'password' => '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', 'created_at' => '2017-04-5 08:47:50', 'updated_at' => '2017-04-5 06:51:17'],
            
            );
 
        // Uncomment the below to run the seeder
        DB::table('user')->insert($user);
    }
 
}
