-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2018 at 03:50 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `property_asap`
--

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `slug`, `name`, `type`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'australia', 'Australia', 5, 0, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(2, 'victoria', 'Victoria', 4, 1, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(3, 'melbourne', 'Melbourne', 3, 2, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(4, 'collins-street', 'Collins Street', 2, 3, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(5, '2', '2', 1, 4, '2017-06-08 01:33:59', '2017-06-08 01:33:59'),
(6, 'south-australia', 'South Australia', 4, 1, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(7, 'millicent', 'Millicent', 3, 6, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(8, 'princes-highway', 'Princes Highway', 2, 7, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(9, '30346', '30346', 1, 8, '2017-06-09 01:20:45', '2017-06-09 01:20:45'),
(10, 'australian-capital-territory', 'Australian Capital Territory', 4, 1, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(11, 'canberra', 'Canberra', 3, 10, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(12, 'n-a', '', 2, 11, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(13, 'n-a', '', 1, 12, '2017-06-12 08:43:03', '2017-06-12 08:43:03'),
(14, 'south-wharf', 'South Wharf', 3, 2, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(15, 'convention-centre-place', 'Convention Centre Place', 2, 14, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(16, '2', '2', 1, 15, '2017-06-12 08:46:19', '2017-06-12 08:46:19'),
(17, '3', '3', 1, 4, '2017-06-12 10:01:05', '2017-06-12 10:01:05'),
(18, 'new-south-wales', 'New South Wales', 4, 1, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(19, 'narooma', 'Narooma', 3, 18, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(20, 'princes-highway', 'Princes Highway', 2, 19, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(21, '2', '2', 1, 20, '2017-06-12 10:02:35', '2017-06-12 10:02:35'),
(22, 'western-australia', 'Western Australia', 4, 1, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(23, 'kingsley', 'Kingsley', 3, 22, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(24, 'creaney-drive', 'Creaney Drive', 2, 23, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(25, 'n-a', '', 1, 24, '2017-06-17 08:47:05', '2017-06-17 08:47:05'),
(26, 'bolwarra', 'Bolwarra', 3, 2, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(27, 'princes-highway', 'Princes Highway', 2, 26, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(28, '104', '104', 1, 27, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(29, 'woolloomooloo', 'Woolloomooloo', 3, 18, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(30, 'crown-street', 'Crown Street', 2, 29, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(31, '2', '2', 1, 30, '2017-06-21 10:53:38', '2017-06-21 10:53:38'),
(32, 'n-a', '', 5, 0, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(33, 'n-a', '', 4, 32, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(34, 'n-a', '', 3, 33, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(35, 'n-a', '', 2, 34, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(36, 'n-a', '', 1, 35, '2017-06-21 11:34:14', '2017-06-21 11:34:14'),
(37, 'queensland', 'Queensland', 4, 1, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(38, 'park-ridge-south', 'Park Ridge South', 3, 37, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(39, 'mount-lindesay-highway', 'Mount Lindesay Highway', 2, 38, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(40, '4111', '4111', 1, 39, '2017-06-21 11:43:28', '2017-06-21 11:43:28'),
(41, 'alexandria', 'Alexandria', 3, 18, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(42, 'n-a', '', 2, 41, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(43, 'n-a', '', 1, 42, '2017-06-21 11:47:23', '2017-06-21 11:47:23'),
(44, 'richmond', 'Richmond', 3, 2, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(45, 'victoria-street', 'Victoria Street', 2, 44, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(46, 'n-a', '', 1, 45, '2017-06-21 11:55:10', '2017-06-21 11:55:10'),
(47, 'n-a', '', 2, 3, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(48, 'n-a', '', 1, 47, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(49, 'swanston-street', 'Swanston Street', 2, 3, '2017-06-25 02:17:14', '2017-06-25 02:17:14'),
(50, '25', '25', 1, 49, '2017-06-25 02:17:14', '2017-06-25 02:17:14'),
(51, 'bourke-street', 'Bourke Street', 2, 3, '2017-07-05 11:32:38', '2017-07-05 11:32:38'),
(52, '45-49', '45-49', 1, 51, '2017-07-05 11:32:38', '2017-07-05 11:32:38'),
(53, 'deer-park', 'Deer Park', 3, 2, '2017-07-19 17:05:40', '2017-07-19 17:05:40'),
(54, 'stevenage-crescent', 'stevenage crescent ', 2, 53, '2017-07-19 17:05:40', '2017-07-19 17:05:40'),
(55, '3', '3', 1, 54, '2017-07-19 17:05:40', '2017-07-19 17:05:40'),
(56, 'moonee-ponds', 'Moonee Ponds', 3, 2, '2017-07-20 21:06:25', '2017-07-20 21:06:25'),
(57, 'park-street', 'Park Street', 2, 56, '2017-07-20 21:06:25', '2017-07-20 21:06:25'),
(58, '25', '25', 1, 57, '2017-07-20 21:06:25', '2017-07-20 21:06:25'),
(59, 'n-a', '', 1, 4, '2017-07-22 19:15:19', '2017-07-22 19:15:19'),
(60, 'south-yarra', 'South Yarra', 3, 2, '2017-07-22 19:38:22', '2017-07-22 19:38:22'),
(61, 'park-street', 'Park Street', 2, 60, '2017-07-22 19:38:22', '2017-07-22 19:38:22'),
(62, '23', '23', 1, 61, '2017-07-22 19:38:22', '2017-07-22 19:38:22'),
(63, 'plumpton', 'Plumpton', 3, 2, '2017-08-11 07:19:49', '2017-08-11 07:19:49'),
(64, 'sample-street', 'Sample Street', 2, 63, '2017-08-11 07:19:49', '2017-08-11 07:19:49'),
(65, '123', '123', 1, 64, '2017-08-11 07:19:49', '2017-08-11 07:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('image','video','document','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `slug`, `name`, `size`, `extension`, `type`, `post_id`, `is_featured`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_5955642eb7e29.jpg', '19_5955642eb7e29.jpg', '74934', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:50', '2017-06-29 13:33:50'),
(2, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_595564337e606.jpg', '19_595564337e606.jpg', '89720', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:55', '2017-06-29 13:33:55'),
(3, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_595564371f79e.jpg', '19_595564371f79e.jpg', '70399', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:33:59', '2017-06-29 13:33:59'),
(4, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\19_5955643ba7935.jpg', '19_5955643ba7935.jpg', '58090', 'jpg', 'image', 19, 0, 2, '2017-06-29 13:34:03', '2017-06-29 13:34:03'),
(5, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f900cc29.jpg', '18_595d2f900cc29.jpg', '848626', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:28', '2017-07-05 11:27:28'),
(6, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f955af10.jpg', '18_595d2f955af10.jpg', '131520', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:33', '2017-07-05 11:27:33'),
(7, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\18_595d2f9c144e3.jpg', '18_595d2f9c144e3.jpg', '423544', 'jpg', 'image', 18, 0, 2, '2017-07-05 11:27:40', '2017-07-05 11:27:40'),
(8, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960658505add.jpg', '20_5960658505add.jpg', '160258', 'jpg', 'image', 20, 1, 2, '2017-07-07 21:54:29', '2017-07-08 08:15:03'),
(11, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960659242db9.jpg', '20_5960659242db9.jpg', '848626', 'jpg', 'image', 20, 0, 2, '2017-07-07 21:54:42', '2017-07-08 08:15:03'),
(16, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960f576392bb.jpg', '20_5960f576392bb.jpg', '52392', 'jpg', 'image', 20, 0, 2, '2017-07-08 08:08:38', '2017-07-08 08:15:03'),
(17, 'G:\\Work\\Freelancer\\property_asap\\public\\uploads\\posts\\20_5960f74bbda1b.jpg', '20_5960f74bbda1b.jpg', '91051', 'jpg', 'image', 20, 0, 2, '2017-07-08 08:16:27', '2017-07-08 08:16:27'),
(18, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a2dd2533.jpg', '23_596f5a2dd2533.jpg', '168973', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:10:05', '2017-07-19 20:12:48'),
(19, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a34d8b9a.jpg', '23_596f5a34d8b9a.jpg', '160258', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:10:12', '2017-07-19 20:12:48'),
(20, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a412af34.jpg', '23_596f5a412af34.jpg', '91051', 'jpg', 'image', 23, 1, 2, '2017-07-19 20:10:25', '2017-07-19 20:12:48'),
(21, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a4d4484b.jpg', '23_596f5a4d4484b.jpg', '221157', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:10:37', '2017-07-19 20:12:48'),
(22, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a57280e7.jpg', '23_596f5a57280e7.jpg', '91051', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:10:47', '2017-07-19 20:12:48'),
(23, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a60e2f67.jpg', '23_596f5a60e2f67.jpg', '848626', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:10:56', '2017-07-19 20:12:48'),
(24, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a681b346.jpg', '23_596f5a681b346.jpg', '166901', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:11:04', '2017-07-19 20:12:48'),
(25, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a6e9bbc5.jpg', '23_596f5a6e9bbc5.jpg', '85359', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:11:10', '2017-07-19 20:12:48'),
(26, '/home/propertyasap/public_html/public/uploads/posts/23_596f5a76857b7.jpg', '23_596f5a76857b7.jpg', '221157', 'jpg', 'image', 23, 0, 2, '2017-07-19 20:11:18', '2017-07-19 20:12:48'),
(27, '/home/propertyasap/public_html/public/uploads/posts/28_59734a172406a.jpg', '28_59734a172406a.jpg', '211848', 'jpg', 'image', 28, 0, 21, '2017-07-22 19:50:31', '2017-07-22 19:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_04_03_160915_create_role_table', 1),
('2017_04_03_162222_create_role_user_table', 1),
('2017_04_03_162313_create_permission_table', 1),
('2017_04_03_162333_create_permission_role_table', 1),
('2017_04_03_162850_create_setting_table', 1),
('2017_04_03_163119_create_report_table', 1),
('2017_04_03_163144_create_notification_table', 1),
('2017_04_03_163159_create_notification_user_table', 1),
('2017_04_03_163231_create_setting_user_table', 1),
('2017_04_03_163527_create_location_table', 1),
('2017_04_04_155744_create_media_table', 1),
('2017_04_04_171955_create_tag_table', 1),
('2017_04_04_172025_create_post_table', 1),
('2017_04_04_172151_create_property_table', 1),
('2017_04_04_173827_create_post_setting_table', 1),
('2017_04_05_120048_create_post_tag_table', 1),
('2017_04_05_120401_create_post_property_table', 1),
('2017_04_06_052132_create_log_table', 1),
('2017_05_27_154417_create_wishlist_table', 1),
('2017_05_29_160145_create_post_location_table', 1),
('2017_06_06_042805_add_column_property_text_in_property_post_table', 2),
('2017_06_06_044301_add_column_type_in_post_table', 3),
('2017_06_06_044301_add_column_type_and_price_in_post_table', 4),
('2017_06_18_032117_create_published_date_column_in_post_table', 5),
('2017_06_25_130300_create_profile_picture_for_user', 6),
('2017_06_25_131539_create_column_activation_code_for_user', 7),
('2017_06_24_054637_add_cloumn_to_user', 8),
('2017_06_25_020517_add_picture_to_user', 9),
('2017_06_25_025533_add_active_code_to_user', 9);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_notification` enum('system','theme','user','post','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_message` enum('success','warning','information','danger','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `for_role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `lifetime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_user`
--

CREATE TABLE `notification_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_message` enum('success','warning','information','danger','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `for_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `lifetime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`email`, `token`, `created_at`) VALUES
('test123@test.com', 'ajNNTAmQWqLuohNYGYYicyOg4LGY2a7n', '2017-06-30 10:34:30'),
('quanghh62@gmail.com', 'NJkFpBiD7U4QVHRlHnamniHNtdeQe43I', '2017-08-10 10:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `full_location` text COLLATE utf8_unicode_ci NOT NULL,
  `street_number` int(11) DEFAULT NULL,
  `street` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `name_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package` int(11) DEFAULT NULL,
  `property_type` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `car_spaces` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting` text COLLATE utf8_unicode_ci,
  `viewers` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `published_date` timestamp NULL DEFAULT NULL,
  `published_date_count` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `slug`, `title`, `description`, `full_location`, `street_number`, `street`, `city`, `state`, `country`, `status`, `type`, `price`, `name_contact`, `email_contact`, `address_contact`, `phone_number`, `home_number`, `package`, `property_type`, `bedrooms`, `bathrooms`, `car_spaces`, `size`, `video_link`, `setting`, `viewers`, `created_by`, `published_date`, `published_date_count`, `created_at`, `updated_at`) VALUES
(16, 'i-want-to-sell-my-house-at-aus-16', 'I want to sell my house at AUS', 'I want to sell my house I want to sell my house I want to sell my house', '104 Princes Highway, Bolwarra, Victoria, Australia', 28, 27, 26, 2, 1, 1, 1, 200000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 2, 2, 2, 2, NULL, '', 10, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 08:40:19', '2017-08-21 03:00:28'),
(17, 'house-for-rent-in-melbourne-victoria-17', 'House for rent in Melbourne, Victoria', 'House for rent in Melbourne, Victoria House for rent in Melbourne, Victoria', 'Alexandria, New South Wales, Australia', 43, 42, 41, 18, 1, 1, 2, 1500000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 1, 1, 1, 222, NULL, '', 83, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 11:00:09', '2017-08-21 03:00:30'),
(18, 'sell-land-in-victoria-just-200-1m-18', 'Sell land in Victoria, just 200$/1m', 'Sell land in Victoria, just 20$/1m Sell land in Victoria, just 20$/1m Sell land in Victoria, just 20$/1m', 'Victoria Street, Richmond, Victoria, Australia', 46, 45, 44, 2, 1, 1, 1, 300000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 5, 2, 2, 2, 150, NULL, '', 8, 2, '2017-06-22 01:38:37', 1498126525, '2017-06-21 11:55:10', '2017-08-21 03:00:05'),
(19, 'sell-a-new-house-full-control-and-full-furniture-19', 'Sell a new house. Full control and full furniture', 'Sell a new house. Full control and full furnitureSell a new house. Full control and full furniture Sell a new house. Full control and full furniture', '2 Collins Street, Melbourne, Victoria, Australia', 5, 4, 3, 2, 1, 1, 2, 3500000, 'Admin', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '0966664602', 3, 2, 2, 2, 2, 750, 'https://www.youtube.com/embed/7590aIHO2aY', '', 10, 2, '2017-06-29 13:11:44', 1498767104, '2017-06-21 21:16:36', '2017-08-21 02:59:52'),
(20, 'outdoor-event-outdoor-event-outdoor-event-outdoor-event-20', 'Outdoor event Outdoor event Outdoor event Outdoor event', 'Outdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor event', '45 Bourke Street, Melbourne, Victoria, Australia', 52, 51, 3, 2, 1, 1, 2, 2000000, 'HUYNH HUY QUANG', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '966664602', '0966664602', 3, 1, 4, 4, 5, 555, NULL, '', 4, 2, '2017-07-05 11:37:27', 1499279847, '2017-07-05 11:32:38', '2017-08-21 02:59:59'),
(21, 'location-location-location-location-location-21', 'location location location location location ', 'i need the web to done be today so i can play around with it, so hurry the fuck up so i start work with it  ', 'Deer Park, Victoria, Australia', 55, 54, 53, 2, 1, 1, 1, 4000000, 'Tony Cutajar', 'tonycutajar@hotmail.com', '3 stevengane crescent deer park', '0423330599', '93631280', 1, 1, 3, 1, 2, 530, NULL, '', 4, 18, '2017-07-19 19:39:42', 1500467982, '2017-07-19 17:05:40', '2017-08-21 02:57:37'),
(22, 'gwefjhrkj-weiufherqihjero-ir-wefiqeo-iguj-ioweufielrj-22', 'gwefjhrkj weiufherqihjero;ir wefiqeo;iguj; ioweufielrj', 'oe;qrgoirhjwr;i eqrioeroigjoerwujwojogow q eiouteogq;ej iqeheqiohjro eiotewroi;rjgw;o pyhi8oerg', 'Deer Park, Victoria, Australia', 55, 54, 53, 2, 1, 4, 1, 50000000, 'Tony Cutajar', 'tonycutajar@hotmail.com', '3 stevengane crescent deer park', '0423330599', '0423330599', 2, 1, 1, 1, 1, 530, NULL, '', 0, 18, NULL, NULL, '2017-07-19 17:12:22', '2017-07-19 17:17:00'),
(23, 'sell-3-stevenage-crescent-deer-park-in-1-month-23', 'Sell 3 Stevenage crescent deer park in 1 month', 'Sell 3 Stevenage crescent deer park in 1 month\nSell 3 Stevenage crescent deer park in 1 month\nSell 3 Stevenage crescent deer park in 1 month\nSell 3 Stevenage crescent deer park in 1 month', '3 Stevenage Crescent, Deer Park, Victoria, Australia', 55, 54, 53, 2, 1, 1, 1, 1000000, 'Tony Cutajar', 'tonycutajar@gmail.com', '3 Stevenage crescent deer park', '966664602', '0966664602', 3, 1, 3, 1, 2, 550, NULL, '', 4, 2, '2017-07-19 20:09:24', 1500469764, '2017-07-19 20:07:00', '2017-08-21 03:00:18'),
(24, 'sell-my-property-right-now-how-hpoe-24', 'sell my property right now how hpoe', 'sell my property right now\nsell my property right now\nsell my property right now\nsell my property right now', '25 Park Street, Moonee Ponds, Victoria, Australia', 58, 57, 56, 2, 1, 2, 1, 1000000, 'Tony Cutajar', 'tonycutajar@hotmail.com', '3 stevengane crescent deer park', '0423330599', '0423330599', 1, 1, 3, 1, 1, 400, NULL, '', 0, 18, NULL, NULL, '2017-07-20 21:06:25', '2017-07-20 21:06:25'),
(25, 'outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-25', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event', 'Outdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor eventOutdoor event Outdoor event Outdoor event', 'Collins Street, Melbourne, Victoria, Australia', 59, 4, 3, 2, 1, 2, 1, 2000000, 'ADMIN', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '009439208039248290', 1, 1, 1, 1, 1, 0, NULL, '', 0, 2, NULL, NULL, '2017-07-22 19:15:19', '2017-07-22 19:15:19'),
(26, 'outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-voutdoor-event-26', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event vOutdoor event ', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event ', 'Collins Street, Melbourne, Victoria, Australia', 59, 4, 3, 2, 1, 2, 1, 200000, 'ADMIN', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '393819082103821390', 1, 1, 1, 1, 1, 0, NULL, '', 0, 2, NULL, NULL, '2017-07-22 19:16:44', '2017-07-22 19:16:44'),
(27, 'outdoor-eventoutdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-27', 'Outdoor eventOutdoor event Outdoor event Outdoor event Outdoor event Outdoor event ', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event ', '2 Collins Street, Melbourne, Victoria, Australia', 5, 4, 3, 2, 1, 2, 1, 150000, 'ADMIN', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '0966664602', 1, 1, 1, 1, 1, 0, NULL, '', 0, 2, NULL, NULL, '2017-07-22 19:19:29', '2017-07-22 19:19:29'),
(28, 'the-property-is-good-please-buy-my-house-28', 'the property is good please buy my house ', 'hi \nhow are you? hope you are well! please help me find the best property for my fist house \nthanks \nkind regards \n', '23 Park Street, South Yarra, Victoria, Australia', 62, 61, 60, 2, 1, 1, 1, 123456789, 'hangbee', 'hangbee@gmail.com', '2 Part street, moonee ponds', '0402356748', '0402356748', 1, 1, 3, 2, 1, 500, NULL, '', 3, 21, '2017-07-22 19:51:45', 1500727905, '2017-07-22 19:38:22', '2017-08-21 03:00:17'),
(29, 'outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-outdoor-event-29', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event ', 'Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event Outdoor event ', '2 Collins Street, Melbourne, Victoria, Australia', 5, 4, 3, 2, 1, 2, 1, 150000, 'ADMIN', 'admin@admin.com', '1054 Vo Van Kiet, District 5', '0966664602', '0966664602', 1, 1, 1, 1, 1, 0, NULL, '', 1, 2, NULL, NULL, '2017-07-22 23:36:14', '2017-07-22 23:36:17'),
(30, 'plumpton-s-perfect-opportunity-don-t-miss-out-inspect-today-30', 'PLUMPTON\\\'S PERFECT OPPORTUNITY - DON\\\'T MISS OUT INSPECT TODAY', 'Beautiful home located in Plumpton perfect for small families and ideal for investors in the ever growing market. Comprising on 3 generous sized bedrooms, study......', 'Plumpton, Victoria, Australia', 65, 64, 63, 2, 1, 2, 1, 490000, 'Milan Tran', 'milan@synergybuilding.net.au', '201/175B Stephen Street', '0466 181 712', '0466181712', 1, 1, 3, 2, 2, 0, NULL, '', 1, 23, NULL, NULL, '2017-08-11 07:19:49', '2017-08-11 07:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 3, NULL, NULL),
(5, 3, NULL, NULL),
(6, 3, NULL, NULL),
(7, 1, NULL, NULL),
(7, 3, NULL, NULL),
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 1, NULL, NULL),
(16, 4, NULL, NULL),
(16, 5, NULL, NULL),
(16, 6, NULL, NULL),
(17, 7, NULL, NULL),
(19, 4, NULL, NULL),
(20, 10, NULL, NULL),
(21, 8, NULL, NULL),
(23, 11, NULL, NULL),
(24, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `target` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Root', 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.', NULL, NULL),
(2, 'Administrator', 'Accept admin location and managing setting backend', NULL, NULL),
(3, 'User', 'User Registration', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(5, 3, NULL, NULL),
(5, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('system','theme','user','post','other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting_user`
--

CREATE TABLE `setting_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `setting` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `popular` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `slug`, `name`, `popular`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'sample', 'sample', 4, 0, '2017-06-08 22:01:10', '2017-06-12 08:23:50'),
(4, 'house', 'house', 4, NULL, NULL, '2017-07-20 21:06:25'),
(5, 'aus', 'aus', 1, 0, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(6, 'victoria', 'victoria', 1, 0, '2017-06-21 08:40:19', '2017-06-21 08:40:19'),
(7, 'apartment', 'apartment', 1, 0, '2017-06-21 11:00:09', '2017-06-21 11:00:09'),
(8, 'land', 'land', 2, 0, '2017-06-21 11:55:10', '2017-06-25 02:17:14'),
(9, 'town-house', 'town house', 1, 0, '2017-06-22 01:38:37', '2017-06-22 01:38:37'),
(10, 'giant', 'giant', 1, 0, '2017-07-05 11:32:38', '2017-07-05 11:32:38'),
(11, 'deer-park', 'deer park', 1, 0, '2017-07-19 20:07:00', '2017-07-19 20:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci,
  `birthday` date DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_member` int(11) NOT NULL DEFAULT '1',
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_json` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_json` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `email`, `password`, `address`, `birthday`, `phone_number`, `gender`, `status`, `remember_token`, `created_at`, `updated_at`, `profile_picture`, `is_member`, `is_admin`, `facebook_id`, `facebook_token`, `facebook_json`, `google_id`, `google_token`, `google_json`, `picture`, `activation_code`) VALUES
(1, 'Root', 'root@root.com', '$2y$10$buknCJpAflTmqwXVxgCjS.1rpA5/lpVIHs8rwZdJnKiWe9h6CIrDa', NULL, NULL, NULL, 1, 0, NULL, '2017-04-05 01:47:50', '2017-04-05 01:47:50', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(2, 'ADMIN', 'admin@admin.com', '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', '1054 Vo Van Kiet, District 5', '1993-03-06', '0966664602', 1, 0, 'uYLNXSKGGNwx1baAuQevexocqQFFMkmuK0MR9H3vaFWCu0uVNpRV1JbTEuDM', '2017-04-05 01:47:50', '2017-08-21 03:01:50', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, 'c81e728d9d4c2f636f067f89cc14862c1499355134.png', ''),
(3, 'User Test', 'user@propertyasap.com', '$2y$10$K2.fve5xLf.t2hQgIlPl.eNV55m68zxwJ0SRZDKfYh2Wy.zM26kv6', NULL, NULL, NULL, 1, 1, NULL, '2017-04-05 01:47:50', '2017-07-25 01:44:57', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(11, 'HUYNH HUY QUANG', 'test@test.com', '$2y$10$3v8Vied0ZZ9MUKnrxv21t.wfQBJyYGS1tER1vNsWylWvhmY0VmsWC', '1054 Vo Van Kiet, District 5', NULL, '966664602', 1, 1, NULL, '2017-07-05 11:25:17', '2017-07-21 01:48:31', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(18, 'Tony Cutajar', 'tonycutajar@hotmail.com', '$2y$10$nhxHXxXAlumR7y1UGPnwIeSBTTjbVmBQMdJ38EWPmYLJGQexZAZlq', '3 stevengane crescent deer park', NULL, '0423330599', 1, 1, NULL, '2017-07-19 16:57:28', '2017-07-19 19:48:52', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(19, 'phong monica', 'mocina@phong.com', '$2y$10$b3uANw0DVUNOj0X7CEonIu3AjzuTsLYDcna.zNeLbtYPr3QpxF862', '12 park st moone ponce ', NULL, '93631280', 1, 1, 'xMcsJrNgYtmbGJRZns3c9saNlJ8Wy7qKtwis5VicjntxUpwDFSSHSVURYJmK', '2017-07-21 16:37:20', '2017-07-22 18:49:26', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(20, 'hang hoang', 'hang@hang.com', '$2y$10$p1aTJNJ0NL3/ddhX72NT8.myFaGNX0zl2lXEyVcZBVUS/dUWdV8aW', '12 park street moone ponce ', NULL, '93631280', 1, 1, 'PqQ8nSyo69xZXa1rdBJTAgRmlSjpj9zLmMAMXLn6ASxiElPkG6SdTysEeu0s', '2017-07-22 18:53:42', '2017-07-22 19:16:13', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(21, 'hangbee', 'hangbee@gmail.com', '$2y$10$fcftaLAePbKka2yKNzMESu049HfB9kUMEOS1Nqi2tFw3w3Fj1UGyO', '2 Part street, moonee ponds', NULL, '0402356748', 1, 1, NULL, '2017-07-22 19:17:16', '2017-07-22 19:17:16', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(22, 'Quang Huynh', 'quanghh62@gmail.com', '$2y$10$kQK7Nd5MAbL/ynU0Mli5ge9YasBdioGOM8QMNtXKG8gOT3/ZAhecK', 'Ho Cho Minh City', NULL, '0966664602', 1, 1, NULL, '2017-07-24 23:42:18', '2017-07-24 23:42:18', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(23, 'Milan Tran', 'milan@synergybuilding.net.au', '$2y$10$gRLX/uuH1LqFuOacUdBU1uIE3.PQjxFhI3JBtZGVy8T.wJXOPoRmm', '201/175B Stephen Street', NULL, '0466 181 712', 1, 1, 'ZrAsbQtUrsNcSalM2bJJuEZ76Vd61idbzVqTUDyW96rvpuxU9MTpmyF0SgQV', '2017-08-11 07:12:09', '2017-08-11 07:13:48', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, '', ''),
(24, '', 'neverbackdown.hhq@gmail.com', '', NULL, NULL, NULL, 1, 1, 'UwU3NSozPetT8VCD5D1HqjU45axy1nM2pjftJ89CRwNSnVbJUZLJvw1hhbwk', '2017-08-11 20:54:19', '2017-08-11 21:25:03', NULL, 1, 0, '1427403437328814', 'EAAMAHroooPIBAPF1aUs2Qjuf6v9RP54N4F0jH7KuWu5w2ypInyhDfzhLw8sXFtQSgiQZCuE9cLZAeoYZCLIKW1zTXhLbbn4K9d1tPswnnuhZCqE5FgfUlY0eAvGMncZCZAz2s5sdwht8A1Sy4X0CGz9N83Q8HCaCE0a8EetFZCzFE3lEK0zgFZCS', '{\"token\":\"EAAMAHroooPIBAPF1aUs2Qjuf6v9RP54N4F0jH7KuWu5w2ypInyhDfzhLw8sXFtQSgiQZCuE9cLZAeoYZCLIKW1zTXhLbbn4K9d1tPswnnuhZCqE5FgfUlY0eAvGMncZCZAz2s5sdwht8A1Sy4X0CGz9N83Q8HCaCE0a8EetFZCzFE3lEK0zgFZCS\",\"refreshToken\":null,\"expiresIn\":5182189,\"id\":\"142740343732', '112420974367380322801', 'ya29.GlykBDXNKtuEkYhOfD7epT1q_kOkKvUwz9LGrViTryAr4iEYOEe1IjrGBs5FFL4iubb4hV_QG495UQ5Edos2KqWHL4VzeveY7beOQsV5MrOSmRiI5dXhXDCO0p-JUA', '{\"token\":\"ya29.GlykBDXNKtuEkYhOfD7epT1q_kOkKvUwz9LGrViTryAr4iEYOEe1IjrGBs5FFL4iubb4hV_QG495UQ5Edos2KqWHL4VzeveY7beOQsV5MrOSmRiI5dXhXDCO0p-JUA\",\"refreshToken\":null,\"expiresIn\":3599,\"id\":\"112420974367380322801\",\"nickname\":null,\"name\":\"Quang Huynh\",\"email\":\"', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `noted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `user_id`, `post_id`, `noted`, `status`, `created_at`, `updated_at`) VALUES
(14, 2, 20, '', 1, '2017-07-08 10:01:32', '2017-07-08 10:01:32'),
(16, 2, 16, '', 1, '2017-07-10 12:32:20', '2017-07-10 12:32:20'),
(17, 2, 17, '', 1, '2017-07-10 12:32:24', '2017-07-10 12:32:24'),
(19, 2, 19, '', 1, '2017-07-10 12:32:46', '2017-07-10 12:32:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_user`
--
ALTER TABLE `notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD KEY `password_reset_email_index` (`email`),
  ADD KEY `password_reset_token_index` (`token`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`role_id`,`permission_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_user`
--
ALTER TABLE `setting_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `setting_user_user_id_unique` (`user_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email_unique` (`email`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_user`
--
ALTER TABLE `notification_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting_user`
--
ALTER TABLE `setting_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
