<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <meta name="user_id" content="{{ Auth::user()->id }}" >
    @section('meta')

    @show

    @section('css')
        @include('backend.partials.css_libraries')
        <!-- Theme style -->
        <link href="{{ asset('assets/backend/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link href="{{ asset('assets/backend/css/skin-blue.min.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/backend/css/master_custom.css') }}" rel="stylesheet" type="text/css" />
    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

    <body class="skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">
                @include('backend.partials.main_header')
            </header>



            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                @include('backend.partials.main_sidebar')
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
            <!-- Content Header (Page header) -->
                <section class="content-header">
                  <h1>
                    @yield('page_header')
                    <small>@yield('optional_description')</small>
                  </h1>
                  @include('backend.partials.breadcrumbs')
                </section>

                <!-- Main content -->
                <section class="content">
                  <!-- Your Page Content Here -->
                  @yield('main')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="pull-right hidden-xs">
                  Powered by <a href="http://www.quanghuynh.com" target="_blank">Quang Huynh</a>
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; 2017 <a href="{{ route('frontend.index') }}">Property Asap</a>.</strong> All rights reserved.
            </footer>
      


            <!-- Control Sidebar -->  
                @include('backend.partials.control_sidebar')
            <!-- /.control-sidebar -->


            <!-- Add the sidebar's background. This div must be placed
               immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>


        @include('backend.partials.box_notification')


        </div><!-- ./wrapper -->
    

        @section('javascript')
            @include('backend.partials.js_libraries')
            <!-- AdminLTE App -->
            <script src="{{ asset('assets/backend/js/admin_lte_app.js') }}" type="text/javascript"></script>

        @show

        <script src="{{ asset('assets/backend/js/app.js') }}" type="text/javascript"></script>

    </body>
</html>