<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Property Asap Administrator</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        @section('css')
            @include('backend.partials.css_libraries')
            <!-- Theme style -->
            <link href="{{ asset('assets/backend/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />

            <link href="{{ asset('assets/backend/css/blank_custom.css') }}" rel="stylesheet" type="text/css" />
        @show

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page">

        @yield('main')


        @section('javascript')
            @include('backend.partials.js_libraries')
        @show

        <!-- EB Common -->
        <script src="{{ asset('assets/backend/js/app.js') }}" type="text/javascript"></script>

        
    </body>
</html>