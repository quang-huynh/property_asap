@extends('backend.layouts.master')
@section('title', 'Detail Permission')
@section('page_header', 'Detail Permission')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Permission</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('permission.index') }}" class="btn btn-default">Back</a></h2>
            <div class="table-responsive">
                <div class="modal">

                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Route Name</th>
                            <th>Created_At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>{{ $permission->title }}</td>
                            <td>{{ $permission->decription }}</td>
                            <td>{{ $permission->getPermissionRouteName() }}</td>
                            <td>{{ $permission->created_at }}</td>
                            <td>
                                <a href="{{ route('permission.update',array('id' => $permission->id)) }}"><i class="glyphicon glyphicon-pencil"></i>
                                <a onclick="return confirm('Xóa ?')" href="{{ route('permission.delete',array('id' => $permission->id)) }}"><i class="glyphicon glyphicon-trash"></i>    
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
