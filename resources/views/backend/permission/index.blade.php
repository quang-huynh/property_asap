@extends('backend.layouts.master')
@section('title', 'Permission')
@section('page_header', 'Permission')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Permission</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('permission.add') }}" class="btn btn-success">Add</a></h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Route Name</th>
                            <th>Created_At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        @foreach($permissions as $index => $permission)
                        @if ($permission->name != 'Root')
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $permission->title }}</td>
                                <td width="30%">{{ $permission->description }}</td>
                                <th>{{ $permission->route_name }}</th>
                                <td>{{ $permission->created_at }}</td>
                                <td>
                                    <a href="{{ route('permission.update',array('id' => $permission->id)) }}" title="Update"><i class="glyphicon glyphicon-pencil"></i>
                                    <a onclick="return confirm('Xóa ?')" href="{{ route('permission.delete',array('id' => $permission->id)) }}" title="delete"><i class="glyphicon glyphicon-trash"></i>       
                                </td>
                            </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                {!! $permissions->render() !!}
                
            </div>
        </div>
    </div>

@endsection
