@extends('backend.layouts.master')
@section('title', 'Add Permission')
@section('page_header', 'Add Permission')
@section('optional_description', '')

@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Permission</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('permission.index') }}" class="btn btn-primary">Back</a></h2>
            <div class="jumbotron">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" permission="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Title</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Description</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="category">List Route Name</label>
                        <div class="col-md-6">
                            <select name="route_name" class="form-control">
                                <option value="">*Select Route Name for setting permission</option>
                                @foreach($routes as $route)
                                    @if (!empty($route->getAction()['as']))
                                        <option value="{{ $route->getAction()['as'] }}">{{ $route->getAction()['as'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
