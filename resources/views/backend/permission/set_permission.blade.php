@extends('backend.layouts.master')
@section('title', 'Set Permission')
@section('page_header', 'Set Permission')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading"><h2>Role : {{ $role->name }}</h2></div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('role.index') }}" class="btn btn-primary">Back</a></h2>
            <div class="jumbotron">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

 
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="category">List Route Name</label>
                        <div class="col-md-6">
                            <select name="permission[]" id='pre-selected-options' multiple='multiple' class="form-control">
                                @foreach ($permissions as $permission)
                                    <option value="{{ $permission->id }}" {{ (in_array($permission->id, $arrPermissionOfRole)) ? 'selected="selected"' : '' }}>{{ $permission->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>

                    

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section("css")
    @parent

    <!-- MultiSelect -->
    <link href="{{ asset('assets/backend/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    
@stop

@section("javascript")
    @parent

    <!-- MultiSelect -->
    <script src="{{ asset('assets/backend/js/plugins/MultiSelect/jquery.multi-select.js') }}" type="text/javascript"></script>

@stop