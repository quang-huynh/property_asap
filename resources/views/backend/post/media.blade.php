@extends('backend.layouts.master')
@section('title', 'Media Post ID: ' . $post->id)
@section('page_header', 'Media Post ID: ' . $post->id)
@section('optional_description', '')

@section('main')
    <div class="panel panel-default">
        <div class="panel-heading">Media</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('admin.post.index') }}" class="btn btn-primary">Back</a></h2>

            <h2 class="sub-header"><a href="{{ route('admin.post.status', ['id' => $post->id]) }}" class="btn btn-primary" target="_blank">Change Status</a></h2>

            <ul id="list-images" class="list-images" data-post-id="{{ $post->id }}" data-url-delete="{{ route('admin.api.post.deletemedia') }}" data-url-featured="{{ route('admin.api.post.featuredimage') }}" >
                @if (!empty($media))
                    @foreach ($media as $key => $item)
                        <li data-id="{{ $item->id }}">
                            <img class="img-responsive" width="300" height="300" src="{{ asset(config('asap.upload_post_url') . $item->name) }}" />
                            @if ($item->is_featured == 0)
                                <button href="javascript:void(0)" class="btn btn-sm btn-primary" rel="featured-image" data-id="{{ $item->id }}">Set featured image</button>
                            @endif
                            <button href="javascript:void(0)" class="btn btn-sm btn-danger" rel="delete-image" data-id="{{ $item->id }}">Remove</button>
                        </li>
                    @endforeach
                @endif
            </ul>


            <div class="jumbotron">
                    <div id="mess" class="alert alert-info" role="alert"></div>
                
                <h3>Upload Images</h3>
                <div class="row form-add">
                    <form class="form-horizontal" role="form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $post->id }}" />

                        <input type="file" name="image" >
                        <button class="btn btn-sm btn-info upload" type="submit" data-url="{{ route('admin.api.post.uploadmedia', ['id' => $post->id]) }}">Upload</button>
                        <button type="button" class="btn btn-sm btn-danger cancel">Cancel</button>

                        <div class="progress progress-striped active">
                            <div class="progress-bar" style="width:0%"></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section("css")
    @parent

    <style>
        input[type=file]{
            float:left;
        }

        #mess {
            display: none;
        }

        .list-images {
            list-style: none;
            /* text-align: center; */
        }

        .list-images li {
            display: inline-block;
            margin: 10px;
            text-align: center;
        }

        .jumbotron {
            padding: 20px;
        }

        .form-add {
            padding: 20px;
        }

        .list-images li img {
            margin: 10px 0;
        }
    </style>
@stop

@section("javascript")
    @parent

    <script type="text/javascript">
        $( document ).ready(function() {

            $(document).on('submit','form',function(e){
                e.preventDefault();

                $form = $(this);

                uploadImage($form);

            });


            $('body').on('click', '[rel="delete-image"]', function(){
                var that = $(this);
                var url = $('.list-images').data('url-delete');
                var id = that.data('id');
                if (!confirm('Do you want remove ?') ) return;
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function(result) {
                    if (result.code == 1) {
                        that.parent('li').fadeOut().remove();
                        $('#mess').empty().append(result.mess).fadeIn('slow/400/fast');
                        setTimeout(function(){ $('#mess').fadeOut(); }, 5000);
                    } else {
                        $('#mess').empty().append(result.mess).fadeIn('slow/400/fast');
                        setTimeout(function(){ $('#mess').fadeOut(); }, 5000);
                    }
                })
                .complete(function(result) {

                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });


            $('body').on('click', '[rel="featured-image"]', function(){
                var that = $(this);
                var url = $('#list-images').data('url-featured');
                var postId = $('#list-images').data('post-id');
                var id = that.data('id');
                if (!confirm('Set feature ?') ) return;
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {postId: postId, id: id},
                })
                .done(function(result) {
                    if (result.code == 1) {
                        window.location.href = window.location.href.split('?')[0];
                    } else {
                        $('#mess').empty().append(result.mess).fadeIn('slow/400/fast');
                        setTimeout(function(){ $('#mess').fadeOut(); }, 5000);
                    }
                })
                .complete(function(result) {

                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });


            

            function uploadImage($form){
                $form.find('.progress-bar').removeClass('progress-bar-success')
                                            .removeClass('progress-bar-danger');

                var formdata = new FormData($form[0]); //formelement
                var request = new XMLHttpRequest();

                //progress event...
                request.upload.addEventListener('progress',function(e){
                    var percent = Math.round(e.loaded/e.total * 100);
                    $form.find('.progress-bar').width(percent+'%').html(percent+'%');
                });

                //progress completed load event
                request.addEventListener('load',function(e){
                    $form.find('.progress-bar').addClass('progress-bar-success').html('Upload completed');
                });

                request.open('post', $form.find('.upload').data('url'), true);
                

                request.onreadystatechange = function () {
                    if(request.readyState === XMLHttpRequest.DONE && request.status === 200) {
                        var result = JSON.parse(request.responseText);
                        //display image
                        if (result.code == 1) {

                            $('.list-images').append('<li data-id="'+ result.id +'"><img class="img-responsive" width="300" height="300" src="'+ result.path +'" /><button href="javascript:void(0)" class="btn btn-sm btn-primary" rel="featured-image" data-id="'+ result.id +'">Set featured image</button><button href="javascript:void(0)" class="btn btn-sm btn-danger" rel="delete-image" data-id="'+ result.id +'">Remove</button></li>');
                        }
                        $('#mess').empty().append(result.mess).fadeIn('slow/400/fast');
                        setTimeout(function(){ $('#mess').fadeOut(); }, 5000);
                    }
                };


                request.send(formdata);

                $form.on('click','.cancel',function(){
                    request.abort();

                    $form.find('.progress-bar')
                        .addClass('progress-bar-danger')
                        .removeClass('progress-bar-success')
                        .html('upload aborted...');
                });

            }
        });
    </script>
    
@stop