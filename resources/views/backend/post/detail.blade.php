@extends('backend.layouts.master')
@section('title', 'Edit Post ID: ' . $post->id)
@section('page_header', 'Edit Post ID: ' . $post->id)
@section('optional_description', '')

@section('main')
    <div class="panel panel-default">
        <div class="panel-heading">Edit</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('admin.post.index') }}" class="btn btn-primary">Back</a></h2>

            <h2 class="sub-header"><a href="{{ route('admin.post.edit', ['id' => $post->id]) }}" class="btn btn-primary" target="_blank">Edit Post</a></h2>

            <h2 class="sub-header"><a href="{{ route('admin.post.media', ['id' => $post->id]) }}" class="btn btn-primary" target="_blank">Media Manage</a></h2>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <label class="col-md-4 control-label">Title</label>
                <div class="col-md-6">
                    <input type="text" name="title" class="form-control" value="{{ $post->title }}"  disabled="disabled" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Description</label>
                <div class="col-md-6">
                    <textarea name="description" class="form-control" rows="5"  disabled="disabled">{{ $post->description }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Status</label>
                <div class="col-md-6">
                    <select name="status" class="form-control" disabled="disabled">
                        @foreach (config('asap.post_status') as $key => $item)
                            <option value="{{ $item }}" <?php if($post->status == $item) echo 'selected="selected"'; ?> >{{ $key }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">  
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Type:</label>
                    <select id="type" name="type" class="form-control" disabled="disabled">
                        <option value="1" @if($post->type == 1) {{ 'selected="selected"'}} @endif>Sell</option>
                        <option value="2" @if($post->type == 2) {{ 'selected="selected"'}} @endif>Rent</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Property Type: </label>
                    <select id="property-type" name="property_type" class="form-control" disabled="disabled">
                        @foreach(config('asap.property_type_label') as $key => $pt)
                            <option value="{{ config('asap.property_type_value')[$key] }}" @if($post->property_type == config('asap.property_type_value')[$key]) selected="selected" @endif>{{ $pt }}</option>
                        @endforeach 
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Bedrooms: </label>
                    <select id="bedrooms" name="bedrooms" class="form-control" disabled="disabled">
                        @foreach(config('asap.bedrooms') as $key => $pt)
                            <option value="{{ config('asap.bedrooms')[$key] }}" @if($post->bedrooms == config('asap.bedrooms')[$key]) selected="selected" @endif>{{ $key }}</option>
                        @endforeach 
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Bathrooms: </label>
                    <select id="bathrooms" name="bathrooms" class="form-control" disabled="disabled">
                        @foreach(config('asap.bathrooms') as $key => $pt)
                            <option value="{{ config('asap.bathrooms')[$key] }}" @if($post->bathrooms == config('asap.bathrooms')[$key]) selected="selected" @endif>{{ $key }}</option>
                        @endforeach 
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Car spaces: </label>
                    <select id="car-spaces" name="car_spaces" class="form-control" disabled="disabled">
                        @foreach(config('asap.car_spaces') as $key => $pt)
                            <option value="{{ config('asap.car_spaces')[$key] }}" @if($post->car_spaces == config('asap.car_spaces')[$key]) selected="selected" @endif>{{ $key }}</option>
                        @endforeach 
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Land sizes (m<sup>2</sup>): </label>
                    <input id="size" name="size" class="form-control" value="{{ $post->size }}" disabled="disabled" />
                </div>
            </div>



            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Price<span class="red-symbol">&#42;</span>: </label>
                    <input id="price" name="price" class="form-control" value="{{ $post->price }}" rel="keypress-only-number" disabled="disabled" />
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Tags: </label>
                    <p>
                        <input id="tags" name="tags" class="form-control" value="{{ implode(',',$tags) }}" data-role="tagsinput" disabled="disabled" />
                    </p>
                </div>
            </div>


            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Name<span class="red-symbol">&#42;</span>: </label>
                    <input id="name-contact" name="name_contact" class="form-control" value="{{ $post->name_contact }}" required  disabled="disabled" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Email<span class="red-symbol">&#42;</span>: </label>
                    <input id="email-contact" name="email_contact" class="form-control" value="{{ $post->email_contact }}" required  disabled="disabled" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Address<span class="red-symbol">&#42;</span>: </label>
                    <input id="address-contact" name="address_contact" class="form-control" value="{{ $post->address_contact }}" required  disabled="disabled" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Phone Number<span class="red-symbol">&#42;</span>: </label>
                    <input id="phone-number" name="phone_number" class="form-control" value="{{ $post->phone_number }}" rel="keypress-only-number" required  disabled="disabled" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>Home Phone<span class="red-symbol">&#42;</span>: </label>
                    <input id="home-number" name="home_number" class="form-control" value="{{ $post->home_number }}" rel="keypress-only-number" required  disabled="disabled" />
                </div>
            </div>

        </div>
    </div>

@endsection


@section("css")
    @parent

    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap-tagsinput.css') }}">
@stop

@section("javascript")
    @parent

    <script src="{{ asset('assets/frontend/js/bootstrap-tagsinput.min.js') }}"></script>

    <script type="text/javascript">
        $('#tags').tagsinput();
    </script>
        

@stop