@extends('backend.layouts.master')
@section('title', 'Post')
@section('page_header', 'Post')
@section('optional_description', '')

@section('meta')
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@stop

@section('main')


    <div class="panel panel-default">
        <div class="panel-heading">Properties</div>

        <div class="panel-body">


            <form class="form-horizontal" role="form" method="post" id="form-search">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputPassword" class="col-sm-3 control-label">Location</label>
                            <div class="col-sm-9">
                                <input id="autocomplete" type="text" class="form-control" name="full_location" value="{{ @$request['full_location'] }}" onFocus="geolocate()" placeholder="Enter location">

                                <input type="hidden" id="street_number" class="form-control" name="location_number_street" value="{{ @$request['location_number_street'] }}" readonly="readonly">
                                <input type="hidden" id="route" class="form-control" name="location_street" value="{{ @$request['location_street'] }}" readonly="readonly">
                                <input type="hidden" id="locality" class="form-control" name="location_city" value="{{ @$request['location_city'] }}" readonly="readonly">
                                <input type="hidden" id="administrative_area_level_1" class="form-control" name="location_state" value="{{ @$request['location_state'] }}" readonly="readonly">
                                <input type="hidden" id="country" class="form-control" name="location_country" value="{{ @$request['location_country'] }}" readonly="readonly">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputPassword" class="col-sm-3 control-label">Id or Title</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="id_title" type="text" value="{{ @$request['id_title'] }}" placeholder="Id or Title" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputPassword" class="col-sm-3 control-label">User Id</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="user_id" type="text" value="{{ @$request['user_id'] }}" placeholder="User Id" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputPassword" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <select name="status" class="form-control">
                                    <option value=""></option>
                                    @foreach (config('asap.post_status') as $key => $item)
                                        <option value="{{ $item }}" >{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputPassword" class="col-sm-3 control-label">Package</label>
                            <div class="col-sm-9">
                                <select name="package" class="form-control">
                                    <option value=""></option>
                                    @foreach (config('asap.package') as $key => $item)
                                        <option value="{{ $item }}" >{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="col-sm-8"></div>
                            <div class="col-sm-4">
                                <input class="form-control btn btn-primary" id="submit-btn" type="submit" value="Search" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="20%">Title</th>
                            <th width="20%">Location</th>
                            <th>Author</th>
                            <th>Package</th>
                            <th>Status</th>
                            <th>Views</th>
                            <th>Published Date</th>
                            <th>Images</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $index => $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td><a href="{{ route('admin.post.detail',array('id' => $post->getId())) }}" target="_blank">{{ $post->getTitle() }}</a></td>
                                <td>{{ $post->getFullLocation() }}</td>
                                <td><a href="{{ route('user.detail', ['id' => $post->getCreatedBy()]) }}" target="_blank">{{ $post->user->getFullName() }}</a></td>
                                <td>{{ array_search($post->getPackage(), config('asap.package')) }}</td>
                                <td><a href="{{ route('admin.post.status',['id' => $post->id]) }}" target="_blank"> {{ array_search($post->getStatus(), config('asap.post_status')) }}</a></td>
                                <td>{{ $post->getViewers() }}</td>
                                <td>{{ $post->getPublishedDate() }}</td>
                                <td><a href="{{ route('admin.post.media', ['id' => $post->getId()]) }}" target="_blank"><i class="glyphicon glyphicon-upload"></i></a></td>
                                <td>
                                    <a href="{{ route('admin.post.edit',array('id' => $post->getId())) }}" title="Update" target="_blank"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" rel="delete-btn" data-url="{{ route('admin.api.post.delete',array('id' => $post->getId())) }}" title="delete" target="_blank"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if (!$search)
                    {!! $posts->render() !!}
                @endif
                
            </div>
        </div>
    </div>
@endsection


@section("css")
    @parent

    <style type="text/css">
        
    </style>
@stop



@section("javascript")
    @parent

    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help posts fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            // postal_code: 'short_name'
        };

        function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {
                // types: ['geocode'],
                componentRestrictions: {country: 'au'}
            }
        );

        // When the post selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
        }

        // Bias the autocomplete object to the post's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }


        $( "#autocomplete" ).autocomplete({
            change: function( event, ui ) {
                $('#tableLocation').fadeIn();
            }
        });



        $('[rel="delete-btn"]').on('click', function(event) {
            if (!confirm('Sure to delete post ?')) {
                return false;
            }
            var that = $(this);
            var url = that.data('url');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json'
            })
            .complete(function() {
                
            })
            .done(function(result) {
                if (result.code == 1) {
                    $('#box-success').find('.box-title').empty().append('Notification');
                    $('#box-success').find('.box-body').empty().append('Delete post success !');
                    $('#box-success').addClass('enable');
                    console.log("hola datevid");
                    that.parent().parent().fadeOut().remove();
                    setTimeout(function(){ 
                        $('#box-success').removeClass('enable');
                    }, 5000);
                    

                }
            })
            .fail(function() {
                console.log("error");
            })    
        });


        $('#form-search').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
                e.preventDefault();
                return false;
            }
        });

        $('#form-search input').on('keypress', function(e) {
            return e.which !== 13;
        });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKnSRmTT7X6CJ9MnQ9u0gn2hrAJrbV2I&libraries=places&callback=initAutocomplete" async defer></script>


@stop
