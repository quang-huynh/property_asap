@extends('backend.layouts.master')
@section('title', 'Post Pending')
@section('page_header', 'Post Pending')
@section('optional_description', '')

@section('meta')
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@stop

@section('main')


    <div class="panel panel-default">
        <div class="panel-heading">Properties</div>

        <div class="panel-body">

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="20%">Title</th>
                            <th width="20%">Location</th>
                            <th>Author</th>
                            <th>Package</th>
                            <th>Status</th>
                            <th>Views</th>
                            <th>Published Date</th>
                            <th>Images</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $index => $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td><a href="{{ route('admin.post.detail',array('id' => $post->getId())) }}" target="_blank">{{ $post->getTitle() }}</a></td>
                                <td>{{ $post->getFullLocation() }}</td>
                                <td><a href="{{ route('user.detail', ['id' => $post->getCreatedBy()]) }}" target="_blank">{{ $post->user->getFullName() }}</a></td>
                                <td>{{ array_search($post->getPackage(), config('asap.package')) }}</td>
                                <td><a href="{{ route('admin.post.status',['id' => $post->id]) }}" target="_blank"> {{ array_search($post->getStatus(), config('asap.post_status')) }}</a></td>
                                <td>{{ $post->getViewers() }}</td>
                                <td>{{ $post->getPublishedDate() }}</td>
                                <td><a href="{{ route('admin.post.media', ['id' => $post->getId()]) }}" target="_blank"><i class="glyphicon glyphicon-upload"></i></a></td>
                                <td>
                                    <a href="{{ route('admin.post.edit',array('id' => $post->getId())) }}" title="Update" target="_blank"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" rel="delete-btn" data-url="{{ route('admin.api.post.delete',array('id' => $post->getId())) }}" title="delete" target="_blank"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
                {!! $posts->render() !!}
                
            </div>
        </div>
    </div>
@endsection


@section("css")
    @parent

    <style type="text/css">
        
    </style>
@stop



@section("javascript")
    @parent

    <script>

        $('[rel="delete-btn"]').on('click', function(event) {
            if (!confirm('Sure to delete post ?')) {
                return false;
            }
            var that = $(this);
            var url = that.data('url');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json'
            })
            .complete(function() {
                
            })
            .done(function(result) {
                if (result.code == 1) {
                    $('#box-success').find('.box-title').empty().append('Notification');
                    $('#box-success').find('.box-body').empty().append('Delete post success !');
                    $('#box-success').addClass('enable');
                    console.log("hola datevid");
                    that.parent().parent().fadeOut().remove();
                    setTimeout(function(){ 
                        $('#box-success').removeClass('enable');
                    }, 5000);
                    

                }
            })
            .fail(function() {
                console.log("error");
            })    
        });


    </script>


@stop
