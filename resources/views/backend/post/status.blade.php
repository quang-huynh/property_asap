@extends('backend.layouts.master')
@section('title', 'Change Status Post ID: ' . $post->id)
@section('page_header', 'Change Status Post ID: ' . $post->id)
@section('optional_description', '')

@section('main')
    <div class="panel panel-default">
        <div class="panel-heading">Change Status</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('admin.post.index') }}" class="btn btn-primary">Back</a></h2>

            <h2 class="sub-header"><a href="{{ route('admin.post.media', ['id' => $post->id]) }}" class="btn btn-primary" target="_blank">Media Manage</a></h2>

            <div class="jumbotron">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $post->id }}" />

                    @if($post->status == config('asap.post_status')['pending'])
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <label class="control-label">One click to publish now </label>
                                <input type="submit" name="published_btn" class="btn btn-success" value="Published Now" onclick="return confirm('Publish now ?')" />
                            </div>
                        </div>


                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <label class="control-label">Or select status</label>
                        </div>
                    </div>

                    @endif

                    <div class="form-group">
                        <label class="col-md-4 control-label">Status</label>
                        <div class="col-md-6">
                            <select name="status" class="form-control">
                                @foreach (config('asap.post_status') as $key => $item)
                                    <option value="{{ $item }}" <?php if($post->status == $item) echo 'selected="selected"'; ?> >{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <input type="submit" name="change" class="btn btn-primary" value="Change" />
                        </div>
                    </div>

                    

                </form>
            </div>
        </div>
    </div>

@endsection


@section("css")
    @parent

    
@stop

@section("javascript")
    @parent

@stop