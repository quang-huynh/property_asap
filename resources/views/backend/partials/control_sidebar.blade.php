<aside class="control-sidebar control-sidebar-dark" id="notification-sidebar">                
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a id="new-notification-list-button" href="#notifications-tab" data-toggle="tab"><i class="fa fa-bell-o"></i></a></li>
        <li><a id="notification-detail-tab-button" href="#notification-detail-tab" data-toggle="tab">Detail</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content q-tab-custom">
        <!-- Home tab content -->
        <div class="tab-pane active" id="notifications-tab">
            <h3 class="control-sidebar-heading">New</h3>
            <ul class="control-sidebar-menu" id="new-notification-list">
                @if (!empty($new_notifications))
                    @foreach($new_notifications as $notification)
                        <li>
                        <i class="glyphicon glyphicon-ok check-read-in-control-sidebar-menu" rel="check-readed-notification" data-url="{{ route('notification.readed', [ 'id' => $notification['id'] ]) }}" data-toggle="tooltip" data-original-title="Đã đọc"></i>
                          <a href="#detail-{{ $notification['id'] }}" rel="click-detail-notification" data-id="{{ $notification['id'] }}" data-url="{{ route('notification.detail', ['id' => $notification['id']]) }}">
                            <i rel="new-notification-icon" class="menu-icon fa fa-bell-o bg-red"></i>
                            <div class="menu-info">
                              <h4 class="control-sidebar-subheading">{{ $notification['title'] }}</h4>
                              <p>{{ $notification['short_description'] }}</p>
                              <p>{{ $notification['created_at'] }}</p>
                            </div>
                          </a>
                        </li>
                    @endforeach
                @endif            
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">All Notifications</h3> 
            <ul class="control-sidebar-menu" id="readed-notification-list">
                @if (!empty($readed_notifications))
                    @foreach($readed_notifications as $notification)
                        <li>
                            <a href="#detail-{{ $notification['id'] }}" rel="click-detail-notification" data-id="{{ $notification['id'] }}" data-url="{{ route('notification.detail', ['id' => $notification['id']]) }}">             
                                <h4 class="control-sidebar-subheading">
                                    {{ $notification['title'] }}
                                    <span class="label label-success pull-right">OK</span>
                                </h4>
                                <p>{{ $notification['short_description'] }}</p>
                                <!-- <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>    -->                                 
                            </a>
                        </li>         
                    @endforeach
                @endif               
            </ul><!-- /.control-sidebar-menu -->   

            <i class="fa fa-refresh fa-spin loading-img-cus" id="loading-img"></i>

        </div><!-- /.tab-pane -->


        <!-- Settings tab content -->
        <div class="tab-pane" id="notification-detail-tab">            
            <p>
                No choose for display
            </p>
        </div>
    </div>
</aside>