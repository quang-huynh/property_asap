<ol class="breadcrumb">
  <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  @if (isset($breadcrumb))
    @foreach ($breadcrumb as $index => $item)
      @if($index < count($breadcrumb) - 1)
        <li>@if(!empty($item['url']))<a href="{{ $item['url'] }}">{{ $item['name'] }}</a>@else{{ $item['name'] }}@endif</li>
      @else
        <li class="active">{{ $item['name'] }}</li>
      @endif
    @endforeach
  @endif
</ol>