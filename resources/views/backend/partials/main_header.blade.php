<!-- Logo -->
<a href="{{ route('admin.index') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">PA</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Property<b>Asap</b></span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success" rel="count-messages"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">You have 0 messages</li>
                    <li>
                        <!-- inner menu: contains the messages -->
                        <ul class="menu">
                        </ul><!-- /.menu -->
                    </li>
                    <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
            </li><!-- /.messages-menu -->

            <!-- Notifications Menu -->
            <li class="dropdown messages-menu" id="dropdown-notifications-menu"><!-- change class notifications-menu to messages-menu -->
                <a href="#" data-toggle="control-sidebar" id="notification-button"><i class="fa fa-bell-o"></i><span class="label label-warning" rel="count-notifications"></span></a>
                <!-- include('backend.partials.notification') -->
            </li>
            <!-- Tasks Menu -->
            <li class="dropdown tasks-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger" rel="count-tasks"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">You have 0 tasks</li>
                    <li>
                        <!-- Inner menu: contains the tasks -->
                        <ul class="menu">
                            
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="#">View all tasks</a>
                    </li>
                </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs">{{ Auth()->user()->fullname }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        <img class="img-circle" src="{{ asset(auth()->user()->getProfilePicture()) }}" />
                        <p>
                            {{ Auth()->user()->fullname }}
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ route('frontend.profile.me') }}" class="btn btn-default btn-flat" target="_blank">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>