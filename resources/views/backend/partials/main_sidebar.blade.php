<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
        <div class="pull-left image">
            <img class="img-circle" src="{{ asset(auth()->user()->getProfilePicture()) }}" />
        </div>
        <div class="pull-left info">
            <p>{{ Auth()->user()->fullname }}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">Home</li>
        <li class="treeview">
            <a href="{{ route('frontend.index') }}">
                <i class="fa  fa-home"></i> <span>Homepage</span>
            </a>
        </li>

        <li class="header">Property</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ route('admin.post.pending') }}"><i class='fa fa-link'></i> <span>Pending Properties</span></a></li>
        <li><a href="{{ route('admin.post.index') }}"><i class='fa fa-link'></i> <span>All Properties</span></a></li>


        <li class="header">Admin</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ route('user.index') }}"><i class='fa fa-link'></i> <span>User</span></a></li>
        <li><a href="{{ route('role.index') }}"><i class='fa fa-link'></i> <span>Role</span></a></li>
        <li><a href="{{ route('permission.index') }}"><i class='fa fa-link'></i> <span>Permission</span></a></li>

    </ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->