    <!-- Menu toggle button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notification-button">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning" rel="count-notifications">{{ (count($new_notifications)) ? count($new_notifications) : '' }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <span rel="count-notifications">{{ (count($new_notifications)) ? count($new_notifications) : '' }}</span> new notifications</li>
        <li>
            <!-- Inner Menu: contains the notifications -->
            <ul class="menu" id="notifications-menu">
                @if (!empty($new_notifications))
                    @foreach($new_notifications as $notification)
                    <li class="unread-item" rel="unread-item">
                        <a href="#">
                            <div class="pull-left">
                                <!-- User Image -->
                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                            </div>
                            <!-- Message title and timestamp -->
                            <h4>
                                {{ $notification['title'] }}
                                <small><i class="fa fa-clock-o"></i>{{ $notification['created_at'] }}</small>
                            </h4>
                            <!-- The message -->
                            <p>{{ $notification['short_description'] }}</p>
                        </a>

                        <i class="glyphicon glyphicon-ok check-read" rel="check-readed-notification" data-url="{{ route('notification.readed', [ 'id' => $notification['id'] ]) }}" data-toggle="tooltip" data-original-title="Đã đọc"></i>
                    </li>
                    @endforeach
                @endif
            </ul>
        </li>
        <li class="footer"><a href="#">View all</a></li>
    </ul>
