@extends('backend.layouts.blank')
@section('title', 'Log In')

@section('main')

    
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ route('admin.login') }}"><b>Property Asap</b></a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to administrator area</p>
            <form method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group has-feedback {{ ($errors->has('email')) ? 'has-error' : '' }}">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <p>{{ ( count($errors) > 0 && !empty($errors->get('email')[0]) ) ? $errors->get('email')[0] : '' }}</p>
                </div>
                <div class="form-group has-feedback {{ ($errors->has('password')) ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <p>{{ ( count($errors) > 0 && !empty($errors->get('password')[0]) ) ? $errors->get('password')[0] : '' }}</p>
                </div>
                <div class="row">
                    <div class="col-xs-8">    
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" rel="icheck" name="remember"> Remember Me
                            </label>
                        </div>                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    
@stop






@section("css")
    @parent
        <!-- iCheck -->
        <link href="{{ asset('assets/backend/css/iCheck.blue.css') }}" rel="stylesheet" type="text/css" />
    
@stop

@section("javascript")
    @parent

    <!-- iCheck -->
    <script src="{{ asset('assets/backend/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

@stop