@extends('backend.layouts.master')
@section('title', 'Detail Role')
@section('page_header', 'Detail Role')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Role</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('role.index') }}" class="btn btn-default">Back</a></h2>
            <div class="table-responsive">
                <div class="modal">

                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created_At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>{{ $role->name }}</td>
                            <td width="30%">{{ $role->description }}</td>
                            <td>{{ $role->created_at }}</td>
                            <td>
                                <a href="{{ route('role.update',array('id' => $role->id)) }}"><i class="glyphicon glyphicon-pencil"></i>
                                <a onclick="return confirm('Xóa ?')" href="{{ route('role.delete',array('id' => $role->id)) }}"><i class="glyphicon glyphicon-trash"></i>    
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
