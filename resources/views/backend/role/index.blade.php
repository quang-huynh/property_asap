@extends('backend.layouts.master')
@section('title', 'Role')
@section('page_header', 'Role')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Role</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('role.add') }}" class="btn btn-success">Add</a></h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created_At</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        @foreach($roles as $index => $role)
                        @if ($role->name != 'Root')
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $role->name }}</td>
                                <td width="30%">{{ $role->description }}</td>
                                <td>{{ $role->created_at }}</td>
                                <td><a class="btn btn-primary" href="{{ route('permission.set',array('roleId' => $role->id)) }}" title="Set Permission">Set Permission</a></td>
                                <td>
                                    <a href="{{ route('role.update',array('id' => $role->id)) }}" title="Update"><i class="glyphicon glyphicon-pencil"></i>
                                    <a onclick="return confirm('Xóa ?')" href="{{ route('role.delete',array('id' => $role->id)) }}" title="delete"><i class="glyphicon glyphicon-trash"></i>       
                                </td>
                            </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                {!! $roles->render() !!}
                
            </div>
        </div>
    </div>

@endsection
