@extends('backend.layouts.master')
@section('title', 'Update Role')
@section('page_header', 'Update Role')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">Role</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('role.index') }}" class="btn btn-primary">Back</a></h2>
            <div class="jumbotron">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ $role->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Description</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="description" value="{{ $role->description }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
