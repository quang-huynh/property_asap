@extends('backend.layouts.master')
@section('title', 'Detail User')
@section('page_header', 'Detail User')
@section('optional_description', '')

@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">User</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('user.index') }}" class="btn btn-default">Back</a></h2>
            <div class="table-responsive">
                <div class="modal">

                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>FullName</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Created_At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>{{ $user->fullname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>
                                @foreach($user->roles()->getResults() as $role)
                                    {{ $role->getName() . ',' }}
                                @endforeach
                            </td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <a href="{{ route('user.update',array('id' => $user->id)) }}"><i class="glyphicon glyphicon-pencil"></i>
                                <a onclick="return confirm('Xóa ?')" href="{{ route('user.delete',array('id' => $user->id)) }}"><i class="glyphicon glyphicon-trash"></i>    
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
