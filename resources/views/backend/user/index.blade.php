@extends('backend.layouts.master')
@section('title', 'User')
@section('page_header', 'User')
@section('optional_description', '')

@section('meta')
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@stop

@section('main')


    <div class="panel panel-default">
        <div class="panel-heading">User</div>

        <div class="panel-body">


            <form class="form-horizontal" role="form" method="post" id="form-search">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="row">

                        <div class="col-sm-8">
                            <label for="inputPassword" class="col-sm-3 control-label">Name or Email</label>
                            <div class="col-sm-9">
                                <input id="typeahead" class="form-control typeahead" name="keyword" type="text" placeholder="Name or Email" data-url="{{ route('user.ajax.searching') }}" />
                                <img class="Typeahead-spinner loading-1" src="{{ asset('assets/backend/img/loading34.gif') }}" style="display: none;">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6">
                                <input class="form-control btn btn-primary" id="submit-btn" type="submit" value="Search" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            <h2 class="sub-header"><a href="{{ route('user.add') }}" class="btn btn-success">Add</a></h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th>Created_At</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $index => $user)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><a href="{{ route('user.detail',array('id' => $user->getId())) }}">{{ $user->getFullName() }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>
                                @foreach($user->roles()->getResults() as $role)
                                    {{ $role->getName() . ',' }}
                                @endforeach
                                </td>
                                <td>{{ $user->getCreatedAt() }}</td>
                                <td>
                                    @foreach(config('asap.user_status') as $key => $status)
                                        @if ($user->status == $status)
                                            {{ $key }}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('user.update',array('id' => $user->getId())) }}" title="Update"><i class="glyphicon glyphicon-pencil"></i>
                                    <a onclick="return confirm('Xóa ?')" href="{{ route('user.delete',array('id' => $user->getId())) }}" title="delete"><i class="glyphicon glyphicon-trash"></i>       
                                    <a onclick="return confirm('Reset Password ?')" href="{{ route('user.resetpassword',array('id' => $user->getId())) }}" title="Reset Password"><i class="glyphicon glyphicon-exclamation-sign"></i> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $users->render() !!}
                
            </div>
        </div>
    </div>
@endsection


@section("javascript")
    @parent

    <!-- Typeahead -->
    <script src="{{ asset('assets/backend/js/plugins/Typeahead/typeahead.bundle.js') }}" type="text/javascript"></script>

@stop
