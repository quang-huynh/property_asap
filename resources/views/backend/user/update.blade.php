@extends('backend.layouts.master')
@section('title', 'Update User')
@section('page_header', 'Update User')
@section('optional_description', '')
@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">User</div>

        <div class="panel-body">
            <h2 class="sub-header"><a href="{{ route('user.index') }}" class="btn btn-primary">Back</a></h2>
            <div class="jumbotron">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="fullname" value="{{ $user->fullname }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="category">Role</label>
                        <div class="col-md-6">
                            <?php 
                                $arrUserRoles = array();
                                $userRoles = $user->roles()->getResults(); 
                                foreach ($userRoles as $index => $role) {
                                    $arrUserRoles[$index + 1] = $role->id;                                    
                                }
                            ?>
                            <select name="role_id[]" id='pre-selected-options' multiple='multiple' class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" {{ in_array((int)$role->id, $arrUserRoles) ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="category">Choose status of user (Optical)</label>
                        <div class="col-md-6">
                            <select name="status" class="form-control">
                                @foreach(config('asap.user_status') as $key => $status)
                                    <option value="{{ $status }}" {{ ($user->status == $status) ? 'selected="selected"' : '' }}>{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section("css")
    @parent

    <!-- MultiSelect -->
    <link href="{{ asset('assets/backend/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    
@stop

@section("javascript")
    @parent

    <!-- MultiSelect -->
    <script src="{{ asset('assets/backend/js/plugins/MultiSelect/jquery.multi-select.js') }}" type="text/javascript"></script>

@stop
