@extends('frontend.layouts.master')

@section('title', 'Create Property')




@section('content')

    
    <div class="rs_transprantbg">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="rs_error_section rs_bottompadder90">
                    <img class="images-kangaroo-404" src="{{ asset('assets/common/images/warning-404.png') }}" alt="" class="img-responsive">
                    <div class="re_error_info rs_toppadder20">
                        <h1>503</h1>
                        <span class="rs_toppadder20">Opps!</span>
                        <p class="rs_toppadder30">Maybe you would like to go back to our</p>
                        <p><a href="{{ route('frontend.index') }}">HomePage</a> or  <a href="{{ route('contact') }}">Contact us</a>?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('css')
@parent


@endsection



@section('js')
@parent

@endsection




