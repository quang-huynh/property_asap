@extends('frontend.layouts.master')

@section('title', 'Password Reset')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))


@section('content')

    <div class="rs_graybg rs_toppadder100 rs_bottompadder100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_contact_form">
                        <h2>Reset Password</h2>

                        <div class="rs_submitform">

                            <form role="form" method="POST" action="{{ url('/password/reset') }}">
                                {{ csrf_field() }}

                                @include('frontend.components.forms.input.input', ['field' => 'email', 'label' => 'Email : ', 'placeholder' => 'Enter your email', 'default' =>''])


                                <div class="rs_btn_div rs_toppadder30">
                                    <button type="submit" class="rs_button rs_button_orange">Reset</button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    @parent
@endsection

@section('js')
    @parent
@endsection