@extends('frontend.layouts.homepage')

@section('title', 'Home')
@section('content')

<div class="hero">

    <div class="hero-content">
        <div class="search-form-container">
            <form action="{{ route('post.search') }}" method="GET" class="search-form" name="search-form">
                {{ csrf_field() }}
                <h1>Search properties to buy</h1>

                <div class="search-container q-homeplage-search-custom-container">


                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 q-item">
                        <div class="q-custom-homepage-choose-type-search">
                            <select class="rs-custom-select" name="type">
                                <option value="buy" selected="selected">Buy</option>
                                <option value="rent">Rent</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 q-item">
                        <div class="rui-search-container search-input-container">
                            <i class="rui-icon rui-icon-search search-icon"></i>
                            <input id="autocomplete" name="search" tabindex="2" class="rui-input rui-location-box" name="full_location" onFocus="geolocate()" placeholder="Enter location">
                            
                            <input type="hidden" id="street_number" class="form-control" name="location_number_street" value="">
                            <input type="hidden" id="route" class="form-control" name="location_street" value="">
                            <input type="hidden" id="locality" class="form-control" name="location_city" value="" disabled="true" readonly="readonly">
                            <input type="hidden" id="administrative_area_level_1" class="form-control" name="location_state" value="" disabled="true" readonly="readonly">
                            <input type="hidden" id="country" class="form-control" name="location_country" value="" disabled="true" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 q-item">
                        <div class="rui-search-container search-input-container">
                            <button class="rui-search-button" tabindex="10">
                                <span class="rui-visually">Search</span>
                            </button>
                            <div class="focus-border"></div>
                        </div>
                    </div>

                    <section class="search-refinements q-item">
                        <div class="condition property-select-holder q-item">
                            <div class="select-holder">
                                <label>Property types</label>
                                <select id="bootstrap-multiselect-property" name="property_type[]" multiple="multiple" required>
                                    @foreach (config('asap.property_type_label') as $index => $label)
                                        <option value="{{ config('asap.property_type_value')[$index] }}">{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                       
                        <div class="condition price-select-container q-item">
                            <div class="min price select-holder q-item">
                                <label>Min price</label>
                                <select id="bootstrap-multiselect-min-price" name="minPrice">
                                    <option value="0" data-all="Any">Min price</option>
                                    <option value="10000">$10,000</option>
                                    <option value="50000">$50,000</option>
                                    <option value="100000">$100,000</option>
                                    <option value="150000">$150,000</option>
                                    <option value="200000">$200,000</option>
                                    <option value="250000">$250,000</option>
                                    <option value="300000">$300,000</option>
                                    <option value="350000">$350,000</option>
                                    <option value="400000">$400,000</option>
                                    <option value="450000">$450,000</option>
                                    <option value="500000">$500,000</option>
                                    <option value="550000">$550,000</option>
                                    <option value="600000">$600,000</option>
                                    <option value="650000">$650,000</option>
                                    <option value="700000">$700,000</option>
                                    <option value="750000">$750,000</option>
                                    <option value="800000">$800,000</option>
                                    <option value="850000">$850,000</option>
                                    <option value="900000">$900,000</option>
                                    <option value="950000">$950,000</option>
                                    <option value="1000000">$1,000,000</option>
                                    <option value="1250000">$1,250,000</option>
                                    <option value="1500000">$1,500,000</option>
                                    <option value="1750000">$1,750,000</option>
                                    <option value="2000000">$2,000,000</option>
                                    <option value="2500000">$2,500,000</option>
                                    <option value="3000000">$3,000,000</option>
                                    <option value="4000000">$4,000,000</option>
                                    <option value="5000000">$5,000,000</option>
                                </select>
                            </div>
                            <div class="max price select-holder q-item">
                                <label>Max price</label>
                                <select id="bootstrap-multiselect-max-price" name="maxPrice">
                                    <option value="0" data-all="Any">Max price</option>
                                    <option value="100000">$100,000</option>
                                    <option value="150000">$150,000</option>
                                    <option value="200000">$200,000</option>
                                    <option value="250000">$250,000</option>
                                    <option value="300000">$300,000</option>
                                    <option value="350000">$350,000</option>
                                    <option value="400000">$400,000</option>
                                    <option value="450000">$450,000</option>
                                    <option value="500000">$500,000</option>
                                    <option value="550000">$550,000</option>
                                    <option value="600000">$600,000</option>
                                    <option value="650000">$650,000</option>
                                    <option value="700000">$700,000</option>
                                    <option value="750000">$750,000</option>
                                    <option value="800000">$800,000</option>
                                    <option value="850000">$850,000</option>
                                    <option value="900000">$900,000</option>
                                    <option value="950000">$950,000</option>
                                    <option value="1000000">$1,000,000</option>
                                    <option value="1250000">$1,250,000</option>
                                    <option value="1500000">$1,500,000</option>
                                    <option value="1750000">$1,750,000</option>
                                    <option value="2000000">$2,000,000</option>
                                    <option value="2500000">$2,500,000</option>
                                    <option value="3000000">$3,000,000</option>
                                    <option value="4000000">$4,000,000</option>
                                    <option value="5000000">$5,000,000</option>
                                    <option value="10000000">$10,000,000</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="rui-clearfix"></div>
                    </section>
                    <div class="rui-clearfix"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="spotlight-container"></div>

    <div class="hero-overlay" style="background-image: url({{ asset('assets/frontend/images/bg-hero.jpg') }}); display: block;"></div>
    <div class="hero-background blur-disappear" style="background-image:url({{ asset('assets/frontend/images/bg-hero.jpg') }})"></div>
</div>



<div class="rs_testimonial_section rs_toppadder80 rs_bottompadder80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1">
                        <div class="row">
                            <div class="rs_subscribe_section_form">
                                <div class="rs_signup_logo">
                                    <img src="{{ asset('assets/frontend/images/logo_1.png') }}" alt="">
                                </div>
                                <div class="rs_signup_info">
                                    <h4>Do you want to sell your house ?</h4>
                                    <p>By clicking "GET STARTED" you agree to our <a href="{{ route('terms') }}" target="_blank">terms and conditions</a></p>
                                </div>
                                <div class="rs_signup_infobtn rs_toppadder10">
                                    @if(auth()->check())
                                        <a href="{{ route('post.create') }}" class="rs_button rs_button_orange rs_center_btn q-btn-get-started pull-right">GET STARTED</a>
                                    @else 
                                        <a href="#" class="rs_button rs_button_orange rs_center_btn q-btn-get-started pull-right" data-toggle="modal" data-target="#agree-terms-privacy-modal">GET STARTED</a>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="agree-terms-privacy-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Terms and Privacy</h4>
            </div>
            <div class="modal-body">
                By clicking "GET STARTED" you agree to our <a href="{{ route('terms') }}" target="_blank">Terms of Service</a> and <a href="{{ route('privacy') }}" target="_blank">Privacy Policy</a>. Please get started here with step by step.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn rs_button_gray" data-dismiss="modal">Close</button>
                <button id="agree-modal-btn" type="button" class="rs_button rs_button_orange rs_center_btn pull-right">Agree & Continue</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection




@section('css')
@parent

@endsection



@section('js')
@parent
<script>
    $(document).ready(function() {

        $('.chosen-select').chosen();


        $('#bootstrap-multiselect-property').multiselect({
            maxHeight: 200,
            numberDisplayed: 1,
            includeSelectAllOption: true,
            nonSelectedText: 'Non property types',
            selectAllText: 'All property types',
            selectAllValue: 'all',
        });

        $('#bootstrap-multiselect-min-price').multiselect({
            maxHeight: 200,
            nonSelectedText: 'Non min price',
        });

        $('#bootstrap-multiselect-max-price').multiselect({
            maxHeight: 200,
            nonSelectedText: 'Non max price',
        });
    });
</script>

<script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        // postal_code: 'short_name'
    };

    function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {
            // types: ['geocode'],
            componentRestrictions: {country: 'au'}
        }
    );

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    $( "#autocomplete" ).autocomplete({
        change: function( event, ui ) {
            $('#tableLocation').fadeIn();
        }
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKnSRmTT7X6CJ9MnQ9u0gn2hrAJrbV2I&libraries=places&callback=initAutocomplete" async defer></script>


@endsection





