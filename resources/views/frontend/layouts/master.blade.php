<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<!--<![endif]-->

    <!-- BEGIN HEAD -->
    <head>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <!-- Open Graph data -->
        <meta property="fb:app_id" content="844556902375666" />
        <meta property="og:title" content="@yield('fb_title')" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="@yield('fb_url')" />
        <meta property="og:image" content="@yield('fb_image')" />
        <meta property="og:description" content="@yield('fb_description')" />

        <meta name="google-site-verification" content="u9dzK5HCIqJ33FwIjshJgv0-bCMsHxNZjVRzQzferzQ" />
       

        <title>@yield('title') | Property Asap</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="add_wishlist_url" content="{{ route('api.wishlist.add_wishlist') }}" />
        <meta name="remove_wishlist_url" content="{{ route('api.wishlist.remove_wishlist') }}" /> 

        <meta name="MobileOptimized" content="320">
        
        <link rel="favicon" type="image/png" href="favicon.png">
        
        @section('css')
        
        <!--srart theme style -->
        <link href="{{ asset('assets/frontend/css/main.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('assets/frontend/css/homepage-custom.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('assets/frontend/css/q-custom.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('assets/frontend/css/color.css') }}" rel="stylesheet" type="text/css">
        <!-- end theme style -->

        @show
        

    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body >

        <!--Loader Start -->
        <div class="rs_preloaded">
            <div class="rs_preloader">
                <div class="lines">
                    <img src="{{ asset('assets/frontend/images/house-icon.png') }}" width="40" height="40" />
                </div>
                <div class="loading-text">LOADING...</div>
            </div>
        </div>
        <!--Loader End -->

        
        @include('frontend.partials.master_header')


        @yield('content')


        @include('frontend.partials.master_footer')

        @include('frontend.partials.notification_modal')
        
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->
        @section('js')
        <script src="{{ asset('assets/frontend/js/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/js/bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/js/modernizr.custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/rating/star-rating.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/countto/jquery.countTo.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/countto/jquery.appear.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/owl/owl.carousel.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/revolution.extension.actions.min.j') }}s" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/revolution.extension.layeranimation.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/revolution.extension.navigation.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/revolution.extension.parallax.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/revel/revolution.extension.slideanims.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/js/jquery.mixitup.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/fancybox/jquery.fancybox.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/bootstrap-slider/bootstrap-slider.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/offcanvasmenu/snap.svg-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/offcanvasmenu/classie.js') }}" type="text/javascript"></script>
        <!-- <script src="{{ asset('assets/frontend/plugins/offcanvasmenu/main3.js') }}" type="text/javascript"></script> -->
        <script src="{{ asset('assets/frontend/plugins/jquery-ui/jquery-ui.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/c3_chart/d3.v3.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/c3_chart/c3.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/js/pgwslideshow.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/frontend/plugins/chosen/chosen.jquery.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/frontend/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/frontend/plugins/jssocials/jssocials.min.js') }}" type="text/javascript"></script>


        <script src="{{ asset('assets/frontend/js/custom.js') }}" type="text/javascript"></script>

        <script type="text/javascript">
            var csrfToken = $('[name="csrf_token"]').attr('content');

            setInterval(refreshToken, 3600000); // 1 hour 

            function refreshToken(){
                $.get('refresh-csrf').done(function(data){
                    csrfToken = data; // the new token
                });
            }

            setInterval(refreshToken, 3600000); // 1 hour 

        </script>

        {{--Using for Laracasts/Flash --}}
        <script>
            $('#flash-overlay-modal').modal();
        </script>
        @include('flash::message')

        @show

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-102856983-1', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="{{ asset('assets/frontend/js/app.js') }}" type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                $('#loginForm').on('submit', function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: '/login',
                        data: $(this).serialize(),
                        success: function (data) {
                            var status = data.status;
                            if (status == 200) {
                                location.reload(true);
                                return;
                            }
                            return showModal('login form', data.msg);
                        }
                    });
                });

                function showModal(titleName, msgName) {
                    console.log(msgName);
                    $("#notificationTitle").text(msgName);
                    $("#notificationModal").modal({backdrop: "static"});
                }
            })
        </script>
        @show
    
    </body>
</html>