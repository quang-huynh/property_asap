@extends('frontend.layouts.master')

@section('title', 'Search')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))


@section('content')

    <!--Breadcrumb start-->
    <div class="rs_pagetitle rs_toppadder40 rs_bottompadder40">
    <div class="rs_overlay"></div>
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_found_count">
                        <p><span id="total-found">{{ count($posts) }}</span> Properties Found</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumb end-->



    <!--Breadcrumb end-->
    <div class="rs_search_section rs_search_red rs_toppadder40 rs_bottompadder40">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="rs_search_form">
                                <form id="location-form" class="form-inline" method="GET" data-url="{{ route('post.search') }}">
                                    <div class="form-group">
                                        <input type="hidden" name="action" value="Search location" />
                                        <input id="autocomplete" type="text" class="form-control" name="full_location" onFocus="geolocate()" value="@if (!empty($request)) {{ @$request['full_location'] }} @endif" placeholder="Enter your property location">

                                        <input type="hidden" id="street_number" class="form-control" name="location_number_street" value="@if(!empty($request)){{ @$request['location_number_street'] }}@endif" readonly="readonly">
                                        <input type="hidden" id="route" class="form-control" name="location_street" value="@if (!empty($request)){{ @$request['location_street'] }}@endif" readonly="readonly">
                                        <input type="hidden" id="locality" class="form-control" name="location_city" value="@if(!empty($request)){{ @$request['location_city'] }}@endif" readonly="readonly">
                                        <input type="hidden" id="administrative_area_level_1" class="form-control" name="location_state" value="@if(!empty($request)){{ @$request['location_state'] }}@endif" readonly="readonly">
                                        <input type="hidden" id="country" class="form-control" name="location_country" value="@if(!empty($request)){{ @$request['location_country'] }}@endif" readonly="readonly">
                                    </div>
                                    <input type="submit" id="submit-btn" class="btn rs_search_btn" value="Find">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="search-post-module" class="rs_graybg rs_product_found rs_bottompadder30 rs_contact_form search-post-module">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_filte rs_product4column">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="rs_sort"><i class="glyphicon glyphicon-option-vertical" aria-hidden="true"></i> <span id="sort-by-text">Sort by</span></div>
                                <div class="rs_product_sorting">
                                    <ul>
                                        @foreach (config('asap.sort_by') as $key => $sortby)
                                            <li><a href="javascript:void(0)" rel="sort-by" data-value="{{ $sortby }}" data-label="{{ $key }}">{{ $key }}</a></li>
                                        @endforeach
                                    </ul>
                                    <input type="hidden" id="sort-by" name="sort_by" value="" />
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                <ul class="rs_sorting">
                                    <li><a href="javascript:void(0)" class="filter" data-filter="all">All</a></li>
                                    <li><a href="javascript:void(0)" class="filter" data-filter=".buy">Buy</a></li>
                                    <li><a href="javascript:void(0)" class="filter" data-filter=".rent">Rent</a></li>
                                </ul>
                                <div id="rs_viewcontrols" class="rs_product_view">
                                    <ul>
                                        <li><a href="javascript:void(0)" class="listview"><i class="fa fa-th-list"></i></a></li>
                                        <li><a href="javascript:void(0)" class="gridview active"><i class="fa fa-th-large"></i></a></li>
                                    </ul>
                                </div>
                                <div class="rs_btn_div">
                                    <a href="javascript:void(0)" id="filter-btn" class="rs_button rs_button_orange" data-url="{{ route('api.post.search') }}" data-offset="{{ config('asap.limit_post_search') }}" data-offset-ori="{{ config('asap.limit_post_search') }}">Filter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="search-main-contain" class="row">
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 col-lg-push-3 col-md-push-4 col-sm-push-4 col-xs-push-0">
                    <div id="rs_grid">
                        <div class="row">
                            <div id="list-post-wrapper" class="woocommerce_wrapper rs_listview_div">
                                <div id="message-list-post" class="message-list-post"><h3 class="title text-center">No item.</h3></div>
                                <ul id="list-post" class="dgm_listdata rs_grid">

                                    @if (count($posts) > 0)
                                        @foreach ($posts as $key => $post)
                                            @include('frontend.post.partials.post_item_search', ['post' => $post, 'postService' => $postService])
                                        @endforeach
                                    @endif


                                </ul>
                            </div>  

                            <div class="loading-status">
                                <div class="loader">Loading...</div>
                            </div>
                        </div>  
                    </div>

                </div>




                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-pull-9 col-md-pull-8 col-sm-pull-8 col-xs-pull-0">
                        <div class="rs_sidebar_wrapper">

                            <aside class="widget widget_file_formate">
                                <h4 class="widget-title">Property Type</h4>
                                <ul>
                                    @foreach (config('asap.property_type_label') as $key => $type)
                                    <li>
                                        <div class="rs_checkbox">
                                            <input type="checkbox" value="{{ config('asap.property_type_value')[$key] }}" id="property-type-filter-{{ $key }}" rel="property-type-filter" data-key="{{ $key }}" name="property_type[]">
                                            <label for="property-type-filter-{{ $key }}"></label>
                                        </div> {{ $type }} <span>(34)</span>
                                    </li>
                                    @endforeach
                                </ul>
                            </aside>

                            <aside class="widget widget_categories">
                                <h4 class="widget-title">Properties Option</h4>
                                <ul class="rs_submitform">
                                    <li>
                                        <label>Bedrooms: </label>
                                        <select id="bedrooms" name="bedrooms" class="form-control">
                                            <option value="0">All</option>
                                            @foreach(config('asap.bedrooms') as $key => $value)
                                                <option value="{{ $value }}">{{ $key }}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li>
                                        <label>Bathrooms: </label>
                                        <select id="bathrooms" name="bathrooms" class="form-control">
                                            <option value="0">All</option>
                                            @foreach(config('asap.bathrooms') as $key => $value)
                                                <option value="{{ $value }}">{{ $key }}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li>
                                        <label>Car spaces: </label>
                                        <select id="car-spaces" name="car_spaces" class="form-control">
                                            <option value="0">All</option>
                                            @foreach(config('asap.car_spaces') as $key => $value)
                                                <option value="{{ $value }}">{{ $key }}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li>
                                        <label>Land sizes (m<sup>2</sup>): </label>
                                        <select id="size-compare" name="size-compare" class="form-control">
                                            <option value=">=">>=</option>
                                            <option value="<="><=</option>
                                            <option value="=">=</option>
                                        </select>
                                        <input id="size" name="size" class="form-control" rel="keypress-only-number" placeholder="Land sizes" />
                                    </li>
                                </ul>
                            </aside>
                            
                            
                            
                            <aside class="widget widget_price">
                                <h4 class="widget-title">price</h4>
                                <div class="rs_price_filter">
                                 <input id="ex2" name="price" type="text" class="span2" value="" data-slider-min="{{ config('asap.min_price') }}" data-slider-max="{{ config('asap.max_price') }}" data-slider-step="5" data-slider-value="[ {{ config('asap.min_price') }} , {{ config('asap.max_price') }} ]" />
                                 </div>
                            </aside>
                            
                            <aside class="widget widget_tag_cloud">
                                <h4 class="widget-title">Tags</h4>
                                <div class="tag_cloud_box">
                                    @foreach ($tags as $key => $tag)
                                        <a href="javascript:void(0)" class="ed_btn ed_orange @if (in_array($tag->id,$tagsSelected)) {{ 'active' }} @endif" rel="tag-filter" data-value="{{ $tag->id }}">{{ $tag->name }}</a>   
                                    @endforeach
                                    <input type="hidden" id="tags-filter" name="tags-filter" value="" />
                                </div>
                            </aside>

                        </div>
                </div>
                
            </div>
        </div>
    </div>

@endsection




@section('css')
@parent

@endsection



@section('js')
@parent


<script src="{{ asset('assets/frontend/plugins/imakewebthings-waypoints/lib/jquery.waypoints.min.js') }}" type="text/javascript"></script>


<script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        // postal_code: 'short_name'
    };

    function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {
            // types: ['geocode'],
            componentRestrictions: {country: 'au'}
        }
    );

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKnSRmTT7X6CJ9MnQ9u0gn2hrAJrbV2I&libraries=places&callback=initAutocomplete" async defer></script>

@endsection





