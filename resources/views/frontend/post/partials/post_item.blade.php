<?php 
    $image = asset('assets/common/images/' . config('asap.default_post_picture_name_path'));

    if ( count($post->media()->getResults()) > 0 ) {
        foreach ($post->media()->getResults() as $key => $img) {
            if ($img->is_featured) {
                $image = asset(config('asap.upload_post_url') . $img->name);
            }
        }
    }
?>


@if (!empty($post))
    <!--- property item -->
    @if (!empty(Auth::user()))
    <div class="rs_product_div" @if($post->created_by == Auth::user()->id) style="border: 1px #028482 solid;" @endif>
    @else
    <div class="rs_product_div">
    @endif
        @if ($postService->pinFeatureOnSearchPage($post->id) && $post->status == config('asap.post_status')['published'])
            <div class="rs_featureddiv">Featured</div>
        @elseif ($postService->PinNewOnSearchPage($post->id) && $post->status == config('asap.post_status')['published'])
            <div class="rs_featureddiv q-rs_newdiv">New</div>
        @endif

        @if (!empty(Auth::user()))
        <div class="wishlist-item @if($postService->hasWishList($post->id)) {{ 'active' }} @endif">
            <i class="fa fa-heart"></i>
            <a href="javascript:void(0)" rel="btn-remove-wishlist" class="btn-remove-wishlist" data-id="{{ $post->id }}">Remove from wishlist</a>
        </div>
        @endif

        <div class="rs_product_img">
        <img src="{{ $image }}" class="img-responsive" alt="{{ $post->title }}">
        <div class="rs_overlay">
            <div class="rs_overlay_inner">
                <ul>
                    <li><a href="{{ route('post.detail', ['slug' => $post->slug]) }}" class="fancybox animated slideInDown" title="{{ $post->title }}"><i class="fa fa-eye"></i></a></li>
                    @if (!empty(Auth::user()))
                    <li><a href="javascript:void(0)" rel="add-wishlist" class="animated slideInDown @if($postService->hasWishList($post->id)) {{'hide'}} @endif" title="Add to wishlist" data-id="{{ $post->id }}"><i class="fa fa-heart"></i></a></li>
                    @else
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#login_popup" class="animated slideInDown" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="rs_product_price">
            @if ($post->status == config('asap.post_status')['closed'])
                <h2><small>CLOSED</small></h2>
            @else
                <h2><small>$</small>{{ $post->price }}</h2>
            @endif
        </div>
        </div>
        <div class="rs_productdata">
            <div class="rs_productdata_inner">
                <div class="rs_product_detail">
                    <h5><a href="{{ route('post.detail', ['slug' => $post->slug]) }}" data-toggle="tooltip" data-placement="bottom" title="Type: {{ config('asap.property_type_display')[$post->property_type] }}, Bedrooms: {{ $post->bedrooms }}, Bathrooms: {{ $post->bathrooms }}, Car Spaces: {{ $post->car_spaces }}, Size: {{ $post->size }}" rel="limit-text">{{ $post->title }}</a></h5>
                    <p>{{ $post->full_location }}</p>
                </div>
                <div class="rs_product_div_footer">
                    <div class="rs_author_div">
                        <img src="http://placehold.it/30X30" class="img-responsive" alt="">
                        <div>
                            <h4><a href="{{ route('post.detail', ['slug' => $post->slug]) }}">{{ $post->user->getFullname() }}</a></h4>
                        </div>
                    </div>
                    <div class="rs_share">
                        <ul>
                            <li><a href="#"><i class="fa fa-eye"></i> <span>{{ $post->viewers }}</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end property item -->
            

@endif

