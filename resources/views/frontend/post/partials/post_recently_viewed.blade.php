@if (!empty($viewedPosts))
	<div class="rs_recentlyview_slider_section rs_bottompadder80">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="rs_main_heading rs_pink_heading rs_bottompadder60">
						<h3>Recently Viewed Properties</h3>
						<div><span><i class="fa fa-heart"></i></span></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="rs_recentlyview_slider rs_bottompadder60">
					<div id="owl-demo" class="owl-carousel owl-theme">
						@foreach($viewedPosts as $key => $post)
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									@include('frontend.post.partials.post_item', ['post' => $post])
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

@endif