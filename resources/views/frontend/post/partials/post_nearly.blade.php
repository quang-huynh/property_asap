<div class="rs_recentlyview_slider_section rs_toppadder100">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="rs_main_heading rs_pink_heading rs_bottompadder60">
					<h3>Nearly Properties</h3>
					<div><span><i class="fa fa-heart"></i></span></div>
				</div>
			</div>
		</div>
		<div class="row">
			@if (count($nearlyPosts) > 0)
				<div class="rs_recentlyview_slider rs_bottompadder60">
					<div id="owl-demo" class="owl-carousel owl-theme">
						@foreach($nearlyPosts as $key => $post)
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									@include('frontend.post.partials.post_item', ['post' => $post])
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@else
				<h3 class="text-center">No item.</h3>
			@endif
		</div>
	</div>
</div>