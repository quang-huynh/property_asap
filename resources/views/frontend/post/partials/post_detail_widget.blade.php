<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
	<div class="rs_sidebar_wrapper_fourth">

		@if ( !empty(Auth::user()))
			@if ($post->getCreatedBy() == Auth::user()->id)
				<aside class="widget widget_authors">
					<h4 class="widget-title">Owner Setting</h4>
					@if ($post->status != config('asap.post_status')['closed'])
						<a href="{{ route('post.edit', ['slug' => $post->slug]) }}" class="rs_button rs_button_orange margin-top-30">Edit</a>
						<a href="{{ route('post.close', ['slug' => $post->slug]) }}" class="rs_button rs_button_orange margin-top-30" onclick="return confirm('Warning: You must contact to us if you want reopen this property. Want to close?')">Close Property</a>
					@endif
					<p>Want delete property? Click <a id="delete-btn" class="delete-btn" href="{{ route('post.delete', ['id' => $post->id]) }}" onclick="return confirm('Warning: You cannot restore this property. Want to delete?')"><b>here</b></a></p>
				</aside>
			@endif
		@endif
		
		
		<aside class="widget widget_authors">
			<h4 class="widget-title">About Author</h4>
			<img src="http://placehold.it/61X60" alt="">
			<h5>{{ $post->user->getFullName() }}</h5>
			<p>Address: {{ $post->user->getAddress() }}</p>
			<p>Phone Number: {{ $post->user->getPhoneNumber() }}</p>
		</aside>

		
		<aside class="widget widget_Share">
			<h4 class="widget-title">Share this</h4>
			<ul class="widget_Share_first">
				<div id="sharePopup-widget"></div>
			</ul>
		</aside>
		
		<aside class="widget widget_meta_attributese">
			<h4 class="widget-title">product info</h4>
			<dl>
				<dt>Status:</dt>
				<dd>@if ($post->type == 1) {{ 'Sell' }} @else {{ 'Rent' }} @endif</dd>      
				<dt>Price:</dt>
				<dd>${{ $post->price }}</dd>    
				<dt>Type</dt>
				@foreach (config('asap.property_type_value') as $key => $value)
					@if ($value == $post->property_type)
						<dd>{{ config('asap.property_type_label')[$key] }}</dd>
					@endif
				@endforeach
						 
				<dt>Bedrooms</dt>
				@foreach (config('asap.bedrooms') as $key => $value)
					@if ($value == $post->bedrooms)
						<dd>{{ $key }}</dd>
					@endif
				@endforeach
							 
				<dt>Bathrooms</dt>
				@foreach (config('asap.bathrooms') as $key => $value)
					@if ($value == $post->bathrooms)
						<dd>{{ $key }}</dd>
					@endif
				@endforeach
							 
				<dt>Car spaces</dt>
				@foreach (config('asap.car_spaces') as $key => $value)
					@if ($value == $post->car_spaces)
						<dd>{{ $key }}</dd>
					@endif
				@endforeach
				
				<dt>Land sizes (m<sup>2</sup>)</dt>
				<dd>{{ $post->size }}</dd>
			</dl>
			<p><span>Tags: </span> 
				@foreach ($tags as $key => $tag)
						<a href="{{ route('post.search', ['tags' => $tag->id]) }}">{{ $tag['attributes']['name'] }}</a>,
				@endforeach
			</p>
		</aside>


		<aside class="widget widget_product">
			<h4 class="widget-title">Hot Properties</h4>
			@if (count($featurePosts) > 0)
				@foreach($featurePosts as $key => $post)
					@include('frontend.post.partials.post_item', ['post' => $post])
				@endforeach
			@endif
		</aside>


	</div>
</div>