@extends('frontend.layouts.master')

@section('title', 'Edit Property')

@section('fb_title', $post->title)
@section('fb_description', $post->description)
@section('fb_url', Request::url())

<?php 
	if ( count($post->media()->getResults()) > 0 ) {
		$fb_image = asset(config('asap.upload_post_url') . $post->media()->getResults()[0]->name);
	} else {
		$fb_image = asset('assets/common/images/' . config('asap.default_post_picture_name_path'));
	}
?>

@section('fb_image', $fb_image)

@section('content')


    <div class="rs_graybg rs_toppadder100 rs_bottompadder100 q-padding-top-100">
		<div class="container">


			<form id="form-update-post" action="{{ route('post.edit', ['slug' => $post->slug]) }}" method="POST">
				{{ csrf_field() }}

				<input type="hidden" id="id" name="id" value="{{ $post->id }}" />

				<div id="mess-update" class="alert alert-success" role="alert" style="display: none;"></div>

				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<div class="row">
								<div class="rs_tab_wrapper">
									 <div class="rs_left_tabwrapper">
										<ul class="nav nav-tabs">
										  <li id="tabs-step-1" class="active"><span></span><a href="#step-1" data-toggle="tab">Edit</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<div class="row">
								<div class="rs_user_dashboard_tab">
									<div class="tab-content">



										<div class="tab-pane active" id="step-1">


											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Location</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">

												<div class="rs_contact_form">

													<div id="mess-step-1" class="alert alert-warning" role="alert" style="display: none;"></div>

													<div class="rs_submitform">
														<div class="form-group">
															<label>Current Location:</label>
															<input class="form-control" value="{{ $post->full_location }}" disabled="disabled" />
															<p></p>
															<label>Location: </label>
															<input id="autocomplete-update" type="text" class="form-control" name="full_location" value="" onFocus="geolocate()" placeholder="Enter your property location">
															<div id="tutorial-for-autocomplete-update-post" class="alert alert-info" role="alert">If you want <b>change</b> location. Please enter new location and wait a few seconds. We'll suggest for you</div>
														</div>
													</div>

													<div id="tableLocation" class="rs_submitform">
														<h4 class="text-center">Preview location</h4>
														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Number: </label>
																	<input type="text" id="street_number" class="form-control" name="location_number_street" value="">
																</div>
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Street: </label>
																	<input type="text" id="route" class="form-control" name="location_street" value="">
																</div>

															</div>
														</div>

														<div class="form-group">
															<label>Suburb: </label>
															<input type="text" id="locality" class="form-control" name="location_city" value="" disabled="true" readonly="readonly">
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>State: </label>
																	<input type="text" id="administrative_area_level_1" class="form-control" name="location_state" value="" disabled="true" readonly="readonly">
																</div>
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Country: </label>
																	<input type="text" id="country" class="form-control" name="location_country" value="" disabled="true" readonly="readonly">
																</div>

															</div>
														</div>

													</div>
													
												</div>
											</div>


											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Options</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_contact_form">
													<div class="rs_submitform">

														


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Type:</label>
																	<select id="type" name="type" class="form-control">
																		<option value="1" @if($post->type == 1) {{ 'selected="selected"'}} @endif>Sell</option>
																		<option value="2" @if($post->type == 2) {{ 'selected="selected"'}} @endif>Rent</option>
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Property Type: </label>
																	<select id="property-type" name="property_type" class="form-control">
																		@foreach(config('asap.property_type_label') as $key => $pt)
																			<option value="{{ config('asap.property_type_value')[$key] }}" @if($post->property_type == config('asap.property_type_value')[$key]) selected="selected" @endif>{{ $pt }}</option>
																		@endforeach	
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Bedrooms: </label>
																	<select id="bedrooms" name="bedrooms" class="form-control">
																		@foreach(config('asap.bedrooms') as $key => $pt)
																			<option value="{{ config('asap.bedrooms')[$key] }}" @if($post->bedrooms == config('asap.bedrooms')[$key]) selected="selected" @endif>{{ $key }}</option>
																		@endforeach	
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Bathrooms: </label>
																	<select id="bathrooms" name="bathrooms" class="form-control">
																		@foreach(config('asap.bathrooms') as $key => $pt)
																			<option value="{{ config('asap.bathrooms')[$key] }}" @if($post->bathrooms == config('asap.bathrooms')[$key]) selected="selected" @endif>{{ $key }}</option>
																		@endforeach	
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Car spaces: </label>
																	<select id="car-spaces" name="car_spaces" class="form-control">
																		@foreach(config('asap.car_spaces') as $key => $pt)
																			<option value="{{ config('asap.car_spaces')[$key] }}" @if($post->car_spaces == config('asap.car_spaces')[$key]) selected="selected" @endif>{{ $key }}</option>
																		@endforeach	
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Land sizes (m<sup>2</sup>): </label>
																	<input id="size" name="size" class="form-control" value="{{ $post->size }}" />
																</div>
															</div>
														</div>


													</div>
												</div>

											</div>

											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Detail</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_contact_form">
													<div class="rs_submitform">

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Title<span class="red-symbol">&#42;</span>: </label>
																	<input id="title" name="title" value="{{ $post->title }}" class="form-control" />
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Description<span class="red-symbol">&#42;</span>: </label>
																	<textarea id="description" name="description" class="form-control" value="" rows="10" id="description" placeholder="Description">{{ $post->description }}</textarea>
																</div>
															</div>
														</div>

													</div>
												</div>
											</div>


											<div class="rs_user_dashboard_tab_heading">
												<h4>Contact information</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">

												<div class="rs_contact_form">
													<div class="rs_submitform">

														

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Name<span class="red-symbol">&#42;</span>: </label>
																	<input id="name-contact" name="name_contact" class="form-control" value="{{ $post->name_contact }}" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Email<span class="red-symbol">&#42;</span>: </label>
																	<input id="email-contact" name="email_contact" class="form-control" value="{{ $post->email_contact }}" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Address<span class="red-symbol">&#42;</span>: </label>
																	<input id="address-contact" name="address_contact" class="form-control" value="{{ $post->address_contact }}" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Phone Number<span class="red-symbol">&#42;</span>: </label>
																	<input id="phone-number" name="phone_number" class="form-control" value="{{ $post->phone_number }}" rel="keypress-only-number" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Home Phone<span class="red-symbol">&#42;</span>: </label>
																	<input id="home-number" name="home_number" class="form-control" value="{{ $post->home_number }}" rel="keypress-only-number" required />
																</div>
															</div>
														</div>


													</div>
												</div>

											</div>

											<div class="rs_btn_div rs_toppadder30">
												<a href="javascript:;" id="update-post-btn" class="rs_button rs_button_orange" data-url="{{ route('api.post.update') }}">Save</a>
											</div>

										</div>



									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>

@endsection




@section('css')
@parent

	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap-tagsinput.css') }}">

	<style>
		#tableLocation {
			display: none;
		}
	</style>

@endsection



@section('js')
@parent
	<script src="{{ asset('assets/frontend/js/bootstrap-tagsinput.min.js') }}"></script>

<script>
	// This example displays an address form, using the autocomplete feature
	// of the Google Places API to help users fill in the information.

	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	var placeSearch, autocomplete;
	var componentForm = {
		street_number: 'short_name',
		route: 'long_name',
		locality: 'long_name',
		administrative_area_level_1: 'long_name',
		country: 'long_name',
		// postal_code: 'short_name'
	};

	function initAutocomplete() {
	// Create the autocomplete object, restricting the search to geographical
	// location types.
	autocomplete = new google.maps.places.Autocomplete(
		/** @type {!HTMLInputElement} */(document.getElementById('autocomplete-update')),
		{
			// types: ['geocode'],
			componentRestrictions: {country: 'au'}
		}
	);

	// When the user selects an address from the dropdown, populate the address
	// fields in the form.
	autocomplete.addListener('place_changed', fillInAddress);
	}

	function fillInAddress() {
	// Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();

	for (var component in componentForm) {
		document.getElementById(component).value = '';
		document.getElementById(component).disabled = false;
	}

	// Get each component of the address from the place details
	// and fill the corresponding field on the form.
	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		if (componentForm[addressType]) {
			var val = place.address_components[i][componentForm[addressType]];
			document.getElementById(addressType).value = val;
		}
	}
	}

	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}

	$( "#autocomplete-update" ).autocomplete({
	  	change: function( event, ui ) {
	  		$('#tableLocation').fadeIn();
	  	}
	});

	$('#tags').tagsinput();

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKnSRmTT7X6CJ9MnQ9u0gn2hrAJrbV2I&libraries=places&callback=initAutocomplete" async defer></script>

@endsection





