@extends('frontend.layouts.master')

@section('title', $post->title)

@section('fb_title', $post->title)
@section('fb_description', $post->description)
@section('fb_url', Request::url())

<?php 
	if ( count($post->media()->getResults()) > 0 ) {
		$fb_image = asset(config('asap.upload_post_url') . $post->media()->getResults()[0]->name);
	} else {
		$fb_image = asset('assets/common/images/' . config('asap.default_post_picture_name_path'));
	}
?>

@section('fb_image', $fb_image)



@section('content')

	<div class="post-detail">



		<!--Breadcrumb start-->
		<div class="rs_pagetitle rs_toppadder40 rs_bottompadder40">
			<div class="rs_overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="page_title">
							<h3 class="rs_bottompadder20">{{ $post->title }}</h3>
						</div>
						
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="property-detail">
							<h5>Location : {{ $post->full_location }}</h5>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!--Breadcrumb end-->





		<div class="rs_graybg rs_toppadder60 rs_bottompadder70">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">


						<div class="ribbon price-tag wow bounceInDown" data-wow-delay="2s" >
							@if ($post->status == config('asap.post_status')['closed'])
		                        <p class="price">CLOSED</p>
		                    @else
		                        <p class="status">@if ($post->type == 1) {{ 'Sell' }} @else {{ 'Rent' }} @endif</p>
			                	<p class="price">${{ $post->price }}</p>
		                    @endif
								
		                </div>

						<div class="rs_single_product_slider @if( $post->status == config('asap.post_status')['pending'] ) {{ 'pending-post' }} @endif">
							@if( $post->status == config('asap.post_status')['pending'] )
								<div class="post-pending-galleries-bg">
									<div class="text">
										<img width="150" height="150" src="{{ asset('assets/common/images/warning-404.png') }}">
										<h3>Pending photos</h3>
										<p>The galleries needs to be approved before display.</p>
									</div>
								</div>
							@endif
							@if ( count($post->media()->getResults()) > 0 )
								<ul class="pgwSlideshow">
									@foreach ($post->media()->getResults() as $key => $item)
										<li><img src="{{ asset(config('asap.upload_post_url') . $item->name) }}" alt=""></li>
									@endforeach
								</ul>
							@else
								<ul class="pgwSlideshow">
									<li><img src="{{ asset(config('asap.default_detail_post')) }}" alt=""></li>
								</ul>
							@endif
						</div>
						<div class="rs_single_product_btn_section rs_toppadder30 rs_bottompadder30">
							<div class="row">
								@if(!$postService->hasWishList($post->id))
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="rs_btn_div">
										<a href="javascript:void(0)" id="action-wishlist" data-id="{{ $post->id }}" data-status="0" class="rs_button rs_button_orange">Add to Wishlist</a>
									</div>
								</div>
								@else
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="rs_btn_div">
										<a href="javascript:void(0)" id="action-wishlist" data-id="{{ $post->id }}" data-status="1" class="rs_button rs_button_orange">Remove from Wishlist</a>
									</div>
								</div>
								@endif
								<div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
									<div class="rs_btn_div">
										<a href="#" class="rs_button rs_button_orange" data-toggle="modal" data-target="#contact-info">Contact Now</a>
									</div>
								</div>
							</div>
						</div>

						<div class="rs_buy_license_section">
							<p>{{ $post->description }}</p>
							
							<div class="rs_social">
								<ul>
									<li>Share:</li>
									<li>
										<div id="sharePopup"></div>
									</li>
								</ul>
							</div>
							<!-- <div class="rs_btn_div">
								<a href="#" class="rs_button rs_button_orange">Contact</a>
							</div> -->
						</div>


						@if ($post->package == 3 && !empty($post->video_link))
							<div class="rs_buy_license_section video-box">
								<h5>Video:</h5>
								<iframe width="560" height="315" src="@if(!empty($post['video_link'])) {{ $post['video_link'] }} @endif" frameborder="0" allowfullscreen></iframe>
							</div>
						@endif

					</div>




					<!-- ----------------------WIDGET------------------------------------------- -->
					@include('frontend.post.partials.post_detail_widget', ['postService' => $postService, 'post' => $post, 'featurePosts' => $featurePosts, 'tags' => $tags])

				</div>
			</div>
		</div>




		@include('frontend.post.partials.post_nearly', ['postService' => $postService, 'nearlyPosts' => $nearlyPosts])

		@include('frontend.post.partials.post_recently_viewed', ['postService' => $postService, 'viewedPosts' => $viewedPosts])



		<div class="modal fade" id="contact-info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel">Contact Info</h4>
					</div>
					<div class="modal-body rs_contact_form">
						<form class="rs_submitform">
							<div class="form-group">
								<label for="name-contact" class="control-label">Name</label>
								<input type="text" id="name-contact" class="form-control" value="{{ $post->name_contact }}" disabled="disabled">
							</div>
							<div class="form-group">
								<label for="email-contact" class="control-label">Email</label>
								<input type="text" id="email-contact" class="form-control" value="{{ $post->email_contact }}" disabled="disabled">
							</div>
							<div class="form-group">
								<label for="address-contact" class="control-label">Address</label>
								<input type="text" id="address-contact" class="form-control" value="{{ $post->address_contact }}" disabled="disabled">
							</div>
							<div class="form-group">
								<label for="phone-number" class="control-label">Phone Number</label>
								<input type="text" id="phone-number" class="form-control" value="{{ $post->phone_number }}" disabled="disabled">
							</div>
							<div class="form-group">
								<label for="home-number" class="control-label">Home Number</label>
								<input type="text" id="home-number" class="form-control" value="{{ $post->home_number }}" disabled="disabled">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>



	</div>

@endsection



@section('css')
@parent


@endsection



@section('js')
@parent
<script src="{{ asset('assets/frontend/js/wow.min.js') }}" type="text/javascript"></script>
<script>
	new WOW().init();
</script>


@endsection




