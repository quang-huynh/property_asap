@extends('frontend.layouts.master')

@section('title', 'Create Property')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))





@section('content')


    <div class="rs_graybg rs_toppadder100 rs_bottompadder100 q-padding-top-100">
		<div class="container">


			<div id="success-box" class="alert alert-success q-success-box">
				<div class="notify successbox">
			        <h1>Success !</h1>
			        <span class="alerticon"><img src="{{ asset('assets/frontend/images/check.png') }}" alt="checkmark" /></span>
			        <p>{{ config('asap.thank_you_create_post') }}</p>
			        <a id="preview-property-btn" href="{{ route('post.edit', array('id' => '1')) }}" class="rs_button rs_button_green" data-text="Edit Post" target="_blank"><span>Preview Property</span></a>
		      	</div>

			</div>

			<form id="form-create-post" action="{{ route('post.create') }}" method="POST">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<div class="row">
								<div class="rs_tab_wrapper">
									 <div class="rs_left_tabwrapper">
										<ul class="nav nav-tabs">
										  <li id="tabs-step-1" class="active"><span><i class="fa fa-map-marker"></i></span><a href="#step-1" data-toggle="tab">Step 1: Property Location</a></li>
										  <li id="tabs-step-2"><span><i class="fa fa-pencil"></i></span><a href="#step-2" data-toggle="tab">Step 2: Property Description</a></li>
										  <li id="tabs-step-3"><span><i class="fa fa-info-circle"></i></span><a href="#step-3" data-toggle="tab">Step 3: Contact information</a></li>
										  <li id="tabs-step-4"><span><i class="fa fa-cloud-upload"></i></span><a href="#step-4" data-toggle="tab">Step 4: Choose package</a></li>
										  <li id="tabs-step-5"><span><i class="fa fa-cogs"></i></span><a href="#step-5" data-toggle="tab">Step 5: Finish</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<div class="row">
								<div class="rs_user_dashboard_tab">
									<div class="tab-content">



										<div class="tab-pane active" id="step-1">
											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Location</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">

												<div class="rs_contact_form">

													<div id="mess-step-1" class="alert alert-warning" role="alert" style="display: none;"></div>

													<div class="rs_submitform">
														<div class="form-group">
															<label>Location: </label>
															<input id="autocomplete" type="text" class="form-control" name="full_location" onFocus="geolocate()" placeholder="Enter your property location">
															<div id="tutorial-for-autocomplete-create-post" class="alert alert-info" role="alert">Please enter your property location and wait a few seconds. We'll suggest for you</div>
														</div>
													</div>

													<div id="tableLocation" class="rs_submitform">
														<h4 class="text-center">Preview location</h4>
														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Number: </label>
																	<input type="text" id="street_number" class="form-control" name="location_number_street" value="">
																</div>
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Street: </label>
																	<input type="text" id="route" class="form-control" name="location_street" value="">
																</div>

															</div>
														</div>

														<div class="form-group">
															<label>Suburb: </label>
															<input type="text" id="locality" class="form-control" name="location_city" value="" disabled="true" readonly="readonly">
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>State: </label>
																	<input type="text" id="administrative_area_level_1" class="form-control" name="location_state" value="" readonly="readonly">
																</div>
																<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<label>Country: </label>
																	<input type="text" id="country" class="form-control" name="location_country" readonly="readonly">
																</div>

															</div>
														</div>

														<div class="rs_btn_div rs_toppadder30">
															<a href="javascript:void(0)" id="btn-continue-step-1" class="rs_button rs_button_orange">Next Step 2</a>
														</div>
													</div>
													
												</div>
											</div>
										</div>


										<div class="tab-pane" id="step-2">
											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Options</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_contact_form">
													<div class="rs_submitform">

														<div id="mess-step-2" class="alert alert-warning" role="alert" style="display: none;"></div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Type:</label>
																	<select id="type" name="type" class="form-control">
																		<option value="1">Sell</option>
																		<option value="2">Rent</option>
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Property Type: </label>
																	<select id="property-type" name="property_type" class="form-control">
																		@foreach(config('asap.property_type_label') as $key => $value)
																			<option value="{{ config('asap.property_type_value')[$key] }}">{{ $value }}</option>
																		@endforeach
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Bedrooms: </label>
																	<select id="bedrooms" name="bedrooms" class="form-control">
																		@foreach(config('asap.bedrooms') as $key => $value)
																			<option value="{{ $value }}">{{ $key }}</option>
																		@endforeach
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Bathrooms: </label>
																	<select id="bathrooms" name="bathrooms" class="form-control">
																		@foreach(config('asap.bathrooms') as $key => $value)
																			<option value="{{ $value }}">{{ $key }}</option>
																		@endforeach
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Car spaces: </label>
																	<select id="car-spaces" name="car_spaces" class="form-control">
																		@foreach(config('asap.car_spaces') as $key => $value)
																			<option value="{{ $value }}">{{ $key }}</option>
																		@endforeach
																	</select>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Land sizes (m<sup>2</sup>): </label>
																	<input id="size" name="size" class="form-control" />
																</div>
															</div>
														</div>


													</div>
												</div>

											</div>

											<div class="rs_user_dashboard_tab_heading">
												<h4>Property Detail</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_contact_form">
													<div class="rs_submitform">

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Title<span class="red-symbol">&#42;</span>: </label>
																	<input id="title" name="title" class="form-control" />
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Description<span class="red-symbol">&#42;</span>: </label>
																	<textarea id="description" name="description" class="form-control" rows="10" id="description" placeholder="Description"></textarea>
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Price<span class="red-symbol">&#42;</span>: </label>
																	<input id="price" name="price" class="form-control" rel="keypress-only-number" />
																</div>
															</div>
														</div>


														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Tags: </label>
																	<p>
																		<input id="tags" name="tags" class="form-control" value="" data-role="tagsinput" />
																	</p>
																</div>
															</div>
														</div>


													</div>
												</div>
											</div>


											<!-- <div class="rs_user_dashboard_tab_heading">
												<h4>Images</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												
											</div> -->

											<div class="rs_btn_div rs_toppadder30">
												<a href="javascript:;" id="btn-continue-step-2" class="rs_button rs_button_orange">Next Step 3</a>
											</div>

										</div>


										<div class="tab-pane" id="step-3">
											<div class="rs_user_dashboard_tab_heading">
												<h4>Contact information</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">

												<div class="rs_contact_form">
													<div class="rs_submitform">

														<div id="mess-step-3" class="alert alert-warning" role="alert" style="display: none;"></div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Name<span class="red-symbol">&#42;</span>: </label>
																	<input id="name-contact" name="name_contact" value="{{ @Auth::user()->fullname }}" class="form-control" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Email<span class="red-symbol">&#42;</span>: </label>
																	<input id="email-contact" name="email_contact" value="{{ @Auth::user()->email }}" class="form-control" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Address<span class="red-symbol">&#42;</span>: </label>
																	<input id="address-contact" name="address_contact" value="{{ @Auth::user()->address }}" class="form-control" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Phone Number<span class="red-symbol">&#42;</span>: </label>
																	<input id="phone-number" name="phone_number" class="form-control" value="{{ @Auth::user()->phone_number }}" rel="keypress-only-number" required />
																</div>
															</div>
														</div>

														<div class="form-group">
															<div class="row">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<label>Home Phone<span class="red-symbol">&#42;</span>: </label>
																	<input id="home-number" name="home_number" class="form-control" value="" rel="keypress-only-number" required />
																</div>
															</div>
														</div>


													</div>
												</div>

											</div>

											<div class="rs_btn_div rs_toppadder30">
												<a href="javascript:;" id="btn-continue-step-3" class="rs_button rs_button_orange">Next Step 4</a>
											</div>

										</div>


										<div class="tab-pane" id="step-4">
											<div class="rs_user_dashboard_tab_heading">
												<h4>Choose Package</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_user_dashboard_tab_info_form">

													<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
														<div class="rs_pricingtable">
															<div class="rs_pricingtable_header rs_toppadder20 rs_bottompadder20">
															<h4>Premium</h4>
															</div>
															<div class="rs_pricingtable_price rs_toppadder20 rs_bottompadder20">
																<h1><sup>$</sup>39</h1>
																<p>"Get your ad to start out"</p>
															</div>
															<ul>
																<li>- Edit up to 5 photos</li>
																<li>- Flag</li>
																<li>- Email to Property Asap</li>
															</ul>
															<div class="rs_pricingtable_footer rs_toppadder20 rs_bottompadder20">
																<input type="radio" id="package" name="package[]" value="1" class="" checked>
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
														<div class="rs_pricingtable">
															<div class="rs_pricingtable_header rs_toppadder20 rs_bottompadder20">
															<h4>Platinum</h4>
															</div>
															<div class="rs_pricingtable_price rs_toppadder20 rs_bottompadder20">
																<h1><sup>$</sup>79</h1>
																<p>"New look and feel"</p>
															</div>
															<ul>
																<li>- Edit up to 7 photos</li>
																<li>- FLag</li>
																<li>- Large image</li>
																<li>- Email to Property Asap</li>
																<li>- Phone to Property Asap</li>
															</ul>
															<div class="rs_pricingtable_footer rs_toppadder20 rs_bottompadder20">
																<input type="radio" id="package" name="package[]" value="2" class="">
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
														<div class="rs_pricingtable">
															<div class="rs_pricingtable_header rs_toppadder20 rs_bottompadder20">
															<h4>Ultimate</h4>
															</div>
															<div class="rs_pricingtable_price rs_toppadder20 rs_bottompadder20">
																<h1><sup>$</sup>99</h1>
																<p>"The best chance of success"</p>
															</div>
															<ul>
																<li>- Edit up to 10 photos</li>
																<li>- 30 second video</li>
																<li>- Flag</li>
																<li>- Large image</li>
																<li>- Top add for 14 days</li>
																<li>- Email to Property Asap</li>
																<li>- Phone to Property Asap</li>
																<li>- Agent help to negotiate</li>
															</ul>
															<div class="rs_pricingtable_footer rs_toppadder20 rs_bottompadder20">
																<input type="radio" id="package" name="package[]" value="3" class="">
															</div>
														</div>
													</div>
													
												</div>
											</div>

											<div class="rs_btn_div rs_toppadder30">
												<a href="javascript:;" id="btn-continue-step-4" class="rs_button rs_button_orange">Choose</a>
											</div>

										</div>


										<div class="tab-pane" id="step-5">
											<div class="rs_user_dashboard_tab_heading">
												<h4>Finish</h4>
											</div>
											<div class="rs_user_dashboard_tab_info">
												<div class="rs_user_dashboard_tab_info_form">

													<div id="mess-step-5" class="alert alert-warning" role="alert" style="display: none;"></div>

													<div class="checkbox">
													  	<label><input id="agree-checkbox" type="checkbox" value="" required>I agree the <a href="{{ route('terms') }}" target="_blank">Terms and Conditions</a></label>
													</div>

													<div class="rs_btn_div rs_toppadder30">
														<a href="javascript:;" id="create-post-btn" class="rs_button rs_button_orange" data-url="{{ route('api.post.create') }}">Finish</a>
													</div>
												</div>
											</div>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>

@endsection




@section('css')
@parent

	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap-tagsinput.css') }}">

	<style>
		#tableLocation {
			display: none;
		}
	</style>

@endsection



@section('js')
@parent
	<script src="{{ asset('assets/frontend/js/bootstrap-tagsinput.min.js') }}"></script>

<script>
	// This example displays an address form, using the autocomplete feature
	// of the Google Places API to help users fill in the information.

	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	var placeSearch, autocomplete;
	var componentForm = {
		street_number: 'short_name',
		route: 'long_name',
		locality: 'long_name',
		administrative_area_level_1: 'long_name',
		country: 'long_name',
		// postal_code: 'short_name'
	};

	function initAutocomplete() {
	// Create the autocomplete object, restricting the search to geographical
	// location types.
	autocomplete = new google.maps.places.Autocomplete(
		/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
		{
			// types: ['geocode'],
			componentRestrictions: {country: 'au'}
		}
	);

	// When the user selects an address from the dropdown, populate the address
	// fields in the form.
	autocomplete.addListener('place_changed', fillInAddress);
	}

	function fillInAddress() {
	// Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();

	for (var component in componentForm) {
		document.getElementById(component).value = '';
		document.getElementById(component).disabled = false;
	}

	// Get each component of the address from the place details
	// and fill the corresponding field on the form.
	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		if (componentForm[addressType]) {
			var val = place.address_components[i][componentForm[addressType]];
			document.getElementById(addressType).value = val;
		}
	}
	}

	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}

	$( "#autocomplete" ).autocomplete({
	  	change: function( event, ui ) {
	  		$('#tableLocation').fadeIn();
	  	}
	});

	$('#tags').tagsinput();

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKnSRmTT7X6CJ9MnQ9u0gn2hrAJrbV2I&libraries=places&callback=initAutocomplete" async defer></script>

@endsection





