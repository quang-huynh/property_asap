@extends('frontend.layouts.master')

@section('title', 'Privacy')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))

@section('content')


	<h3 class="text-center">Privacy Policy</h3>


	<section class="privacy-page">
		
		<div class="container">

			<h3>
			    INTRODUCTION
			</h3>
			<p>
			    We take our privacy obligations seriously and we’ve created this privacy
			    policy to explain how we store, maintain, use and disclose personal
			    information. It should be read together with our Terms of Use.
			</p>
			<p>
			    By providing personal information to us, you consent to our storage,
			    maintenance, use and disclosing of personal information in accordance with
			    this privacy policy.
			</p>
			<p>
			    We may change this privacy policy from time to time by posting an updated
			    copy on our website and we encourage you to check our website regularly to
			    ensure that you are aware of our most current privacy policy.
			</p>
			<h3>
			    TYPES OF PERSONAL INFORAMTION WE COLLECT
			</h3>
			<p>
			    The personal information we collect may include the following:
			</p>
			<ul>
			    <li>
			        name;
			    </li>
			    <li>
			        mailing or street address;
			    </li>
			    <li>
			        email address;
			    </li>
			    <li>
			        social media information;
			    </li>
			    <li>
			        telephone number and other contact details;
			    </li>
			    <li>
			        age;
			    </li>
			    <li>
			        date of birth;
			    </li>
			    <li>
			        credit card information;
			    </li>
			    <li>
			        information about your business or personal circumstances;
			    </li>
			</ul>
			<p>
			    · information in connection with client surveys, questionnaires and
			    promotions;
			</p>
			<p>
			    · your device identity and type, I.P. address, geo-location information,
			    page view statistics, advertising data and standard web log information;
			</p>
			<ul>
			    <li>
			        information about third parties; and
			    </li>
			</ul>
			<p>
			    · any other information provided by you to us via this website or our
			    online presence, or otherwise required by us or provided by you.
			</p>
			<h3>
			    HOW WE COLLECT PERSONAL INFORMATION
			</h3>
			<p>
			    We may collect personal information either directly from you, or from third
			    parties, including where you:
			</p>
			<ul>
			    <li>
			        contact us through on our website;
			    </li>
			</ul>
			<p>
			    · communicate with us via email, telephone, SMS, social applications (such
			    as LinkedIn, Facebook or Twitter) or otherwise;
			</p>
			<p>
			    · interact with our website, social applications, services, content and
			    advertising; and
			</p>
			<p>
			    · invest in our business or enquire as to a potential purchase in our
			    business.
			</p>
			<p>
			    We may also collect personal information from you when you use or access
			    our website or our social media pages. This may be done through use of web
			    analytics tools, 'cookies' or other similar tracking technologies that
			    allow us to track and analyse your website usage. Cookies are small files
			    that store information on your computer, mobile phone or other device and
			    enable and allow the creator of the cookie to identify when you visit
			    different websites. If you do not wish information to be stored as a
			    cookie, you can disable cookies in your web browser.
			</p>
			<h3>
			    USE OF YOUR PERSONAL INFORMATION
			</h3>
			<p>
			    We collect and use personal information for the following purposes:
			</p>
			<ul>
			    <li>
			        to provide services or information to you;
			    </li>
			    <li>
			        for record keeping and administrative purposes;
			    </li>
			</ul>
			<p>
			    · to provide information about you to our contractors, employees,
			    consultants, agents or other third parties for the purpose of providing
			    services to you;
			</p>
			<ul>
			    <li>
			        to improve and optimise our service offering and customer experience;
			    </li>
			</ul>
			<p>
			    · to comply with our legal obligations, resolve disputes or enforce our
			    agreements with third parties;
			</p>
			<p>
			    · to send you marketing and promotional messages and other information that
			    may be of interest to you and for the purpose of direct marketing (in
			    accordance with the Spam Act). In this regard, we may use email, SMS,
			    social media or mail to send you direct marketing communications. You can
			    opt out of receiving marketing materials from us by using the opt-out
			    facility provided (e.g. an unsubscribe link);
			</p>
			<p>
			    · to send you administrative messages, reminders, notices, updates,
			    security alerts, and other information requested by you; and
			</p>
			<ul>
			    <li>
			        to consider an application of employment from you.
			    </li>
			</ul>
			<p>
			    We may disclose your personal information to cloud-providers, contractors
			    and other third parties located inside or outside of Australia. If we do
			    so, we will take reasonable steps to ensure that any overseas recipient
			    deals with such personal information in a manner consistent with how we
			    deal with it.
			</p>
			<h3>
			    SECURITY
			</h3>
			<p>
			    We take reasonable steps to ensure your personal information is secure and
			    protected from misuse or unauthorised access. Our information technology
			    systems are password protected, and we use a range of administrative and
			    technical measure to protect these systems. However, we cannot guarantee
			    the security of your personal information.
			</p>
			<h3>
			    LINKS
			</h3>
			<p>
			    Our website may contain links to other websites. Those links are provided
			    for convenience and may not remain current or be maintained. We are not
			    responsible for the privacy practices of those linked websites and we
			    suggest you review the privacy policies of those websites before using
			    them.
			</p>
			<h3>
			    REQUESTING ACCESS OR CORRECTING YOUR PERSONAL INFORMATION
			</h3>
			<p>
			    If you wish to request access to the personal information we hold about
			    you, please contact us using the contact details set out below including
			    your name and contact details. We may need to verify your identity before
			    providing you with your personal information. In some cases, we may be
			    unable to provide you with access to all your personal information and
			    where this occurs, we will explain why. We will deal with all requests for
			    access to personal information within a reasonable timeframe.
			</p>
			<p>
			    If you think that any personal information we hold about you is inaccurate,
			    please contact us using the contact details set out below and we will take
			    reasonable steps to ensure that it is corrected.
			</p>
			<h3>
			    COMPLAINTS
			</h3>
			<p>
			    If you wish to complain about how we handle your personal information or
			    held by us, please contact us using the details set out below including
			    your name and contact details. We will investigate your complaint promptly
			    and respond to you within a reasonable time.
			</p>
			<h3>
			    CONTACT US
			</h3>
			<p>
			    For further information about our privacy policy or practices, or to access
			    or correct your personal information, or make a complaint, please contact
			    us using the details set out below:
			</p>
			<p>
			    <strong>Email</strong>
			    : tony@propertyasap.com.au
			</p>
			<p>
			    Our privacy policy was last updated on <strong>propertyasap.com.au</strong>
			    .
			</p>

		</div>

	</section>



@endsection




@section('css')
@parent

@endsection



@section('js')
@parent


@endsection





