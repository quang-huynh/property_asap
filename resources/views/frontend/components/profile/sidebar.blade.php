<div class="rs_tab_wrapper">
    <div class="rs_user_profile_img">
        <img src="http://placehold.it/285X92" alt="" class="img-responsive">
        <div class="rs_menu_pic_overlay"><h6 class="text-center">Dashboard</h6></div>
    </div>
    <div class="rs_left_tabwrapper">
        <ul class="nav nav-tabs">
            <li class="@if($routeName == 'frontend.profile.me') active @endif">
                <span><i class="fa fa-user"></i></span>
                <a href="{{ route('frontend.profile.me') }}">
                    Me
                </a>
            </li>
            <li class="@if($routeName == 'frontend.profile.edit') active @endif">
                <span><i class="fa fa-pencil-square" aria-hidden="true"></i></span>
                <a href="{{ route('frontend.profile.edit') }}">Edit</a>
            </li>
            <li class="@if($routeName == 'frontend.profile.change-password') active @endif">
                <span><i class="fa fa-envelope"></i></span>
                <a href="{{ route('frontend.profile.change-password') }}">Change password</a>
            </li>

            <li class="@if($routeName == 'frontend.profile.wishlist') active @endif">
                <span><i class="fa fa-star-o"></i></span>
                <a href="{{ route('frontend.profile.wishlist') }}">Wishlist</a>
            </li>

            <li class="@if($routeName == 'frontend.profile.properties') active @endif">
                <span><i class="fa fa-home"></i></span>
                <a href="{{ route('frontend.profile.properties') }}">My Properties</a>
            </li>


            <li>
                <span><i class="fa fa-power-off"></i></span>
                <a href="javascript:void()"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                   data-toggle="tab">Logout</a>
            </li>
        </ul>
    </div>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>