<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
    <label for="{{ $field }}">{{ $label }}</label>
    <input type="password" class="form-control" id="f-{{ $field }}" name="{{ $field }}" placeholder="{{ $placeholder }}"
           value="">
    @include('frontend.components.forms.error', ['field' => $field])
</div>