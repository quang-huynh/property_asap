<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
    <label for="{{ $field }}">{{ $label }}</label>
    <input type="text" class="form-control" id="f-{{ $field }}" name="{{ $field }}" placeholder="{{ $placeholder }}"
           value="{{ old($field, $default) }}">
    @include('frontend.components.forms.error', ['field' => $field])
</div>