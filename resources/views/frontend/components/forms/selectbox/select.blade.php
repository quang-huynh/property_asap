<div class="form-group has-feedback {{ $errors->has($field) ? 'has-error' : '' }}">
    <label for="status">{{ $label }}</label>
    <select name="{{ $field }}" id="f-{{ $field }}" class="form-control">
        @if(!empty($data))
            @foreach($data as $key => $item)
                <option value="{{ $key }}"
                        @if(!empty($default) && $default == $key) selected @endif>
                    {{ $item }}
                </option>
            @endforeach
        @else
            <option value="">Not found !</option>
        @endif
    </select>
    @include('frontend.components.forms.error', ['field' => $field])
</div>