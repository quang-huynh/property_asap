<header>
    <div class="rs_index2_topheader">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo">
                        <a href="{{ route('frontend.index') }}"><img src="{{ asset('assets/common/images/logo-white-s.png') }}" class="img-responsive" alt="logo"></a>
                    </div>
                </div>
                @include('frontend.partials.user_nav')
            </div>
        </div>
    </div>
</header>