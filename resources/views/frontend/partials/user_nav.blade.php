@if(auth()->check())
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 user-nav">
        <div class="rs_user_pic">
            <div>
                <h6>{{auth()->user()->fullname}}</h6>
            </div>
            <img src="{{ asset(auth()->user()->getProfilePicture()) }}" alt="">
            <i class="glyphicon glyphicon-option-vertical" aria-hidden="true"></i>
        </div>
        <div class="rs_user_profile">
            <ul>
                <li><a href="{{ route('frontend.profile.me') }}"><i class="fa fa-user"></i>Profile</a></li>
                <li><a href="{{ route('logout') }}"><i class="fa fa-download"></i> Sign Out</a></li>
            </ul>
        </div>
        <!-- <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><i class="fa fa-heart"></i> (28)</a></li>
            <li><a href="cart.html"><i class="fa fa-shopping-cart"></i> (2)</a></li>
        </ul> -->
    </div>
@else
    <div class="col-lg-4 col-md-8 col-sm-12 col-xs-12 pull-right">
        <div class="rs_index2_topheader_links">
            <ul>
                <li><a href="#" data-toggle="modal" data-target="#login_popup">Login</a></li>
                <li><a href="/register">Signup</a></li>
            </ul>
            <!-- Modal -->
            <div class="modal fade rs_mypopup" id="login_popup" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                  </div>
                  <div class="modal-body">
                    <div class="rs_popup_list">
                        <ul>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_1.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>30 days Money Back Guarantee</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_2.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>New Items Are Included in same subscribtions</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_3.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>24/7 Dedicated Customer Support</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_6.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>100% Secured Payment Gateway</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="rs_popup_form">
                        <div class="rs_popup_form_header">
                            <img src="{{ asset('assets/frontend/images/logo.png') }}" class="img-responsive" alt="">
                            <h4 class="text-uppercase">Login in to Property Asap</h4>
                        </div>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Enter your mail address">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <a href="{{url('/password/reset')}}">Forget your password?</a>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="rs_button rs_button_orange">Login</button>
                                <h4>or</h4>
                                <a href="{{ route('frontend.auth.facebook.callback') }}" class="rs_button rs_button_darkblue"><i class="fa fa-facebook"></i> Log in Via Facebook</a>
                                <h4>or</h4>
                                <a href="{{ route('frontend.auth.google.callback') }}" class="rs_button rs_button_red"><i class="fa fa-google"></i> Log in Via Google</a>
                            </div>
                        </form>
                        <p>Don’t have account yet? <a href="{{url('register')}}">signup <i class="fa fa-angle-double-right"></i></a></p>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            
            <div class="modal fade rs_mypopup rs_signup" id="signup_popup" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                  </div>
                  <div class="modal-body">
                    <div class="rs_popup_list">
                        <ul>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_1.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>30 days Money Back Guarantee</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_2.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>New Items Are Included in same subscribtions</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_3.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>24/7 Dedicated Customer Support</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                            <li>
                                <div class="rs_popup_list_img"><img src="{{ asset('assets/frontend/images/af_6.jpg') }}" alt="" class="img-responsive"></div>
                                <div class="rs_popup_list_data">
                                    <h5>100% Secured Payment Gateway</h5>
                                    <p>We are a fairly small, flexible design studio that designs for print and web. We work flexibly with clients</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="rs_popup_form">
                        <div class="rs_popup_form_header">
                            <img src="{{ asset('assets/frontend/images/logo.png') }}" class="img-responsive" alt="">
                            <h4 class="text-uppercase">Create Your Account</h4>
                        </div>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="User Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter your mail address">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="rs_form_checkbox">
                                <div class="rs_checkbox">
                                    <input type="checkbox" value="1" id="check4" name="checkbox">
                                    <label for="check4"></label>
                                </div>
                                <p>I'd like to hear about promos, new templates, and much more! </p>
                            </div>
                            <div class="rs_form_checkbox">
                                <div class="rs_checkbox">
                                    <input type="checkbox" value="1" id="check5" name="checkbox">
                                    <label for="check5"></label>
                                </div>
                                <p>By clicking "Sign up" you agree to our Terms of Service and Privacy Policy</p>
                            </div>
                            <div class="form-group">
                                <a href="#" class="rs_button rs_button_orange">Login</a>
                                <h4>or</h4>
                                <a href="#" class="rs_button rs_button_darkblue"><i class="fa fa-facebook"></i> Log in Via Facebook</a>
                            </div>
                        </form>
                        <p>Already have an account? <a href="#">login <i class="fa fa-angle-double-right"></i></a></p>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
        </div>
    </div>
@endif