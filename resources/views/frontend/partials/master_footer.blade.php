<footer>
            
    <div class="rs_topfooterwrapper rs_toppadder20 rs_bottompadder20">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_footersocial">
                        <h5>Connect with us</h5>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-apple"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-xing-square"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rs_footer rs_toppadder60 rs_bottompadder60">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="rs_footerdiv">
                        <img src="{{ asset('assets/common/images/logo-white.png') }}" class="img-responsive" alt="">
                        <p>We have a number of different teams within our agency that specialise in different areas of business.</p>
                        <a href="{{ route('about') }}" class="rs_readmore">Read more <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="rs_footerdiv">
                            <h5 class="rs_footerheading">Company Links</h5>
                            <ul>
                                <li><a href="{{ route('about') }}"><i class="fa fa-angle-double-right"> </i>About <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="{{ route('terms') }}"><i class="fa fa-angle-double-right"> </i>Terms <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="{{ route('privacy') }}"><i class="fa fa-angle-double-right"> </i>Privacy Policy <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Careers <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Site Maps<i class="fa fa-angle-double-right"> </i></a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="rs_footerdiv">
                            <h5 class="rs_footerheading"></h5>
                            <!-- <ul>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Open a Shop <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Become a Partner <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Free Goods <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Purchase Credits <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Gift Cards <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Branding ebook <i class="fa fa-angle-double-right"> </i></a></li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="rs_footerdiv">
                            <h5 class="rs_footerheading"></h5>
                            <!-- <ul>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Discussions <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Blog <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Members <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Products <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Help Center <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Collections <i class="fa fa-angle-double-right"> </i></a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"> </i>Meet up <i class="fa fa-angle-double-right"> </i></a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="rs_footerdiv widget_tag_cloud">
                        <h5 class="rs_footerheading">Popular Tags</h5>
                        @foreach ($popularTags as $key => $tag)
                            <a href="{{ route('post.search', ['tags' => $tag->id]) }}" class="ed_btn ed_orange">{{ $tag->name }}</a>   
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rs_bottomfooter rs_toppadder30 rs_bottompadder30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_copyright">
                        <p>&copy; <a href="/">Property Asap.</a> All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>