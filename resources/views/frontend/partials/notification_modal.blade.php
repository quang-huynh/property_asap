<div class="modal fade" id="notificationModal" role="dialog">
    <div class="modal-dialog" style="width: 40% !important;">
        <div class="modal-content" style="border-radius: 0 !important;">
            <div class="modal-body">
                <h2 class="modal-title text-center" id="notificationTitle">Notification</h2>
                <div class="clearfix">

                </div>
                <p class="text-center">
                    <button name="btnClose" type="button" class="btn btn-default btnClose" data-dismiss="modal">Close
                    </button>
                </p>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #notificationModal {
        top: 20%;
    }

    h2#notificationTitle {
        margin-top: 15px;
        margin-bottom: 30px;
    }

    #notificationModal button.btnClose {
        font-weight: bold;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        border: none;
        font-size: 18px;
        border-radius: 60px;
        padding: 0.5em 4em;
        text-decoration: none;
        display: inline-block;
        background: red;
    }

    #notificationModal button.btnClose:hover {
        background: #ff4040;
    }
</style>