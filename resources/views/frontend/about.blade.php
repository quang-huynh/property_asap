@extends('frontend.layouts.master')

@section('title', 'About')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))

@section('content')


	<div class="container">

		<div id="top-image" style="background:url({{ asset('assets/common/images/about_us_cover.jpg') }})"></div>

		<h1 class="text-center">Property ASAP   - You’re in Control!</h1>

		<div class="row description">
			<div class="col-md-12">
				<p></p>
				<p>
					Property ASAP welcomes you to the future.  The future of a seamless life, powered by the internet!  The internet has revolutionised many industries and real estate is next!  We are here to simplify the selling process, whilst maximising your profits.  We remove those hidden fees and offer a modern fair fixed price! That’s right, fixed price! Regardless the price of your property being sold!!!
				</p>
				<p>
					Property ASAP is about leaving you with full control.  We have different packages for you to choose which will best suit your lifestyle. Let Property ASAP help alleviate those pressures found in selling your property. 
				</p>
				<p></p>
				<p class="text-center">
					<b>Property ASAP is about leaving you with full control</b>
				</p>
			</div>
		</div>
	</div>



@endsection




@section('css')
@parent

<style>
	.description {
		font-style: oblique;
	    font-size: 14px;
	    line-height: 30px;
	    width: 75%;
	    text-align: justify;
	    margin: 20px auto;
	}

	#top-image {
		position: relative;
		width:100%;
		height: 250px;
		margin: 30px 0;
		z-index:0; 
		background-size: calc(100%);
		background-repeat: no-repeat;
	}
</style>

@endsection



@section('js')
@parent


<script type="text/javascript">
	$(document).ready(function() {
		var movementStrength = 25;
		var height = movementStrength / $(window).height();
		var width = movementStrength / $(window).width();
		$("#top-image").mousemove(function(e){
		          var pageX = e.pageX - ($(window).width() / 2);
		          var pageY = e.pageY - ($(window).height() / 2);
		          var newvalueX = width * pageX * -1 - 25;
		          var newvalueY = height * pageY * -1 - 50;
		          $('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
		});
	});

</script>


@endsection





