@extends('frontend.layouts.master')

@section('title', 'Terms')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))

@section('content')


	<h3 class="text-center">Terms and Conditions</h3>
	

	<section class="terms-page">
		<div class="container">
			
			<p>
			    Last updated on propertyasap.com.au
			</p>
			<p>
			    These terms of use (Terms) govern your use of the Property ASAP website
			    located at http://propertyasap.com.au/ (Website) and any other services
			    made available through the Website. By using the Website, you agree to be
			    bound by these Terms which form a binding contractual agreement between
			    you, the user of the Website and us, Property ASAP Pty Ltd ACN 619 892 750
			    (Property ASAP, we or us).
			</p>
			<p>
			    We may change these Terms at any time by updating this webpage, and your
			    continued use of the Website following such an update will represent an
			    agreement by you to be bound by the Terms as amended.
			</p>
			<p>
			    The Website is a passive medium that facilitates the introduction of:
			</p>
			<p>
			    · users of the Website who are seeking the opportunity to buy or rent
			    property (Customers); and
			</p>
			<p>
			    · users of the Website who are, or represent, owners of property who are
			    seeking to sell or rent out property (Providers).
			</p>
			<p>
			    The remainder of these Terms are divided into three parts:
			</p>
			<p>
			    · Part 1 (General), which sets out terms that apply to both Customers and
			    Providers; and
			</p>
			<p>
			    · Part 2 (Providers), which sets out further terms that only apply to
			    Providers.
			</p>
			<h3>
			    1. GENERAL
			</h3>
			<h4>
			    1.1 ELIGIBILITY
			</h4>
			<p>
			    This Website is not intended for use by any person under the age of 18
			    years old or any person who has previously been suspended or prohibited
			    from using the Website. By using the Website, you represent that you are
			    over the age of 18 years and have not been suspended or prohibited from
			    using the Website. If you are under the age of 18 years, please do not
			    access or use the Website.
			</p>
			<h4>
			    1.2 YOUR OBLIGATIONS
			</h4>
			<p>
			    You hereby undertake to:
			</p>
			<h5>
			    (a) not copy, mirror, reproduce, translate, adapt, vary, modify, sell,
			    decipher or decompile any part or aspect of the Website without the express
			    consent of Property ASAP;
			</h5>
			<h5>
			    (b) not use the Website for any purpose other than the purpose of making
			    arrangements to rent or sell property;
			</h5>
			<h5>
			    (c) not use the Website in a manner that is illegal or fraudulent or
			    facilitates illegal or fraudulent activity;
			</h5>
			<h5>
			    (d) not use the contact details of other users for the purpose of
			    distributing unsolicited commercial content, junk mail, spam, bulk content,
			    harassment, abuse or any unlawful purpose;
			</h5>
			<h5>
			    (e) not use the Website with the assistance of any automated scripting tool
			    or software; and
			</h5>
			<h5>
			    (f) ensure that your employees, sub-contractors and any other agents who
			    use or access the Website comply with the Terms.
			</h5>
			<h4>
			    1.3 POSTED MATERIALS - WARRANTIES
			</h4>
			<p>
			    By providing or posting any information, materials or other content
			    (including information and images relating to any property) on the Website
			    (<strong>Posted Material</strong>), you represent and warrant that:
			</p>
			<h5>
			    (a) you are authorised to provide the Posted Material (including in
			    relation to any property that you represent you can rent or sell);
			</h5>
			<h5>
			    (b) the Posted Material is accurate and true at the time it is provided;
			</h5>
			<h5>
			    (c) any Posted Material which is in the form of a review or feedback is
			    honest, accurate and presents a fair view of the relevant person and/or
			    your experience;
			</h5>
			<h5>
			    (d) the Posted Material is free from any harmful, discriminatory,
			    defamatory or maliciously false implications and do not contain any
			    offensive or explicit material;
			</h5>
			<h5>
			    (e) the Posted Material is not “passing off” of any product or service and
			    does not constitute unfair competition;
			</h5>
			<h5>
			    (f) the Posted Material does not infringe any intellectual property rights,
			    including copyright, trade marks, business names, patents, confidential
			    information or any other similar proprietary rights, whether registered or
			    unregistered, anywhere in the world (<strong>IPR</strong>);
			</h5>
			<h5>
			    (g) the Posted Material does not contain any viruses or other harmful code,
			    or otherwise compromise the security or integrity of any network or system;
			    and
			</h5>
			<h5>
			    (h) the Posted Material does not breach or infringe any applicable laws.
			</h5>
			<h4>
			    1.4 POSTED MATERIALS – OBLIGATIONS
			</h4>
			<p>
			    You hereby undertake to ensure that the Posted Material:
			</p>
			<h5>
			    (a) is accurate and true at the time it is provided;
			</h5>
			<h5>
			    (b) does not exploit any person or contain any harmful, discriminatory,
			    defamatory or maliciously false implications and do not contain any
			    offensive or explicit material;
			</h5>
			<h5>
			    (c) does not contain any viruses or other harmful code, or otherwise
			    compromise the security or integrity of any network or system;
			</h5>
			<h5>
			    (d) does not contain any content that could reasonably be considered to be
			    untargeted or repetitive;
			</h5>
			<h5>
			    (e) does not contain any professional advice, including financial, legal or
			    medical advice; and
			</h5>
			<h5>
			    (f) does not promote or solicit any goods or services other than in
			    accordance with these Terms.
			</h5>
			<h4>
			    1.5 POSTED MATERIALS - LICENCE
			</h4>
			<h4>
			    You grant to Property ASAP a perpetual, irrevocable, transferable,
			    worldwide and royalty-free licence (including the right to sublicense) to
			    use, copy, modify, reproduce and adapt any IPR in any Posted Material in
			    order for Property ASAP to use, exploit or otherwise enjoy the benefit of
			    such Posted Material.
			</h4>
			<h4>
			    1.6 POSTED MATERIALS - REMOVAL
			</h4>
			<p>
			    Property ASAP acts as a passive conduit for the online distribution of
			    Posted Materials and has no obligation to screen Posted Materials in
			    advance of them being posted. However, Property ASAP may, in its absolute
			    discretion, review and remove any Posted Materials (including links to you,
			    your profile or your property that you have posted on the Website) at any
			    time without notice and without giving any explanation or justification for
			    removing the material and/or information.
			</p>
			<h4>
			    <span name="_Toc486437531">1.7 SERVICE LIMITATIONS</span>
			</h4>
			<p>
			    The Website is made available to you strictly on an 'as is' basis. Without
			    limitation, you acknowledge that Property ASAP cannot guarantee that:
			</p>
			<h5>
			    (a) the Website will be free from errors or defects;
			</h5>
			<h5>
			    (b) the Website will be accessible at all times;
			</h5>
			<h5>
			    (c) messages sent through the Website will be delivered promptly, or
			    delivered at all;
			</h5>
			<h5>
			    (d) information you receive or supply through the Website will be secure or
			    confidential; or
			</h5>
			<h5>
			    (e) any information provided through the Website is accurate or true.
			</h5>
			<h4>
			    1.8 INTELLECTUAL PROPERTY
			</h4>
			<p>
			    Property ASAP retains ownership of the Website and reserves all rights in
			    any IPR owned or licensed by it not expressly granted to you.
			</p>
			<h4>
			    1.9 THIRD PARTY CONTENT
			</h4>
			<p>
			    The Website may contain text, images, data and other content provided by a
			third party and displayed on the Website (    <strong>Third Party Content</strong>). Property ASAP takes no
			    responsibility for Third Party Content and makes no representation or
			    warranty about the quality, suitability, accuracy, reliability, currency or
			    completeness of Third Party Content.
			</p>
			<h4>
			    1.10 SECURITY
			</h4>
			<p>
			    Property ASAP does not accept responsibility for loss or damage to computer
			    systems, mobile phones or other electronic devices arising in connection
			    with use of the Website. You should take your own precautions to ensure
			    that the process which you employ for accessing the Website does not expose
			    you to risk of viruses, malicious computer code or other forms of
			    interference.
			</p>
			<h4>
			    1.11 DISLAIMER
			</h4>
			<p>
			    The Website is a medium that facilitates the introduction of Customers and
			    Providers for the purposes of making arrangements to rent or sell property.
			    Property ASAP simply collects a service fee from Providers in consideration
			    for providing this introduction service and does not have any obligations
			    or liabilities to, and is not a party to any contract between, Customers
			    and Providers in relation to subsequent agreements to rent or sell property
			    or any other agreement otherwise resulting from the introduction.
			</p>
			<p>
			    All information on the Website is general information, not advice, and
			    should not be relied upon as advice. You should make your own inquiries and
			    obtain independent advice about any decision you make in relation to the
			    general information on the Website.
			</p>
			<h5>
			    Property ASAP may at its absolute discretion change the functionality of
			    the Website at any time by updating the Website, and may discontinue any
			    feature or service.
			</h5>
			<p>
			    To the maximum extent permitted by applicable law, Property ASAP excludes
			    completely all liability to any person for loss or damage of any kind,
			    however arising whether in contract, tort (including negligence), statute,
			    equity, indemnity or otherwise, arising from or relating in any way to the
			    Website or its use. This includes the transmission of any computer virus.
			</p>
			<p>
			    You agree to indemnify Property ASAP and its employees and agents in
			    respect of all liability for loss, damage or injury which may be suffered
			    by any person arising from you or your representatives use of the Website.
			</p>
			<p>
			    All express or implied representations and warranties are, to the maximum
			    extent permitted by applicable law, excluded. Where any law (including the
			    Competition and Consumer Act 2010 (Cth)) implies a condition, warranty or
			    guarantee into these Terms which may not lawfully be excluded, then to the
			    maximum extent permitted by applicable law, Property ASAP’s liability for
			    breach of that non-excludable condition, warranty or guarantee will, at our
			    option, be limited to:
			</p>
			<h5>
			    (a) in the case of goods, their replacement or the supply or equivalent
			    goods or their repair; and
			</h5>
			<h5>
			    (b) in the case of services, the supply of the services again, or the
			    payment of the cost of having them supplied again.
			</h5>
			<p>
			    Under no circumstances will Property ASAP be liable for any incidental,
			    special or consequential loss or damages, or damages for loss of data,
			    business or business opportunity, goodwill, anticipated savings, profits or
			    revenue arising under or in connection with the Website, these Terms or
			    their subject matter.
			</p>
			<h4>
			    1.12 CONFIDENTIALITY
			</h4>
			<p>
			    You agree that:
			</p>
			<h5>
			    (a) no information owned by Property ASAP, including system operations,
			    documents, marketing strategies, staff information and client information,
			    will be disclosed or made available to any third parties; and
			</h5>
			<h5>
			    (b) all communications involving the details of other users on this website
			    are confidential, will be kept as such by you and will not be distributed
			    nor disclosed to any third party.
			</h5>
			<h4>
			    1.13 PRIVACY
			</h4>
			<p>
			    You agree to be bound by the clauses outlined in Property ASAP’s Privacy
			    Policy, which can be found propertyasap.com.au/privacy.
			</p>
			<h4>
			    1.14 TERMINATION
			</h4>
			<p>
			    Property ASAP reserves the right to terminate a user’s access to any or all
			    of the Website (including any listings and memberships) at any time without
			    notice, for any reason.
			</p>
			<p>
			    In the event that a user’s membership is terminated, at Property ASAP’s
			    option:
			</p>
			<h5>
			    (a) the user’s access to all posting tools on the Website will be revoked;
			</h5>
			<h5>
			    (b) the user will be unable to view the details of all other users
			    (including contact details, geographic details, any other personal details
			    and requests); and
			</h5>
			<h5>
			    (c) the user will also be unable to view the details of all Providers
			    (including contact details, geographic details and any other details), and
			    all Posted Material previously posted by the user will also be removed from
			    the Website.
			</h5>
			<h5>
			    Users may terminate their membership on the Website at any time by using
			    the Website’s functionality where such functionality is available. Where
			    such functionality is not available, Property ASAP will effect such
			    termination within a reasonable time after receiving written notice from
			    the user.
			</h5>
			<h5>
			    Notwithstanding termination or expiry of your membership of the Website or
			    these Terms, the provisions of Part 1, and any other provision which by its
			    nature would reasonably be expected to be complied with after termination,
			    will continue to apply.
			</h5>
			<h4>
			    1.15 TAX
			</h4>
			<p>
			    You are responsible for the collection and remission of all taxes
			    associated with the services or transactions through the use of this site
			    and Property ASAP will not be held accountable in relation to any
			    transactions held between Customers and Providers where tax related
			    misconduct has occurred.
			</p>
			<h4>
			    1.16 RECORD/AUDIT
			</h4>
			<p>
			    To the extent permitted by law, Property ASAP reserves the right to keep
			    all records of any and all transactions and communications made through
			    this Website between you and other members (including conversations, user
			    posts, job request bids, comments, feedback, cookies, and I.P. address
			    information) for administration purposes and also holds the right to
			    produce these records in the event of any legal dispute involving Property
			    ASAP.
			</p>
			<h4>
			    1.17 RELATIONSHIP
			</h4>
			<p>
			    Nothing contained in this agreement creates an agency, partnership, joint
			    venture or employment relationship between you and Property ASAP or any of
			    their respective employees, agents or contractors. You must not hold
			    yourself out as having any such relationship with Property ASAP or as being
			    entitled to contract or accept payment in the name of or on account of
			    Property ASAP.
			</p>
			<h4>
			    1.18 NO WAIVER
			</h4>
			<p>
			    You must not rely on Property ASAP’s words or conduct as a waiver of any
			    right unless the waiver is in writing and signed by Property ASAP granting
			    the waiver.
			</p>
			<h4>
			    1.19 GOVERNING LAW
			</h4>
			<p>
			    These Terms are governed by the law applying in New South Wales, Australia.
			</p>
			<h4>
			    <span name="_Toc486437532">1.20 JURISDICTION</span>
			</h4>
			<p>
			    Each party irrevocably submits to the exclusive jurisdiction of the courts
			    of New South Wales, Australia and courts of appeal from them in respect of
			    any proceedings arising out of or in connection with these Terms. Each
			    party irrevocably waives any objection to the venue of any legal process on
			    the basis that the process has been brought in an inconvenient forum.
			</p>
			<h3>
			    2. PROVIDERS – FURTHER TERMS
			</h3>
			<h4>
			    2.1 MEMBERSHIP AND PROPERTY LISTINGS
			</h4>
			<p>
			    You agree and acknowledge that:
			</p>
			<h5>
			    (a) you must pay the service fee specified on the Website or as otherwise
			    notified by us to you (<strong>Service Fee</strong>) in order to finalise
			    and upload a listing to the Website in respect of an opportunity for a
			    Customer to rent or buy a property (<strong>Property Listing</strong>);
			</h5>
			<h5>
			    (b) you must create an account on the Website using the Website’s
			    functionality in order to create a Property Listing;
			</h5>
			<h5>
			    (c) Property ASAP may at its discretion change the Service Fee at any time
			    by updating the Website;
			</h5>
			<h5>
			    (d) the Service Fee displayed on the Website on the date you make payment
			    in respect of a Property Listing is the applicable Service Fee for that
			    Property Listing;
			</h5>
			<h5>
			    (e) the account registration information you provide must be accurate,
			    complete and up-to-date;
			</h5>
			<h5>
			    (f) you must ensure that your account details for the Website are kept
			    confidential and must immediately notify Property ASAP if you become aware
			    of any unauthorised use of your account details;
			</h5>
			<h5>
			    (g) you are solely responsible for maintaining the confidentiality and
			    security of your account information and your password;
			</h5>
			<h5>
			    (h) you are solely responsible for any activities that occur through your
			    account, including those of any third party, whether those activities have
			    been authorised by you or not;
			</h5>
			<h5>
			    (i) you are able to fulfil the representations and details on each Property
			    Listing;
			</h5>
			<h5>
			    (j) you must not create multiple accounts on the Website;
			</h5>
			<h5>
			    (k) you must not impersonate any other person or create an account on the
			    Website for any other person;
			</h5>
			<h5>
			    (l) we may request evidence of your identity at any time and you must
			    provide such details as may be reasonably necessary to verify your
			    identity;
			</h5>
			<h5>
			    (m) you must provide us promptly with all other information reasonably
			    requested by us in order to enable us to ensure you are authorised to
			    provide the listing and finalise the listing;
			</h5>
			<h5>
			    (n) you must not claim or attempt to claim a Service Fee from a Customer;
			    and
			</h5>
			<h5>
			    (o) any agreement or terms and conditions between you and the Customer in
			    relation to a property described in a Property Listing do not involve
			    Property ASAP in any way, except that they must not be inconsistent with
			    your or the Customer’s obligations under these Terms.
			</h5>
			<h4>
			    2.2 REMOVING A PROPERTY LISTING AND CANCELLATIONS
			</h4>
			<p>
			    If you wish to remove a Property Listing or cancel your account you must
			    contact us using the Website’s functionality. Property ASAP may request
			    information and reasons in respect of your request to remove a Property
			    Listing or cancel your account.
			</p>
			<p>
			    Property ASAP will take reasonable steps to remove your Property Listing or
			    cancel your account within a reasonable time after a request made under
			    this clause.
			</p>
			<p>
			    To the maximum extent permitted by law, you will not be entitled to a
			    refund of any portion of the Service Fee following a request made under
			    this clause.
			</p>
			<p>
			    Property ASAP removing a Property Listing or cancelling your account does
			    not constitute an admission or concession of liability of any kind or a
			    waiver of any of its rights under these Terms.
			</p>
			<p>
			    Property ASAP will have no liability or obligation to you if a Customer
			    cancels any offer or terminates any agreement with you in respect of a
			    Property Listing and you will not be entitled to any compensation from
			    Property ASAP, including any portion of the Service Fee.
			</p>
			<h4>
			    2.3 WARRANTIES
			</h4>
			<p>
			    By listing yourself as a Provider on the Website, you represent and warrant
			    that:
			</p>
			<h5>
			    (a) you are able to fulfil the representations and details on each Property
			    Listing;
			</h5>
			<h5>
			    (b) the registration information you provide is accurate, complete and
			    up-to-date;
			</h5>
			<h5>
			    (c) you will provide the relevant services to Customers:
			</h5>
			<p>
			    (i) using suitably qualified and trained personnel exercising due care and
			    skill in a professional, efficient, diligent and safe manner, and to best
			    industry standards; and
			</p>
			<p>
			    (ii) in compliance with all applicable laws; and
			</p>
			<h5>
			    (d) any individuals or companies involved in performing the relevant
			    services have not been previously convicted of a felony, and there are no
			    current legal, criminal, civil or administrative proceedings against such
			    individuals or companies.
			</h5>
			<strong>
			    <br clear="all"/>
			</strong>
			<p>
			    <strong> </strong>
			</p>

		</div>
	</section>

@endsection




@section('css')
@parent

@endsection



@section('js')
@parent


@endsection





