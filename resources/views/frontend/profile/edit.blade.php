@extends('frontend.layouts.master')

@section('title', 'Profile')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))


@section('content')

    <div class="rs_graybg rs_toppadder100 rs_bottompadder40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="row">
                            @include('frontend.components.profile.sidebar')
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="rs_user_dashboard_tab">
                                <div class="tab-content">
                                    <div class="tab-pane active">
                                        <div class="rs_user_dashboard_tab_heading">
                                            <h4>Edit Your Information</h4>
                                        </div>
                                        <div class="rs_user_dashboard_tab_info">

                                            <div class="rs_contact_form">
                                                <form action="" method="post" name="update-me"
                                                      id="update-me">

                                                    {{ csrf_field() }}

                                                    @include('frontend.components.forms.input.input', ['field' => 'fullname', 'label' => 'Fullname : ', 'placeholder' => 'Enter your fullname', 'default' => $me->fullname])

                                                    @include('frontend.components.forms.input.input', ['field' => 'address', 'label' => 'Address : ', 'placeholder' => 'Enter your address', 'default' => $me->address])

                                                    @include('frontend.components.forms.input.input', ['field' => 'birthday', 'label' => 'Birthday : ', 'placeholder' => 'Enter your birthday', 'default' => $me->birthday])

                                                    @include('frontend.components.forms.input.input', ['field' => 'phone_number', 'label' => 'Phone number : ', 'placeholder' => 'Enter your Phone number', 'default' => $me->phone_number])

                                                    @include('frontend.components.forms.selectbox.select', ['field' => 'gender', 'label' => 'Gender : ', 'placeholder' => 'Enter your gender', 'data' => $genders, 'default' => $me->gender])

                                                    <div class="rs_btn_div rs_toppadder30">
                                                        <button type="submit" class="rs_button rs_button_orange">Edit</button>
                                                        <p id="err"></p>
                                                    </div>

                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection




@section('css')
    @parent

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/jquery.datetimepicker.css') }}"/>

@endsection



@section('js')
    @parent

    <script src="{{ asset('assets/frontend/js/jquery.datetimepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#f-birthday').datetimepicker({
                timepicker:false,
                format:'m/d/Y',
                formatDate:'m/d/Y',
                timepickerScrollbar:false
            });
        });
    </script>

@endsection