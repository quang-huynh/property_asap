@extends('frontend.layouts.master')

@section('title', 'Profile')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))


@section('content')

    <div class="rs_graybg rs_toppadder100 rs_bottompadder40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="row">
                            @include('frontend.components.profile.sidebar')
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="rs_user_dashboard_tab">
                                <div class="tab-content">
                                    <div class="tab-pane active">
                                        <div class="rs_user_dashboard_tab_heading">
                                            <h4>Change password</h4>
                                        </div>
                                        <div class="rs_user_dashboard_tab_info">
                                            <div class="rs_contact_form">

                                                {{--cjsj--}}
                                                @if(!empty($message))
                                                    <div class="alert alert-warning alert-dismissable fade in">
                                                        <a href="#" class="close" data-dismiss="alert"
                                                           aria-label="close">&times;</a>
                                                        <strong>Warning !</strong>
                                                        {{ @$message['content'] }}
                                                    </div>
                                                @endif
                                                {{--    ashc    --}}

                                                <form action="" method="post" name="change-password"
                                                      id="change-password">
                                                    {{ csrf_field() }}

                                                    @include('frontend.components.forms.input.password', ['field' => 'current_password', 'label' => 'Old Password : ', 'placeholder' => 'Enter your old password', 'default' =>''])

                                                    @include('frontend.components.forms.input.password', ['field' => 'password', 'label' => 'New Password : ', 'placeholder' => 'Enter your new password', 'default' =>''])

                                                    @include('frontend.components.forms.input.password', ['field' => 'password_confirmation', 'label' => 'Re-Password : ', 'placeholder' => 'Enter your Repeat password', 'default' =>''])

                                                    <div class="rs_btn_div rs_toppadder30">
                                                        <button type="submit" class="rs_button rs_button_orange">
                                                            Submit
                                                        </button>
                                                        <p id="err"></p>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('css')
    @parent
@endsection



@section('js')
    @parent
@endsection




