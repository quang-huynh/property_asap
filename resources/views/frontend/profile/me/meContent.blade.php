<?php
/**
 * @author: 
 * @datetime: 6/25/2017 7:10 am
 */
?>

<div class="rs_author_dashboard_tab_info_img">


    <form id="change-picture-me" action="{{ route('frontend.profile.me.upload-picture') }}"
          method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        <label class="myFile">
            <img src="{{ $me->picture_full_path }}" alt="" class="img-responsive">
            <p class="change-picture"><label>(Change picture)</label></p>
            <input type="file" class="" name="picture" accept="image/*"
                   style="display: none !important; cursor:pointer">
        </label>

        <div class="rs_btn_div rs_toppadder30" style="padding-top: 0 !important;">
            <button type="submit" class="rs_button rs_button_orange">Change</button>
        </div>
    </form>

    <hr/>
    <h4>{{ $me->fullname }}</h4>
    <p><b>{{ $me->email }}</b></p>
</div>
<table class="table tblMe">
    <tr>
        <td class="text-right" style="width: 50%">Email</td>
        <td class="text-left" style="width: 50%">{{ $me->email }}</td>
    </tr>
    <tr>
        <td class="text-right">Gender</td>
        <td class="text-left">{{ $me->gender_dis }}</td>
    </tr>
    <tr>
        <td class="text-right">Birthday</td>
        <td class="text-left">
            <?php 
                $birthdayOri = strtotime(  $me->birthday );
                $birhtday = date( 'm/d/Y', $birthdayOri );
            ?>
            {{ $birhtday }}
        </td>
    </tr>
    <tr>
        <td class="text-right">Phone number</td>
        <td class="text-left">{{ $me->phone_number }}</td>
    </tr>
    <tr>
        <td class="text-right">Address</td>
        <td class="text-left">{{ $me->address }}</td>
    </tr>
</table>
<style type="text/css">
    .tblMe tr {
        border-top: 0 !important;
        background: none !important;
    }

    .tblMe tr td {
        padding: 10px 20px !important;
        background: none !important;
        font-size: 14px !important;
        font-weight: normal !important;
    }

    .myFile {
        position: relative;
        overflow: hidden;
    }

    .myFile input[type="file"] {
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        opacity: 0;
        font-size: 100px;
        filter: alpha(opacity=0);
        cursor: pointer;
    }

    .myFile input[type="file"]:hover {
        background: #ccc;
    }

    .myFile img {
        cursor: pointer;
    }

    .myFile img:hover {
        opacity: 0.8 !important;
    }

    p.change-picture label {
        font-weight: normal !important;
        font-size: 13px !important;
        font-style: italic !important;
    }
</style>