@extends('frontend.layouts.master')

@section('title', 'Profile')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))


@section('content')

    <div class="rs_graybg rs_toppadder100 rs_bottompadder40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="row">
                            @include('frontend.components.profile.sidebar')
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="rs_user_dashboard_tab">
                                <div class="tab-content">
                                    <div class="tab-pane active">
                                        <div class="rs_user_dashboard_tab_heading">
                                            <h4>Your Information</h4>
                                        </div>
                                        <div class="rs_user_dashboard_tab_info">
                                            @include('frontend.profile.me.meContent')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection




@section('css')
    @parent

@endsection



@section('js')
    @parent

@endsection




