@extends('frontend.layouts.master')

@section('title', 'Register Success')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))

@section('content')

    <div class="rs_graybg rs_toppadder100 rs_bottompadder100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Register account successfully !</h1>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    @parent
@endsection

@section('js')
    @parent
@endsection





