@extends('frontend.layouts.master')

@section('title', 'Register')

@section('fb_title', 'Property Asap')
@section('fb_description', 'You’re in Control')
@section('fb_url', Request::url())
@section('fb_image', asset('assets/common/images/' . config('asap.default_post_picture_name_path')))



@section('content')


    <div class="rs_graybg rs_toppadder100 rs_bottompadder100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="rs_contact_form">
                        <h2>Register Form</h2>

                        <div class="rs_submitform">
                            <form role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email: </label>
                                    <input type="text" class="form-control" name="email" id="register_mail"
                                           placeholder="Enter your email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password: </label>
                                    <input type="password" class="form-control" name="password" id="register_password"
                                           placeholder="Enter your password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password Confirmation: </label>
                                    <input type="password" name="password_confirmation" class="form-control"
                                           id="register_password"
                                           placeholder="Re enter your password">
                                </div>

                                <div class="form-group">
                                    <label>Fullname: </label>
                                    <input type="text" name="fullname" class="form-control" id="register_fullname"
                                           placeholder="Enter your full name">
                                    @if ($errors->has('fullname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <label>Phone number: </label>
                                            <input type="text" name="phone_number" class="form-control"
                                                   id="register_phonenumber"
                                                   placeholder="Enter your phone number">
                                            @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <label>Address: </label>
                                            <input type="text" name="address" class="form-control" id="register_address"
                                                   placeholder="Enter your address">
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="accept_term" value="1">By click, you agree to our Terms of Service and Privacy
                                        Policy.
                                    </label>
                                </div>

                                <div class="rs_btn_div rs_toppadder30">
                                    <button type="submit"  class="rs_button rs_button_orange">Register</button>
                                    <p id="err"></p>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




@section('css')
    @parent

@endsection



@section('js')
    @parent

@endsection





