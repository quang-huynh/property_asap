'use strict';


var app = {
    init: function() {
        this.common();
        this.homepage();
        this.create_post();
        this.update_post();
        this.search_post();
    },
    
    common: function() { 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        

        $('[rel="keypress-only-number"]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });


        if ($("#sharePopup").length != 0) {
            $("#sharePopup").jsSocials({
                shareIn: "popup",
                shares: ["facebook", "twitter","googleplus", "linkedin", "pinterest"]
            });
        }

        if ($("#sharePopup-widget").length != 0) {
            $("#sharePopup-widget").jsSocials({
                shareIn: "popup",
                showLabel: false,
                showCount: false,
                shares: ["facebook", "twitter","googleplus", "linkedin", "pinterest"]
            });
        }

        if ($('[rel="limit-text"]').length != 0) {
            $.each($('[rel="limit-text"]'), function(index, val) {
                var txt= $(val).text();
                if(txt.length > 50) {
                    $(val).text(txt.substring(0,50) + '.....');
                }
            });
                
        }

        if ($('[rel="add-wishlist"]').length != 0) {
            $('[rel="add-wishlist"]').on('click', function() {
                var that = $(this);
                var url = $('meta[name="add_wishlist_url"]').attr('content');
                var id = that.data('id');

                if (that.parents('.rs_product_div').find('.wishlist-item').hasClass('active')) {
                    return;
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id}
                })
                .complete(function() {

                })
                .done(function(result) {
                    if (result.code == 1) {
                        that.parents('.rs_product_div').find('.wishlist-item').fadeIn().addClass('active');
                        that.removeClass('show').fadeOut();
                    }
                })
                .fail(function() {
                    console.log("error");
                })
            });
        }

        if ($('[rel="btn-remove-wishlist"]').length != 0) {
            $('[rel="btn-remove-wishlist"]').on('click', function(){
                var that = $(this);
                var url = $('meta[name="remove_wishlist_url"]').attr('content');
                var id = that.data('id');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id}
                })
                .complete(function() {

                })
                .done(function(result) {
                    if (result.code == 1) {
                        that.parents('.rs_product_div').find('[rel="add-wishlist"]').fadeIn().removeClass('hide').addClass('show');
                        that.parents('.wishlist-item').removeClass('active').fadeOut();
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                
            });
        }


        if ($('#action-wishlist').length != 0) {
            $('#action-wishlist').on('click', function() {
                var that = $(this);
                if (that.data('status') == 0) {
                    var url = $('meta[name="add_wishlist_url"]').attr('content');
                    var id = that.data('id');

                    if (that.parents('.rs_product_div').find('.wishlist-item').hasClass('active')) {
                        return;
                    }

                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id}
                    })
                    .complete(function() {

                    })
                    .done(function(result) {
                        if (result.code == 1) {
                            that.empty().append('Remove from Wishlist');
                            that.data('status', '1');
                            that.attr('data-status', '1');
                        }
                    })
                    .fail(function() {
                        console.log("error");
                    })
                } else {
                    var url = $('meta[name="remove_wishlist_url"]').attr('content');
                    var id = that.data('id');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id}
                    })
                    .complete(function() {

                    })
                    .done(function(result) {
                        if (result.code == 1) {
                            that.empty().append('Add to Wishlist');
                            that.data('status', '0');
                            that.attr('data-status', '0');
                        }
                    })
                    .fail(function() {
                        console.log("error");
                    })
                }
            });
        }
            

    },



    homepage: function() {

    	if ($('#agree-modal-btn').length != 0) {
    		$('#agree-modal-btn').on('click', function() {
    			$('#agree-terms-privacy-modal').modal('hide');
    			setTimeout(function(){ 
    				window.location.replace("/register");
    			}, 1000);
				
    		});
    			
    	}

    },



    create_post: function() {

        function init() {
            $('#tabs-step-2').fadeOut();
            $('#tabs-step-3').fadeOut();
            $('#tabs-step-4').fadeOut();
            $('#tabs-step-5').fadeOut();
            $('#autocomplete').attr('style', 'border: 2px dashed #028482;');
            setTimeout(function(){
                $('#autocomplete').attr('style', 'border: none;');
            }, 5000);
            $("#autocomplete").focusout(function() {
                if (!$('#tutorial-for-autocomplete-create-post').hasClass('hide')) {
                    $('#tutorial-for-autocomplete-create-post').fadeOut().addClass('hide');
                }
            });
                            
        }


        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }



        if ($('#form-create-post').length != 0) {
            init();

            $('#btn-continue-step-1').on('click', function() {
                if ($('input[name="location_city"]').val() != "" && $('input[name="location_state"]').val() != "") {
                    $('#tabs-step-2').fadeIn();
                    $('#tabs-step-1').removeClass('active');
                    $('#tabs-step-2').addClass('active');
                    $('#step-1').removeClass('active');
                    $('#step-2').addClass('active');

                    $("html, body").animate({ scrollTop: 0 }, 600);
                } else {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-1').append('Oops! Something were wrong. Please check again!').fadeIn();
                    setTimeout(function(){
                        $('#mess-step-1').fadeOut().empty();
                    }, 5000);

                    return;
                }
            });



            $('#btn-continue-step-2').on('click', function() {
                var mess;
                if ($('input[name="title"]').val() != "" && $('input[name="title"]').val().length >= 30 &&  $.trim($('textarea[name="description"]').val()) != "" && $('textarea[name="description"]').val().length >= 60 && $('input[name="price"]').val() != "") {
                    $('input[name="title"]').removeClass('q-empty-input');
                    $('textarea[name="description"]').removeClass('q-empty-input');

                    $('#tabs-step-3').fadeIn();
                    $('#tabs-step-2').removeClass('active');
                    $('#tabs-step-3').addClass('active');
                    $('#step-2').removeClass('active');
                    $('#step-3').addClass('active');

                    $("html, body").animate({ scrollTop: 0 }, 600);
                } else {
                    mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="title"]').val() == "" || $('input[name="title"]').val().length < 30) {
                        $('input[name="title"]').addClass('q-empty-input');
                        mess += 'The title must not empty and more than 8 word!<br>';
                    } else {
                        $('input[name="title"]').removeClass('q-empty-input');
                    }
                    if ( $.trim($('textarea[name="description"]').val()) == "" || $('textarea[name="description"]').val().length < 60) {
                        $('textarea[name="description"]').addClass('q-empty-input');
                        mess += 'The description must not empty and more than 20 word!';
                    } else {
                        $('textarea[name="description"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="price"]').val() == "") {
                        $('input[name="price"]').addClass('q-empty-input');
                        mess += 'The price must not empty<br>';
                    } else {
                        $('input[name="price"]').removeClass('q-empty-input');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-2').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-2').fadeOut().empty();
                    }, 5000);

                    return;
                }
            });




            $('#btn-continue-step-3').on('click', function() {
                var mess;
                if ($('input[name="name_contact"]').val() != "" && $('input[name="email_contact"]').val() != "" && isEmail($('input[name="email_contact"]').val()) &&  $('input[name="address_contact"]').val() != "" && $('input[name="address_contact"]').val().length >= 15 && $('input[name="phone_number"]').val() != "" && $('input[name="home_number"]').val() != "") {
                    $('input[name="name_contact"]').removeClass('q-empty-input');
                    $('input[name="address"]').removeClass('q-empty-input');
                    $('input[name="phone_number"]').removeClass('q-empty-input');
                    $('input[name="home_phone"]').removeClass('q-empty-input');

                    $('#tabs-step-4').fadeIn();
                    $('#tabs-step-3').removeClass('active');
                    $('#tabs-step-4').addClass('active');
                    $('#step-3').removeClass('active');
                    $('#step-4').addClass('active');

                    $("html, body").animate({ scrollTop: 0 }, 600);
                } else {
                    mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="name_contact"]').val() == "") {
                        $('input[name="name_contact"]').addClass('q-empty-input');
                        mess += 'The name must not empty<br>';
                    } else {
                        $('input[name="name_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="email_contact"]').val() == "" || !isEmail($('input[name="email_contact"]').val()) ) {
                        $('input[name="email_contact"]').addClass('q-empty-input');
                        mess += 'Please correct email contact<br>';
                    } else {
                        $('input[name="email_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="address_contact"]').val() == "" || $('input[name="address_contact"]').val().length < 15) {
                        $('input[name="address_contact"]').addClass('q-empty-input');
                        mess += 'The address contact must not empty and more than 4 word!<br>';
                    } else {
                        $('input[name="address_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="phone_number"]').val() == "") {
                        $('input[name="phone_number"]').addClass('q-empty-input');
                        mess += 'The phone number must not empty<br>';
                    } else {
                        $('input[name="phone_number"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="home_phone"]').val() == "") {
                        $('input[name="home_phone"]').addClass('q-empty-input');
                        mess += 'The home number must not empty<br>';
                    } else {
                        $('input[name="home_number"]').removeClass('q-empty-input');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-3').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-3').fadeOut().empty();
                    }, 5000);

                    return;
                }
            });


            $('#btn-continue-step-4').on('click', function() { 


                $("html, body").animate({ scrollTop: 0 }, 600);
                $('#tabs-step-5').fadeIn();
                $('#tabs-step-4').removeClass('active');
                $('#tabs-step-5').addClass('active');
                $('#step-4').removeClass('active');
                $('#step-5').addClass('active');
            });


            function validateFormCreatePost()
            {
                if ($('#autocomplete').val() == "" || $('input[name="location_city"]').val() == "" || $('input[name="location_state"]').val() == "") {
                    $('#mess-step-1').append('Oops! Something were wrong. Please check again!').fadeIn();
                    setTimeout(function(){
                        $('#mess-step-1').fadeOut().empty();
                    }, 5000);

                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#tabs-step-3').addClass('active');
                    $('#step-3').addClass('active');

                    $('#tabs-step-1').removeClass('active');
                    $('#step-1').removeClass('active');
                    $('#tabs-step-2').removeClass('active');
                    $('#step-2').removeClass('active');
                    $('#tabs-step-4').removeClass('active');
                    $('#step-4').removeClass('active');
                    $('#tabs-step-5').removeClass('active');
                    $('#step-5').removeClass('active');
                    return false;
                }
                if ($('input[name="title"]').val() == "" || $('input[name="title"]').val().length < 30 || $.trim($('textarea[name="description"]').val()) == "" || $('textarea[name="description"]').val().length < 60 || $('input[name="price"]').val() == "") {
                    var mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="title"]').val() == "" || $('input[name="title"]').val().length < 30) {
                        $('input[name="title"]').addClass('q-empty-input');
                        mess += 'The title must not empty and more than 8 word!<br>';
                    } else {
                        $('input[name="title"]').removeClass('q-empty-input');
                    }
                    if ( $.trim($('textarea[name="description"]').val()) == "" || $('textarea[name="description"]').val().length < 60) {
                        $('textarea[name="description"]').addClass('q-empty-input');
                        mess += 'The description must not empty and more than 20 word!';
                    } else {
                        $('textarea[name="description"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="price"]').val() == "") {
                        $('input[name="price"]').addClass('q-empty-input');
                        mess += 'The price must not empty and more than 8 word!<br>';
                    } else {
                        $('input[name="price"]').removeClass('q-empty-input');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-2').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-2').fadeOut().empty();
                    }, 5000);


                    $('#tabs-step-3').addClass('active');
                    $('#step-3').addClass('active');

                    $('#tabs-step-1').removeClass('active');
                    $('#step-1').removeClass('active');
                    $('#tabs-step-2').removeClass('active');
                    $('#step-2').removeClass('active');
                    $('#tabs-step-4').removeClass('active');
                    $('#step-4').removeClass('active');
                    $('#tabs-step-5').removeClass('active');
                    $('#step-5').removeClass('active');
                    return false;
                }
                if ($('input[name="name_contact"]').val() == "" || $('input[name="email_contact"]').val() == "" || !isEmail($('input[name="email_contact"]').val()) || $('input[name="address_contact"]').val() == "" || $('input[name="address_contact"]').val().length < 15  || $('input[name="phone_number"]').val() == "" || $('input[name="home_number"]').val() == "") {
                    var mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="name_contact"]').val() == "") {
                        $('input[name="name_contact"]').addClass('q-empty-input');
                        mess += 'The name must not empty<br>';
                    } else {
                        $('input[name="name_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="email_contact"]').val() == "" || !isEmail($('input[name="email_contact"]').val()) ) {
                        $('input[name="email_contact"]').addClass('q-empty-input');
                        mess += 'Please correct email contact<br>';
                    } else {
                        $('input[name="email_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="address_contact"]').val() == "" || $('input[name="address_contact"]').val().length < 15) {
                        $('input[name="address_contact"]').addClass('q-empty-input');
                        mess += 'The address contact must not empty and more than 4 word!<br>';
                    } else {
                        $('input[name="address_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="phone_number"]').val() == "") {
                        $('input[name="phone_number"]').addClass('q-empty-input');
                        mess += 'The phone number must not empty<br>';
                    } else {
                        $('input[name="phone_number"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="home_phone"]').val() == "") {
                        $('input[name="home_phone"]').addClass('q-empty-input');
                        mess += 'The home phone must not empty<br>';
                    } else {
                        $('input[name="home_number"]').removeClass('q-empty-input');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-3').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-3').fadeOut().empty();
                    }, 5000);

                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#tabs-step-3').addClass('active');
                    $('#step-3').addClass('active');

                    $('#tabs-step-1').removeClass('active');
                    $('#step-1').removeClass('active');
                    $('#tabs-step-2').removeClass('active');
                    $('#step-2').removeClass('active');
                    $('#tabs-step-4').removeClass('active');
                    $('#step-4').removeClass('active');
                    $('#tabs-step-5').removeClass('active');
                    $('#step-5').removeClass('active');
                    return false;
                    
                }
                return true;
            }

            function getDataCreatePostForm()
            {
                var data = {};

                data['full_location'] = $('#autocomplete').val();
                data['location_number_street'] = $('#street_number').val();
                data['location_street'] = $('#route').val();
                data['location_city'] = $('#locality').val();
                data['location_state'] = $('#administrative_area_level_1').val();
                data['location_country'] = $('#country').val();
                data['type'] = $('#type').val();
                data['property_type'] = $('#property-type').val();
                data['bedrooms'] = $('#bedrooms').val();
                data['bathrooms'] = $('#bathrooms').val();
                data['car_spaces'] = $('#car-spaces').val();
                data['size'] = $('#size').val();
                data['title'] = $('#title').val();
                data['description'] = $('#description').val();
                data['price'] = $('#price').val();
                data['tags'] = $('#tags').val();
                data['name_contact'] = $('#name-contact').val();
                data['email_contact'] = $('#email-contact').val();
                data['address_contact'] = $('#address-contact').val();
                data['phone_number'] = $('#phone-number').val();
                data['home_number'] = $('#home-number').val();
                data['package'] = $('#package:checked').val();



                return data;
            }


            function callAjaxSubmitCreatePostForm(data)
            {
                console.log($('#create-post-btn').data('url'));
                $('#create-post-btn').addClass('disabled').empty().prepend('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp Creating');
                var url = $('#create-post-btn').data('url');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                })
                .complete(function(result) {
                    setTimeout(function(){
                        $('#create-post-btn').removeClass('disabled').empty().append('Finish');
                    }, 3000);
                })
                .done(function(result) {
                    if (result.code == 1) {
                        $("html, body").animate({ scrollTop: 0 }, 600);

                        $('#tabs-step-1').hide();
                        $('#tabs-step-2').hide();
                        $('#tabs-step-3').hide();
                        $('#tabs-step-4').hide();
                        $('#tabs-step-5').hide();
                        $('#step-5').removeClass('active');

                        $('#form-create-post').hide();
                        $('#preview-property-btn').attr('href', result.url_preview_post);
                        $('#success-box').fadeIn();
                    } else {
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $('#mess-step-5').empty().append('An unexpected error has occurred. Please check and try again!').fadeIn();
                        setTimeout(function(){
                            $('#mess-step-5').fadeOut().empty();
                        }, 10000);
                    }
                })
                .fail(function(result) {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('#mess-step-5').empty().append('An unexpected error has occurred. Create property fail!').fadeIn();
                    setTimeout(function(){
                        $('#mess-step-5').fadeOut().empty();
                    }, 10000);
                })
                
            }

            $('#create-post-btn').on('click', function(){
                var checked = $("#agree-checkbox:checked").length;

                if (checked == 0) {
                    $('#mess-step-5').append('Do you agree the <b>Terms and Conditions</b> ?').fadeIn();
                    setTimeout(function(){
                        $('#mess-step-5').fadeOut().empty();
                    }, 3000);
                    return;
                } else {
                    if (validateFormCreatePost()) {
                        var data = getDataCreatePostForm();
                        callAjaxSubmitCreatePostForm(data);
                    }
                }            
            });



        }


    },



    update_post: function() {

        function init() {
            $('#autocomplete-update').attr('style', 'border: 2px dashed #028482;');
            setTimeout(function(){
                $('#autocomplete-update').attr('style', 'border: none;');
            }, 5000);
            $("#autocomplete-update").focusout(function() {
                if (!$('#tutorial-for-autocomplete-update-post').hasClass('hide')) {
                    $('#tutorial-for-autocomplete-update-post').fadeOut().addClass('hide');
                }
            });
                            
        }


        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }



        if ($('#form-update-post').length != 0) {
            init();



            function validateFormUpdatePost()
            {
                if ($('#autocomplete-update').val() != "") {
                    if ($('input[name="location_city"]').val() == "" || $('input[name="location_state"]').val() == "") {
                        $('#mess-step-1').append('Oops! Something were wrong. Please check again!').fadeIn();
                        $('#autocomplete-update').addClass('q-empty-input');
                        setTimeout(function(){
                            $('#mess-step-1').fadeOut().empty();
                        }, 5000);

                        $("html, body").animate({ scrollTop: 0 }, 1000);
                        return false;
                    }
                }

                if ($('input[name="title"]').val() == "" || $('input[name="title"]').val().length < 30 || $.trim($('textarea[name="description"]').val()) == "" || $('textarea[name="description"]').val().length < 60 || $('input[name="price"]').val() == "") {
                    var mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="title"]').val() == "" || $('input[name="title"]').val().length < 30) {
                        $('input[name="title"]').addClass('q-empty-input');
                        mess += 'The title must not empty and more than 8 word!<br>';
                    } else {
                        $('input[name="title"]').removeClass('q-empty-input');
                    }
                    if ( $.trim($('textarea[name="description"]').val()) == "" || $('textarea[name="description"]').val().length < 60) {
                        $('textarea[name="description"]').addClass('q-empty-input');
                        mess += 'The description must not empty and more than 20 word!';
                    } else {
                        $('textarea[name="description"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="price"]').val() == "") {
                        $('input[name="price"]').addClass('q-empty-input');
                        mess += 'The price must not empty and more than 8 word!<br>';
                    } else {
                        $('input[name="price"]').removeClass('q-empty-input');
                    }
                    // $('html, body').animate({
                    //     scrollTop: $('#mess-step-2').offset().top
                    // }, 1000);
                    $('#mess-step-2').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-2').fadeOut().empty();
                    }, 5000);

                    return false;
                }
                if ($('input[name="name_contact"]').val() == "" || $('input[name="email_contact"]').val() == "" || !isEmail($('input[name="email_contact"]').val()) || $('input[name="address_contact"]').val() == "" || $('input[name="address_contact"]').val().length < 15  || $('input[name="phone_number"]').val() == "" || $('input[name="home_number"]').val() == "") {
                    var mess = 'Oops! Something were wrong<br>';
                    if ( $('input[name="name_contact"]').val() == "") {
                        $('input[name="name_contact"]').addClass('q-empty-input');
                        mess += 'The name must not empty<br>';
                    } else {
                        $('input[name="name_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="email_contact"]').val() == "" || !isEmail($('input[name="email_contact"]').val()) ) {
                        $('input[name="email_contact"]').addClass('q-empty-input');
                        mess += 'Please correct email contact<br>';
                    } else {
                        $('input[name="email_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="address_contact"]').val() == "" || $('input[name="address_contact"]').val().length < 15) {
                        $('input[name="address_contact"]').addClass('q-empty-input');
                        mess += 'The address contact must not empty and more than 4 word!<br>';
                    } else {
                        $('input[name="address_contact"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="phone_number"]').val() == "") {
                        $('input[name="phone_number"]').addClass('q-empty-input');
                        mess += 'The phone number must not empty<br>';
                    } else {
                        $('input[name="phone_number"]').removeClass('q-empty-input');
                    }
                    if ( $('input[name="home_phone"]').val() == "") {
                        $('input[name="home_phone"]').addClass('q-empty-input');
                        mess += 'The home phone must not empty<br>';
                    } else {
                        $('input[name="home_number"]').removeClass('q-empty-input');
                    }
                    // $('html, body').animate({
                    //     scrollTop: $("#step-3").offset().top
                    // }, 1000);
                    $('#mess-step-3').append(mess).fadeIn();
                    setTimeout(function(){
                        $('#mess-step-3').fadeOut().empty();
                    }, 5000);

                    return false;
                    
                }
                return true;
            }

            function getDataUpdatePostForm()
            {
                var data = {};

                data['id'] = $('#id').val();
                data['full_location'] = $('#autocomplete-update').val();
                data['location_number_street'] = $('#street_number').val();
                data['location_street'] = $('#route').val();
                data['location_city'] = $('#locality').val();
                data['location_state'] = $('#administrative_area_level_1').val();
                data['location_country'] = $('#country').val();
                data['type'] = $('#type').val();
                data['property_type'] = $('#property-type').val();
                data['bedrooms'] = $('#bedrooms').val();
                data['bathrooms'] = $('#bathrooms').val();
                data['car_spaces'] = $('#car-spaces').val();
                data['size'] = $('#size').val();
                data['title'] = $('#title').val();
                data['description'] = $('#description').val();
                data['price'] = $('#price').val();
                data['tags'] = $('#tags').val();
                data['name_contact'] = $('#name-contact').val();
                data['email_contact'] = $('#email-contact').val();
                data['address_contact'] = $('#address-contact').val();
                data['phone_number'] = $('#phone-number').val();
                data['home_number'] = $('#home-number').val();

                data['package'] = $('#package:checked').val();



                return data;
            }


            function callAjaxSubmitUpdatePostForm(data)
            {
                $('#update-post-btn').addClass('disabled').empty().prepend('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp Creating');
                var url = $('#update-post-btn').data('url');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                })
                .complete(function(result) {
                    setTimeout(function(){
                        $('#update-post-btn').removeClass('disabled').empty().append('Save');
                    }, 3000);
                })
                .done(function(result) {
                    if (result.code == 1) {
                        $('#mess-update').empty().append('Update success! Click <a href="'+result.url_preview_post+'" target="_blank"><b>here</b></a> to return your property!').fadeIn();
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $('form-update-post').hide();
                    } else {
                        $('#mess-step-5').empty().append('An unexpected error has occurred. Please check and try again!').fadeIn();
                        setTimeout(function(){
                            $('#mess-step-5').fadeOut().empty();
                        }, 10000);
                    }
                })
                .fail(function(result) {
                    $('#mess-step-5').empty().append('An unexpected error has occurred. Create property fail!').fadeIn();
                    setTimeout(function(){
                        $('#mess-step-5').fadeOut().empty();
                    }, 10000);
                })
                
            }

            $('#update-post-btn').on('click', function(){
                if (validateFormUpdatePost()) {
                    var data = getDataUpdatePostForm();
                    callAjaxSubmitUpdatePostForm(data);
                }
 
            });



        }


    },



    search_post: function() {

        if ($('#search-post-module').length != 0) {

            $('#submit-btn').on('click', function(e) {
                if ($.trim($('#autocomplete').val()) == '') {
                    e.preventDefault();
                    // window.location.replace($('#location-form').data('url'));
                    window.location.href = window.location.href.split('?')[0];
                }
            });

            $('#location-form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) { 
                    e.preventDefault();
                    return false;
                }
            });

            $('#location-form input').on('keypress', function(e) {
                return e.which !== 13;
            });

            $('[rel="sort-by"]').on('click', function(){
                $('.rs_sort').removeClass('rs_active');
                $('.rs_product_sorting').hide();
                $('#sort-by-text').empty().append($(this).data('label'));
                $('#sort-by').val($(this).data('value'));
            });

            $('[rel="tag-filter"]').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                } else {
                    $(this).addClass('active');
                }
                var value = '';
                $('#tags-filter').val('');
                $.each( $('[rel="tag-filter"]') , function(index, val) {
                    if ($(val).hasClass('active')) {
                        value += $(val).data('value') + ',';
                    }
                });
                $('#tags-filter').val(value.slice(0, -1));
            });



            function getAllFilter() {
                var filters = {};

                filters['full_location'] = $('#autocomplete').val();
                filters['location_number_street'] = $('#street_number').val();
                filters['location_street'] = $('#route').val();
                filters['location_city'] = $('#locality').val();
                filters['location_state'] = $('#administrative_area_level_1').val();
                filters['location_country'] = $('#country').val();
                filters['sort_by'] = $('#sort-by').val();
                
                var property_type = '';
                $.each($('[rel="property-type-filter"]'), function(index, val) {
                    if ($(val).prop('checked')) {
                        property_type += $(val).val() + ',';
                    }
                });
                filters['property_type'] = property_type.slice(0, -1);
                filters['bedrooms'] = $('#bedrooms').val();
                filters['bathrooms'] = $('#bathrooms').val();
                filters['car_spaces'] = $('#car-spaces').val();
                filters['size'] = $('#size').val();
                filters['size_compare'] = $('#size-compare').val();
                filters['price'] = $('#ex2').val();
                filters['tags'] = $('#tags-filter').val();

                return filters;
            }


            $('#filter-btn').on('click', function(){
                Waypoint.enableAll();
                var url = $(this).data('url');

                var filters = getAllFilter();

                var that = $(this);

                that.addClass('disabled').empty().prepend('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp Filtering');
                $('#search-main-contain .widget').addClass('search-disabled');


                $('#list-post').empty().hide();

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: filters,
                })
                .complete(function(result) {
                    setTimeout(function(){
                        that.removeClass('disabled').empty().append('Filter');
                        $('#search-main-contain .widget').removeClass('search-disabled');
                    }, 1000);
                })
                .done(function(result) {
                    if (result.code == 1) {
                        $('#message-list-post .title').fadeOut();
                        if (result.count) {
                            $('#total-found').empty().append(result.count);
                        } else {
                            $('#total-found').empty().append('0');
                        }
                        if (result.html) {
                            $('#list-post').append(result.html).fadeIn();
                            that.attr('data-offset', that.data('offset-ori'));
                            that.data('offset', that.data('offset-ori'));
                        } else {
                            $('#message-list-post .title').fadeIn();
                        }
                    }
                })
                .fail(function(result) {
                    console.log("filter search fail");
                });
                
            });


            if ($('#list-post').length != 0) {
                var loadmoreListPost = $('#list-post-wrapper').waypoint({
                    handler: function(direction) {
                        if (this === this.group.last()) {
                            if (direction == 'down') {
                                callAjaxLoadMoreList();
                                Waypoint.refreshAll();
                            }
                        }
                        else {
                            //Fail
                        }
                    },
                    offset: 75
                });
            }



            function callAjaxLoadMoreList()
            {
                var that = $('#filter-btn');

                var url = $('#filter-btn').data('url');
                var offset = $('#filter-btn').data('offset');

                var filters = getAllFilter();
                filters['offset'] = offset;

                that.addClass('disabled').empty().prepend('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp Loading');
                $('#search-main-contain .widget').addClass('search-disabled');

                $('.loading-status').fadeIn();

                Waypoint.disableAll();

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: filters,
                })
                .complete(function(result) {
                    setTimeout(function(){
                        that.removeClass('disabled').empty().append('Filter');
                        $('#search-main-contain .widget').removeClass('search-disabled');
                        $('.loading-status').fadeOut();
                    }, 1000);
                })
                .done(function(result) {
                    if (result.code == 'end') {
                        $('.loading-status').empty().append('<h3 class="text-center">.</h3>').fadeIn();
                        that.attr('data-offset', 'end');
                        that.data('offset', 'end');
                        Waypoint.disableAll();
                        return;
                    }
                    if (result.code == 1) {

                        if (result.count) {
                            var totalOld = $('#total-found').text();
                            $('#total-found').empty().append( parseInt(totalOld) + parseInt(result.count) );
                            var currentCount = that.data('offset');
                            that.attr('data-offset', currentCount + result.count);
                            that.data('offset', currentCount + result.count);
                        }
                        if (result.html) {
                            $('#list-post').append(result.html);
                        }

                        Waypoint.refreshAll();
                        Waypoint.enableAll();
                    }
                })
                .fail(function(result) {
                    console.log("load more fail");
                });


            }

        }

    },







}

$().ready(function() {
    app.init();
});