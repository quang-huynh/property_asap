'use strict';

var app_server = {
    init: function() {
        this.initAjax();
        this.common();
        this.more();
        this.addNewMessageToMenu();
    },

    initAjax: function() {
        $.ajaxSetup ({
            headers: {
                headers: { 'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content') }
            }
        });
    },
    
    common: function() {
        var role_id = $('meta[name="role_id"]').attr('content');

        var arrRoleId = role_id.split(',');

        var conn = new ab.Session('ws://dev.inventorymanagement.com:8080',
            function() {
                for (var i = 0; i < arrRoleId.length; i++) {
                    conn.subscribe(arrRoleId[i], function(args, topic) {
                        //console.log(topic);
                        $('#box-success .box-title').html(topic['title']);
                        $('#box-success .box-body').html(topic['short_description']);
                        $('#box-success').css('right', '0px');
                        setTimeout(function(){ $('#box-success').css('right', '-500px'); }, 3000);

                        app_server.addNewMessageToMenu(topic);

                    });
                };
                //conn.publish('com.myapp.hello', ['Hello, world!']);
                
            },
            function() {
                console.warn('WebSocket connection closed');
            },
            {'skipSubprotocolCheck': true}
        );

    },


//NEW DEMO
    more: function() {

        //#notification-button
        if (! $('#notification-button').length == 0) {
            $('#notification-button').on('click', function(e){
                // $('body').on('click', function(e) {
                //     e.stopPropagation();
                    
                //     if (! $(e.target).is('#notification-sidebar') && ! $(e.target).is('#notification-button')) {
                //         $('#notification-sidebar').removeClass('control-sidebar-open');
                //     }
                //     else {
                //         // if ($(e.target).is('#notification-button')) {
                //         //     $('#notification-sidebar').addClass('control-sidebar-open');
                //         // }
                        
                //     }
                // });

                $('[rel="count-notifications"]').html('');

                if ( $('#dropdown-notifications-menu').hasClass('open-by-cus') ) {
                    $('#dropdown-notifications-menu').removeClass('open-by-cus');
                    $('#dropdown-notifications-menu ul.dropdown-menu').css('display','none');
                } else {
                    $('#dropdown-notifications-menu').addClass('open-by-cus');
                    $('#dropdown-notifications-menu ul.dropdown-menu').css('display','block');
                }
            });
        }

        //click-to-detail
        if (! $('[rel="click-detail-notification"]').length == 0) {
            $('[rel="click-detail-notification"]').on('click', function(){
                var that = $(this);
                var id = that.data('id');
                var ajax_url = that.data('url');
                $.ajax({
                    url: ajax_url,
                    type: 'POST',
                    //dataType: 'json',
                    data: {'id': id},
                    success : function(data) {
                        if (data.notification) {
                            var html = '<h3 class="control-sidebar-heading">#'+ data.notification['id'] +'</h3><h4>Title : '+ data.notification['title'] +'</h4><label class="control-sidebar-subheading">Description :'+ data.notification['description'] +'</label><p>'+ data.notification['created_at'] +'</p>';
                            $('#notification-detail-tab').empty().append(html);

                            $('#notification-detail-tab-button').tab('show');

                            //new
                            that.find('[rel="new-notification-icon"]').removeClass('fa-bell-o bg-red');
                            that.find('[rel="new-notification-icon"]').addClass('fa-check bg-green');

                            setTimeout(function(){ 
                                that.parents('li').find('.tooltip').remove();
                                that.parents('li').find('[rel="check-readed-notification"]').remove();
                            }, 1000);
                        }
                        
                    }
                });
            });
        }


        //rel=check-readed-notification
        if(! $('[rel="check-readed-notification"]').length == 0) {
            $('[rel="check-readed-notification"]').on('click', function() {

                var user_id = $('meta[name="user_id"]').attr('content');
                var that = $(this);
                var ajax_url = that.data('url');
                $.ajax({
                    url: ajax_url,
                    type: 'POST',
                    //dataType: 'json',
                    data: {'user_id': user_id},
                    success : function(data) {
                        if (data.msg == 'success') {

                            //new
                            that.parents('li').find('[rel="new-notification-icon"]').removeClass('fa-bell-o bg-red');
                            that.parents('li').find('[rel="new-notification-icon"]').addClass('fa-check bg-green');

                            that.fadeOut();
                            setTimeout(function(){ 
                                that.parents('li').find('.tooltip').remove();
                                that.remove();
                            }, 1000);
                        }
                    }
                });
            });
        }

        if (! $('#new-notification-list').length == 0) {
            $('#new-notification-list li').hover(
                function() {
                    $( this ).find('[rel="check-readed-notification"]').fadeIn();;
                }, function() {
                    $( this ).find('[rel="check-readed-notification"]').fadeOut();
                }
            );
        }

        if (! $('#btn-view-more-notification').length == 0) {
            $('#btn-view-more-notification').on('click', function(){
                var that = $(this);
                var index = that.attr('data-index');
                var ajax_url = that.data('url');
                $.ajax({
                    url: ajax_url,
                    type: 'POST',
                    dataType: 'json',
                    data: {'index': index},
                    beforeSend : function() {
                        $('#loading-img').fadeIn();
                        that.fadeOut();
                    },
                    success : function(data) {
                        if (data) {
                            $('#loading-img').fadeOut();
                            that.fadeIn();

                            console.log(data);
                            $('#readed-notification-list').append(data.html).fadeIn();
                            that.attr('data-index',data.index);
                            if (data.index == 'end') {
                                that.remove();
                            }
                        }
                    }
                });
            });
        }

        if (! $('#notification-sidebar').length == 0) {
            // $('body').on('click', function(e) {
            //     e.stopPropagation();
                
            //     if (! $(e.target).is('#notification-sidebar') && ! $(e.target).is('#notification-button')) {
            //         $('#notification-sidebar').removeClass('control-sidebar-open');

            //         $('#dropdown-notifications-menu').removeClass('open-by-cus');
            //         $('#dropdown-notifications-menu ul.dropdown-menu').css('display','none');
            //     }
            //     else {
            //         // if ($(e.target).is('#notification-button')) {
            //         //     $('#notification-sidebar').addClass('control-sidebar-open');
            //         // }
                    
            //     }
            // });

        }

        




    },

    // more: function() {

    //     //#notification-button
    //     if (! $('#notification-button').length == 0) {
    //         $('#notification-button').on('click', function(e){
    //             $('[rel="count-notifications"]').html('');

    //             if ( $('#dropdown-notifications-menu').hasClass('open-by-cus') ) {
    //                 $('#dropdown-notifications-menu').removeClass('open-by-cus');
    //                 $('#dropdown-notifications-menu ul.dropdown-menu').css('display','none');
    //                 //$('#dropdown-notifications-menu').removeClass('open');
    //             } else {
    //                 $('#dropdown-notifications-menu').addClass('open-by-cus');
    //                 //$('#dropdown-notifications-menu').removeClass('open');
    //                 $('#dropdown-notifications-menu ul.dropdown-menu').css('display','block');
    //             }
    //         });
    //     }

    //     //rel=check-readed-notification
    //     if(! $('[rel="check-readed-notification"]').length == 0) {
    //         $('[rel="check-readed-notification"]').on('click', function() {
    //             var user_id = $('meta[name="user_id"]').attr('content');
    //             var that = $(this);
    //             var ajax_url = that.data('url');
    //             $.ajax({
    //                 url: ajax_url,
    //                 type: 'POST',
    //                 //dataType: 'json',
    //                 data: {'user_id': user_id},
    //                 success : function(data) {
    //                     if (data.msg == 'success') {
    //                         that.parents('[rel="unread-item"]').removeClass('unread-item');

    //                         that.fadeOut();
    //                         setTimeout(function(){ 
    //                             that.parents('[rel="unread-item"]').find('.tooltip').remove();
    //                             that.remove();
    //                         }, 1000);
    //                     }
                        

    //                     //$(this).tooltip( "option", "disabled", true );
    //                     //$(this).fadeOut().empty().remove();
    //                 }
    //             });
    //         });
    //     }

    //     if (! $('[rel="unread-item"]').length == 0) {
    //         $('[rel="unread-item"]').hover(
    //             function() {
    //                 $( this ).find('[rel="check-readed-notification"]').fadeIn();;
    //             }, function() {
    //                 $( this ).find('[rel="check-readed-notification"]').fadeOut();
    //             }
    //         );
    //     }

    // },


    //NEW DEMO
    addNewMessageToMenu: function(data) {
        if (data) {
            var count_notification = parseInt($('[rel="count-notifications"]').html());
            if (!count_notification) {
                $('[rel="count-notifications"]').html('1');
            } else {
                $('[rel="count-notifications"]').html(count_notification + 1);
            }
            var input = '<li><a href="#detail-'+ data['id'] +'" rel="click-detail-notification" data-id="'+ data['id'] +'" data-url="notification/detail/'+ data['id'] +'"><i rel="new-notification-icon" class="menu-icon fa fa-bell-o bg-red"></i><i class="glyphicon glyphicon-ok check-read-in-control-sidebar-menu" rel="check-readed-notification" data-url="notification/readed/'+ data['id'] +'" data-toggle="tooltip" data-original-title="Đã đọc"></i><div class="menu-info"><h4 class="control-sidebar-subheading">'+ data['title'] +'</h4><p>'+ data['short_description'] +'</p><p>'+ data['created_at'] +'</p></div></a></li>';
            $('#new-notification-list').prepend(input);
        }
        
    },


    // addNewMessageToMenu: function(data) {
    //     if (data) {
    //         var count_notification = parseInt($('[rel="count-notifications"]').html());
    //         if (!count_notification) {
    //             $('[rel="count-notifications"]').html('1');
    //         } else {
    //             $('[rel="count-notifications"]').html(count_notification + 1);
    //         }


    //         var input = '<li class="unread-item"><a href="#"><div class="pull-left"><i class="fa fa-users text-aqua"></i></div><h4>'+ data['title'] +'<small><i class="fa fa-clock-o"></i>' + data['created_at'] + '</small></h4><p>'+ data['short_description'] +'</p></a>';
    //         input += '<i class="glyphicon glyphicon-ok check-read" rel="check-readed-notification" data-url="/notification/readed/'+ data['id'] +'" data-toggle="tooltip" data-original-title="Đã đọc"></i>';
    //         input += '</li>';
    //         $('#notifications-menu').prepend(input);
    //     }
        
    // },

}

$().ready(function() {
    app_server.init();
});