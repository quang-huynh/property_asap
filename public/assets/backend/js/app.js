'use strict';

var app = {
    init: function() {
        this.common();
        this.initajax();
        this.typeahead();
    },
    
    common: function() {
        if (!$('input[rel="icheck"]').length == 0) { 
            $(function () {
                $('input[rel="icheck"]').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
                  increaseArea: '20%' // optional
                });
            });
        }

        if (!$('#pre-selected-options').length == 0) { 
            $('#pre-selected-options').multiSelect();
        }
    },

    initajax: function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content') }
        });
    },


    typeahead: function() {
        if (!$('.typeahead').length == 0) { 
            var url = $('.typeahead').data('url');

            // var substringMatcher = function(strs) {
            //     return function findMatches(q, cb) {
            //         var matches, substrRegex;
                 
            //         // an array that will be populated with substring matches
            //         matches = [];
                 
            //         // regex used to determine if a string contains the substring `q`
            //         substrRegex = new RegExp(q, 'i');
                 
            //         // iterate through the pool of strings and for any string that
            //         // contains the substring `q`, add it to the `matches` array
            //         //console.log(strs);
            //         $.each(strs, function(i, str) {
            //           if (substrRegex.test(str)) {
            //             matches.push(str);
            //           }
            //         });
                 
            //         cb(matches);
            //     };
            // };
             
            // var fullname =  $.ajax({
            //                     type: 'POST',
            //                     url: url,
            //                     //data: 'fullname=' + query,
            //                     //dataType: 'json',
            //                     async: false,
            //                     success: function(data){
            //                         //return data;
            //                         //console.log(data);
            //                         return data;
            //                    }
            //                 });

            // console.log(fullname);
             
            // $('.typeahead').typeahead({
            //     hint: true,
            //     highlight: true,
            //     minLength: 3
            // },
            // {
            //     name: 'fullname',
            //     source: substringMatcher(fullname.responseJSON)
            // });

            // $('#typeahead').typeahead({
            //     hint: true,
            //     highlight: true,
            //     minLength: 3
            // },
            // {
            //     name: 'fullname',
            //     source: function(query, process){
            //         return $.ajax({
            //             headers: { 'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content') },
            //             type: 'POST',
            //             url: url,
            //             data: 'fullname=' + query,
            //             dataType: 'json',
            //             async: false,
            //             success: function(data){
            //                 //return data;
            //                 //console.log(data.result);
            //                 return process(data);
            //            }
            //         });
            //     }
            // });
            // 
            
            var fullname = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              prefetch: url,
              remote: {
                url: url + '?keyword=%QUERY',
                wildcard: '%QUERY',
                // transform: function(res) {
                //     console.log(res);
                // }
              }
            });
             
            $('#typeahead').typeahead(null, {
              source: fullname,
              //display: 'value' //key
            }).on('typeahead:asyncrequest', function() {
                $('.Typeahead-spinner').show();
            })
              .on('typeahead:asynccancel typeahead:asyncreceive', function() {
                $('.Typeahead-spinner').hide();
            });

            // function typeaheadData(q, async, sync) {
            //     fullname.search(q, async, sync);
            // }
            

            $('#typeahead').on('typeahead:select', function(ev, suggestion) {
                console.log($('#form-search').length);
                $(this).parents( "#form-search" ).submit();
                // $('#submit-btn').click();
                console.log('Selection: ' + suggestion);
            });


        }
    }

}

$().ready(function() {
    app.init();
});