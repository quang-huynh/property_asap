<?php

namespace App;


// use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Entities\User as UserPA;

class User extends UserPA {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='user';
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
