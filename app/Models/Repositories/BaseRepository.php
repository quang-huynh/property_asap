<?php namespace App\Models\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository {
    
    protected $model;

    public function __construct(Model $model = null)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    

    public function getByAttribute($attributeName, $attributeValue)
    {
        return $this->model->where($attributeName, $attributeValue)->first();
    }

    public function getAllByAttribute($attributeName, $attributeValue)
    {
        return $this->model->where($attributeName, $attributeValue)->get();
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getByIdOrSlug($idOrSLug)
    {
        try {
            return $this->model->where('id', $idOrSLug)
                ->orWhere('slug', $idOrSLug)
                ->first();
        } catch (Exception $e) {
            return $this->getById($idOrSLug);
        }
    }

    public function createModelInstance($attributes = array())
    {
        $instance = $this->model->newInstance();

        if ($attributes) {
            foreach ($attributes as $k => $v) {
                $instance->$k = $v;
            }
        }

        return $instance;
    }

    public function delete($model)
    {
        return $model->delete();
    }

    public function save($data)
    {
        if ($data instanceOf Model) {
            $model = $data;

            if ($model->getDirty()) {
                return $model->save();
            } else {
                return $model->touch();
            }
        } elseif (is_array($data)) {
            $model = $this->createModelInstance($data);

            return $this->save($model);
        }
    }


    public function getPaginate($limit)
    {
        return $this->getModel()->orderBy('id', 'DESC')->paginate($limit);
    }

}
