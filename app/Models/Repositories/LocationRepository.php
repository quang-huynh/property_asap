<?php namespace App\Models\Repositories;

use App\Models\Entities\Location;
use DB;


class LocationRepository extends BaseRepository {

    protected $model;

    public function __construct(Location $model)
    {
        $this->model = $model;
    }


    /**
     * Add Post with return instance
     * @return
     */
    public function saveLocation($data)
    {

        $instance = $this->createModelInstance($data);

        $this->save($instance);

        return $instance;
    }


    public function updateAttributeById($id, $attribute, $value)
    {
    	$post = $this->getById($id);
		if (!$post) {
			return false;
		}
    	try {
	    	$post->$attribute = $value;
	    	return $post->save();
    	} catch (Exception $ex) {
    		return false;
    	}

    }


    public function findbySlugTypeParentId($slug, $type, $parentId = '')
    {
        return $this->model->where('slug', $slug)->where('type', $type)->where('parent_id', $parentId)->first();
    }


    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * [queryWhereRaw description]
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $user = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $user->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                } else {
                    $user->orWhereRaw($query);
                }
            }
            $i++;

        }
        return $user;
    }

}