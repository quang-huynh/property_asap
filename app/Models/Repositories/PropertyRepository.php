<?php namespace App\Models\Repositories;

use App\Models\Entities\Property;
use DB;


class PropertyRepository extends BaseRepository {

	public function __construct(Property $model)
    {
        $this->model = $model;
    }


    public function getAllActive()
    {
        return $this->model->where('status', config('asap.status_default')['active'])->get();
    }


    public function updateAttributeById($id, $attribute, $value)
    {
    	$property = $this->getById($id);
		if (!$property) {
			return false;
		}
    	try {
	    	$property->$attribute = $value;
	    	return $property->save();
    	} catch (Exception $ex) {
    		return false;
    	}
    }


    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * [queryWhereRaw description]
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $user = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $user->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                } else {
                    $user->orWhereRaw($query);
                }
            }   
            $i++;

        }
        return $user;
        
    }

}