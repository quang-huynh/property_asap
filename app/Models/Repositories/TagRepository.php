<?php namespace App\Models\Repositories;

use App\Models\Entities\Tag;
use DB;


class TagRepository extends BaseRepository {

	public function __construct(Tag $model)
    {
        $this->model = $model;
    }

    /**
     * Add tag with return instance
     * @return
     */
    public function saveTag($data)
    {

        $instance = $this->createModelInstance($data);

        $this->save($instance);

        return $instance;
    }


    public function updateAttributeById($id, $attribute, $value)
    {
    	$tag = $this->getById($id);
		if (!$tag) {
			return false;
		}
    	try {
	    	$tag->$attribute = $value;
	    	return $tag->save();
    	} catch (Exception $ex) {
    		return false;
    	}
    }


    public function findbySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }


    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * [queryWhereRaw description]
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $tag = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $tag->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $tag->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $tag->orderBy($column, $sort);
                    }
                } else {
                    $tag->orWhereRaw($query);
                }
            }   
            $i++;

        }
        return $tag;
        
    }


    /**
     * Get popular tag with limit (if not)
     * @param  integer $limit limit tag
     * @return collection tags
     */
    public function getPopularTags($limit = null)
    {
        $tags = $this->getQuery();

        if ($limit != null) {
            $tags->offset(0)->limit($limit);
        }

        $tags->orderBy('popular', 'desc');
        return $tags->get();
    }

}