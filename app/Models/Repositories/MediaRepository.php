<?php namespace App\Models\Repositories;

use App\Models\Entities\Media;

use Auth;

class MediaRepository extends BaseRepository {

    public function __construct(Media $model)
    {
        $this->model = $model;
    }


    /**
     * Add Permission in Backend
     * @return
     */
    public function add($data)
    {
        if ($data) {
            $arrData = [
                'slug' => $data['slug'],
                'name' => $data['name'],
                'size' => $data['size'],
                'extension' => $data['extension'],
                'type' => $data['type'],
                'post_id' => $data['post_id'],
                'created_by' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')

            ];
            $instance = $this->createModelInstance($arrData);

            $this->save($instance);

            return $instance;
        }
    }


    public function getByPostId($postId)
    {
        return $this->model->where('post_id', $postId)->get();
    }


    public function removeFeaturedImages($postId)
    {
        return $this->model->where('post_id', $postId)->update(['is_featured' => 0]);
    }


    /**
     * Add media with return instance
     * @return
     */
    public function saveMedia($data)
    {

        $instance = $this->createModelInstance($data);

        $this->save($instance);

        return $instance;
    }



    public function deleteByPostId($postId)
    {
        return $this->model->where('post_id', '=', $postId)->delete();
    }

    
}