<?php namespace App\Models\Repositories;

use App\Models\Entities\Notification;

class NotificationRepository extends BaseRepository
{
    protected $model;

    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    /**
     * Add Permission in Backend
     * @return
     */
    public function add($data)
    {
        if ($data) {
            $arrData = [
                'title' => $data['title'],
                'description' => $data['description'],
                'short_description' => $data['short_description'],
                'subscribe_event_id' => $data['subscribe_event_id'],

            ];
            $instance = $this->createModelInstance($arrData);

            $this->save($instance);

            return $instance;
        }
    }


    public function getAllByAttributeOrderBy($attributeName, $attributeValue, $column, $orderby = 'DESC')
    {
        return $this->model->where($attributeName, $attributeValue)->where('created_at','>',\Auth::user()->created_at)->orderBy($column, $orderby)->get();
    }


    public function getAllByAttributeOrderBy($attributeName, $attributeValue, $column, $orDerby = 'DESC')
    {
        return $this->model->where($attributeName, $attributeValue)
            ->where('created_at', '>', \Auth::user()->created_at)
            ->orderBy($column, $orDerby)->get();
    }

}