<?php namespace App\Models\Repositories;

use App\Models\Entities\Post;
use DB;
use Auth;


class PostRepository extends BaseRepository {

	public function __construct(Post $model)
    {
        $this->model = $model;
    }



    /**
     * Add Post with return instance
     * @return
     */
    public function savePost($data)
    {

        $instance = $this->createModelInstance($data);

        $this->save($instance);

        return $instance;
    }



    public function updateAttributeById($id, $attribute, $value)
    {
    	$post = $this->getById($id);
		if (!$post) {
			return false;
		}
    	try {
	    	$post->$attribute = $value;
	    	return $post->save();
    	} catch (Exception $ex) {
    		return false;
    	}
    }


    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }



    /**
     * 
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $post = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $post->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $post->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $post->orderBy($column, $sort);
                    }
                } else {
                    $post->orWhereRaw($query);
                }
            }   
            $i++;

        }
        return $post;
        
    }


    public function getFeaturePosts($now, $expired, $limit)
    {
        $posts = $this->model->where('status', '=', config('asap.post_status')['published'] )
                            ->where('package', '=', '3')
                            ->whereRaw('published_date_count +' . $expired . ' >= ' . $now)
                            ->orderBy('published_date_count', 'DESC')
                            ->skip(0)
                            ->take($limit)
                            ->get();
        return $posts;
    }


    public function getMyProperties()
    {
        return $this->model->where('created_by', '=', Auth::user()->id)->get();
    }


    public function getMyPendingProperties()
    {
        return $this->model->where('created_by', '=', Auth::user()->id)->where('status', config('asap.post_status')['pending'])->get();
    }


    public function getPendingPaginate($limit)
    {
        return $this->getModel()->where('status', config('asap.post_status')['pending'])->orderBy('id', 'DESC')->paginate($limit);
    }



    /**
     * Search module
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function callSearchAdvanced($where, $sortBy = ['value' => 1,'column' => '', 'sort' => 'DESC'], $limit = 0, $offset = 0)
    {
        $postTable = 'p';
        $postTagTable = 'pt';

        $query1 = "
                    Select $postTable.* 
                    FROM post as $postTable 
                    WHERE $postTable.status IN (" . config('asap.post_status')['published'] . ',' . config('asap.post_status')['closed'] . ")";


        if (isset($where['ids'])) {
            $query1 .= " AND $postTable.id IN (" . $where['ids'] . ")";
        }

        if (!empty($where['id'])) {
            $query1 .= " AND $postTable.id != " . $where['id'];
        }

        if (isset($where['location_country'])) {
            $query1 .= " AND $postTable.country = '" . $where['location_country'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state'])) {
            $query1 .= " AND $postTable.state = '" . $where['location_state'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city'])) {
            $query1 .= " AND $postTable.city = '" . $where['location_city'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city']) && isset($where['location_street'])) {
            $query1 .= " AND $postTable.street = '" . $where['location_street'] . "'";
        }

        if (isset($where['property_type'])) {
            $query1 .= " AND $postTable.property_type IN (" . $where['property_type'] . ")";
        }
        if (isset($where['bathrooms'])) {
            $query1 .= " AND $postTable.bathrooms IN (" . $where['bathrooms'] . ")";
        }
        if (isset($where['bedrooms'])) {
            $query1 .= " AND $postTable.bedrooms IN (" . $where['bedrooms'] . ")";
        }
        if (isset($where['car_spaces'])) {
            $query1 .= " AND $postTable.car_spaces IN (" . $where['car_spaces'] . ")";
        }
        if (isset($where['size'])) {
            $query1 .= " AND $postTable.size " . $where['size_compare'] . " " . $where['size'];
        }
        if (isset($where['min_price']) && isset($where['max_price']) ) {
            $query1 .= " AND ($postTable.price BETWEEN ". $where['min_price'] ." AND ". $where['max_price'] .") ";
        }
        if (isset($where['tags'])) {
            $query1 .= " AND $postTagTable.tag_id IN (". $where['tags'] .")";
        }

        $query1 .= " GROUP BY p.id ";

        

        $query2 = "
                    Select $postTable.* 
                    FROM post as $postTable 
                    WHERE $postTable.status IN (" . config('asap.post_status')['published'] . ',' . config('asap.post_status')['closed'] . ")";


        if (isset($where['ids'])) {
            $query2 .= " AND $postTable.id IN (" . $where['ids'] . ")";
        }

        if (!empty($where['id'])) {
            $query2 .= " AND $postTable.id != " . $where['id'];
        }

        if (isset($where['location_country'])) {
            $query2 .= " AND $postTable.country = '" . $where['location_country'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state'])) {
            $query2 .= " AND $postTable.state = '" . $where['location_state'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city'])) {
            $query2 .= " AND $postTable.city = '" . $where['location_city'] . "'";
        }

        if (isset($where['property_type'])) {
            $query2 .= " AND $postTable.property_type IN (" . $where['property_type'] . ")";
        }
        if (isset($where['bathrooms'])) {
            $query2 .= " AND $postTable.bathrooms IN (" . $where['bathrooms'] . ")";
        }
        if (isset($where['bedrooms'])) {
            $query2 .= " AND $postTable.bedrooms IN (" . $where['bedrooms'] . ")";
        }
        if (isset($where['car_spaces'])) {
            $query2 .= " AND $postTable.car_spaces IN (" . $where['car_spaces'] . ")";
        }
        if (isset($where['size'])) {
            $query2 .= " AND $postTable.size " . $where['size_compare'] . " " . $where['size'];
        }
        if (isset($where['min_price']) && isset($where['max_price']) ) {
            $query2 .= " AND ($postTable.price BETWEEN ". $where['min_price'] ." AND ". $where['max_price'] .") ";
        }
        if (isset($where['tags'])) {
            $query2 .= " AND $postTagTable.tag_id IN (". $where['tags'] .")";
        }

        $query2 .= " GROUP BY p.id ";


        $sortMulti = '';
        if (!empty($sortBy['top'])) {
            $sortMulti = " , published_date DESC";
        }

        $mainQuery = "SELECT t.*
                        FROM ( $query1 UNION ALL $query2 ) t
                        GROUP BY t.id 
                        ORDER BY t.".$sortBy['column'] ." ". $sortBy['sort'] . $sortMulti . " LIMIT ". $limit . " OFFSET $offset";

        // DB::enableQueryLog();  
        $posts = DB::select($mainQuery);
        // dd(DB::getQueryLog());

        //convert std class to Model Post
        $posts = Post::hydrate($posts);

        return $posts;

    }



    public function callSearchAdmin($where)
    {
        $postTable = 'p';
        $postTagTable = 'pt';

        $query1 = "
                    Select $postTable.* 
                    FROM post as $postTable 
                    INNER JOIN post_tag as $postTagTable ON $postTable.id = $postTagTable.post_id
                    WHERE 1=1 ";

        if (isset($where['location_country'])) {
            $query1 .= " AND $postTable.country = '" . $where['location_country'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state'])) {
            $query1 .= " AND $postTable.state = '" . $where['location_state'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city'])) {
            $query1 .= " AND $postTable.city = '" . $where['location_city'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city']) && isset($where['location_street'])) {
            $query1 .= " AND $postTable.street = '" . $where['location_street'] . "'";
        }

        if (isset($where['id_title'])) {
            $query1 .= " AND ($postTable.title LIKE '%" . $where['id_title'] . "%' OR  $postTable.id LIKE '" . $where['id_title'] . "') ";
        }
        if (isset($where['post_id'])) {
            $query1 .= " AND $postTable.created_by = " . $where['post_id'];
        }
        if (isset($where['status'])) {
            $query1 .= " AND $postTable.status = " . $where['status'];
        }
        if (isset($where['package'])) {
            $query1 .= " AND $postTable.package = " . $where['package'];
        }

        $query1 .= " GROUP BY p.id ";

        

        $query2 = "
                    Select $postTable.* 
                    FROM post as $postTable 
                    INNER JOIN post_tag as $postTagTable ON $postTable.id = $postTagTable.post_id
                    WHERE 1=1 ";


        if (isset($where['location_country'])) {
            $query2 .= " AND $postTable.country = '" . $where['location_country'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state'])) {
            $query2 .= " AND $postTable.state = '" . $where['location_state'] . "'";
        }
        if (isset($where['location_country']) && isset($where['location_state']) && isset($where['location_city'])) {
            $query2 .= " AND $postTable.city = '" . $where['location_city'] . "'";
        }

        if (isset($where['id_title'])) {
            $query2 .= " AND ($postTable.title LIKE '%" . $where['id_title'] . "%' OR  $postTable.id LIKE '" . $where['id_title'] . "') ";
        }
        if (isset($where['post_id'])) {
            $query2 .= " AND $postTable.created_by = " . $where['post_id'];
        }
        if (isset($where['status'])) {
            $query2 .= " AND $postTable.status = " . $where['status'];
        }
        if (isset($where['package'])) {
            $query2 .= " AND $postTable.package = " . $where['package'];
        }

        $query2 .= " GROUP BY p.id ";


        $mainQuery = "SELECT t.*
                        FROM ( $query1 UNION ALL $query2 ) t
                        GROUP BY t.id 
                        ORDER BY t.id DESC";

        // DB::enableQueryLog();  
        $posts = DB::select($mainQuery);
        // dd(DB::getQueryLog());

        //convert std class to Model Post
        $posts = Post::hydrate($posts);

        return $posts;
    }




    public function changeStatus($postId, $status)
    {
        $post = $this->getById($postId);

        if (!$post) {
            return false;
        }

        $post->status = $status;

        return $this->save($post);
    }



}