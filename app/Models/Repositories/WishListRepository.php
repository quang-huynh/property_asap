<?php namespace App\Models\Repositories;

use App\Models\Entities\Wishlist;
use DB;

use Auth;

class WishlistRepository extends BaseRepository {

	public function __construct(Wishlist $model)
    {
        $this->model = $model;
    }

    /**
     * Add wishlist with return instance
     * @return
     */
    public function saveWishlist($data)
    {

        $instance = $this->createModelInstance($data);

        $this->save($instance);

        return $instance;
    }


    public function updateAttributeById($id, $attribute, $value)
    {
    	$wishlist = $this->getById($id);
		if (!$wishlist) {
			return false;
		}
    	try {
	    	$wishlist->$attribute = $value;
	    	return $wishlist->save();
    	} catch (Exception $ex) {
    		return false;
    	}
    }


    public function findbySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }


    public function findByUserId()
    {
        return $this->model->where('user_id', Auth::user()->id)->get();
    }


    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * [queryWhereRaw description]
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $wishlist = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $wishlist->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $wishlist->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $wishlist->orderBy($column, $sort);
                    }
                } else {
                    $wishlist->orWhereRaw($query);
                }
            }   
            $i++;

        }
        return $wishlist;
        
    }


    public function findByPostId($postId)
    {
        if (!empty(Auth::user())) {
            return $this->model->where('post_id', $postId)->where('user_id', Auth::user()->id)->first();
        }
        return false;
    }


    public function removeByPostId($postId)
    {
        if (!empty(Auth::user())) {
            return $this->model->where('post_id', $postId)->where('user_id', Auth::user()->id)->delete();
        }
        return false;
    }

}