<?php namespace App\Models\Entities;

class Notification extends Base {

    /**
     * The database table used by the model
     * 
     * @var string
     */
    protected $table = 'notification';

    /**
     * Define relationship: Users have many Notifications, Notifications have many Users.
     * 
     * @return App\Models\Entities\User Collection 
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\Entities\User', 'notification_user', 'notification_id', 'user_id');
    }

    /**
     * Define relationship: Notification belongs to a Subscribe Event, Subscribe Event has many Notification.
     * 
     * @return App\Models\Entities\SubscribeEvent Object
     */
    public function subscribeEvent()
    {
        return $this->belongsTo('App\Models\Entities\SubscribeEvent');
    }


    /**
     * Get name attribute
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get description attribute
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get short description attribute
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }


    /**
     * Get subscribe event id attribute
     * @return string
     */
    public function getSubscribeEventId()
    {
        return $this->subscribe_event_id;
    }

}