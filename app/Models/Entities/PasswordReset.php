<?php namespace App\Models\Entities;

class PasswordReset extends Base
{

    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'password_reset';

    public $timestamps = false;

    protected $fillable = [
      'email',
      'token'
    ];

    protected $hidden = ['created_at'];

    public static function getLastPasswordReset($email = '', $token = '')
    {
        if (!empty($email) && !empty($token)) {
            $passwordReset = self::where('email', $email)
              ->where('token', $token)
              ->orderBy('created_at', 'desc')
              ->first();
            return $passwordReset;
        }
        return null;
    }
}