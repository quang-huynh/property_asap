<?php namespace App\Models\Entities;

class Tag extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'tag';


	/**
	 * Define relationship: Post has many Tags , Tag has many Posts
	 * @return App\Models\Entities\Post Object
	 */
	public function posts()
	{
		return $this->belongsToMany('App\Models\Entities\Post', 'post_tag', 'tag_id', 'post_id');
	}


	/**
     * Get parent tag
     *
     * @return  App\Models\Entities\Tag Object
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Entities\Tag', 'parent_id', 'id');
    }


    /**
     * Get children tag
     * @return App\Models\Entities\Tag Collection
     */
    public function children()
    {
        return $this->hasMany('App\Models\Entities\Tag', 'id', 'parent_id');
    } 


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Get parent id attribute
	 * @return string
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

}