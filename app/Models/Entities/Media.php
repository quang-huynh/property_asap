<?php namespace App\Models\Entities;

class Media extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'media';


	/**
	 * Define relationship: Media belongs to a post, Post has many Media
	 * @return App\Models\Entities\Post Object
	 */
	public function post()
	{
		return $this->belongsTo('App\Models\Entities\Post', 'post_id');
	}

	/**
	 * Define relationship: media belongs to a user, User has many media
	 * @return App\Models\Entities\Log Object
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Entities\User', 'created_by');
	}


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Get size attribute
	 * @return string
	 */
	public function getSize()
	{
		return $this->size;
	}


	/**
	 * Get extension attribute
	 * @return string
	 */
	public function getExtension()
	{
		return $this->extension;
	}



	/**
	 * Get type attribute
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}


	/**
	 * Get post id attribute
	 * @return string
	 */
	public function getPostId()
	{
		return $this->post_id;
	}

	/**
	 * Get user id attribute
	 * @return string
	 */
	public function getCreatedBy()
	{
		return $this->created_by;
	}

}