<?php namespace App\Models\Entities;

class Role extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'role';

	/**
	 * Define relationship: Users have many Roles, Roles have many Users.
	 * 
	 * @return App\Models\Entities\User Collection 
	 */
	public function users()
	{
		return $this->belongsToMany('App\Models\Entities\User', 'role_user', 'role_id', 'user_id');
	}

	/**
	 * Define relationship: Roles have many Permissions, Permissions have many Roles.
	 * 
	 * @return App\Models\Entities\Permission Collection 
	 */
	public function permissions()
	{
		return $this->belongsToMany('App\Models\Entities\Permission', 'permission_role', 'role_id', 'permission_id');
	}



	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()

	{
		return $this->name;
	}

	/**
	 * Get description attribute
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

}