<?php namespace App\Models\Entities;

class Wishlist extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'wishlist';


	/**
	 * Define relationship: Wishlist belong to a Post , Post has many Wishlists
	 * @return App\Models\Entities\Post Object
	 */
	public function post()
	{
		return $this->belongsTo('App\Models\Entities\Post', 'post_id');
	}



	/**
	 * Define relationship: Wishlist belong to a User , User has many Wishlists
	 * @return App\Models\Entities\User Object
	 */
	public function user()
	{
		return $this->belongsToMany('App\Models\Entities\User', 'user_id');
	}


	/**
	 * Get user id attribute
	 * @return integer
	 */
	public function getUserId()
	{
		return $this->user_id;
	}


	/**
	 * Get post id attribute
	 * @return integer
	 */
	public function getPostId()
	{
		return $this->post_id;
	}


	/**
	 * Get noted attribute
	 * @return integer
	 */
	public function getNoted()
	{
		return $this->noted;
	}


	/**
	 * Get status attribute
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}

}