<?php namespace App\Models\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Base implements AuthenticatableContract, CanResetPasswordContract
{

    const GENDER_NONE = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_MALE_TEXT = 'Male';
    const GENDER_FEMALE_TEXT = 'Female';


    static $genders = [
      '' => 'None',
      self::GENDER_MALE => self::GENDER_MALE_TEXT,
      self::GENDER_FEMALE => self::GENDER_FEMALE_TEXT,
    ];


    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'fullname',
      'email',
      'address',
      'birthday',
      'phone_number',
      'gender',
      'status',
      'role_id',
      'is_member',
      'is_admin',
      'facebook_id',
      'facebook_token',
      'facebook_json',
      'google_id',
      'google_token',
      'google_json'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'created_at', 'updated_at', 'is_deleted'];


    protected $appends = ['gender_dis', 'picture_full_path'];



	/**
	 * Define relationship: Users have many Notifications, Notifications have many Users.
	 * 
	 * @return App\Models\Entities\Notification Collection
	 */
	public function notifications()
	{
		return $this->belongsToMany('App\Models\Entities\Notification', 'notification_user', 'user_id', 'notification_id');
	}


	/**
	 * Define relationship: Users have many Roles, Roles have many Users.
	 * 
	 * @return App\Models\Entities\Role Collection
	 */
	public function posts()
	{
		return $this->hasMany('App\Models\Entities\Post', 'created_by');
	}



	/**
	 * Define relationship: Users have many Roles, Roles have many Users.
	 * 
	 * @return App\Models\Entities\Role Collection
	 */
	public function roles()
	{
		return $this->belongsToMany('App\Models\Entities\Role', 'role_user', 'user_id', 'role_id');
	}

	public function getGenderDisAttribute() {
        if (!is_null($this->gender) && !empty($this->gender)) {
            $genders = self::$genders;
         	return isset($genders[$this->gender]) ? $genders[$this->gender] : 'None';
        }
        return 'None';
	}



    public function getPictureFullPathAttribute()
    {
        if (!empty($this->picture)) {
            return url(	config('asap.upload_profile_picture_url') . $this->picture);
        }
        return "http://placehold.it/100X100";
    }

    /*
    * return list admin
    */
    static function getListAdmin()
    {
        $listAdmin = User::where(['is_admin' => 1, 'is_deleted' => 0])->get();
        return $listAdmin;
    }

    /*
    * return list admin
    */
    static function getListMember()
    {
        $listMember = User::where(['is_member' => 1, 'is_deleted' => 0])->get();
        return $listMember;
    }


    public static function allMail()
    {
        $data = self::get()->pluck('email')->toArray();
        return $data;
    }



    /**
	 * Get birthday attribute
	 * @return string
	 */
	public function getBirthday()
	{
		return $this->birthday;
	}


	/**
	 * Get phone number attribute
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phone_number;
	}


	/**
	 * Get address attribute
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}


	/**
	 * Get email attribute
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}


	/**
	 * Get gender attribute
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}


	/**
	 * Get status attribute
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}


	/**
	 * Get fullname attribute
	 * @return string
	 */
	public function getFullName()
	{
		return $this->fullname;
	}
	

	/**
	 * Get phone attribute
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Get password attribute
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}


	public function getProfilePicture()
	{
		if ($this->picture) {
			return config('asap.profile_picture_url') . $this->picture;
		} else {
			return config('asap.default_profile_picture');
		}
	}

}