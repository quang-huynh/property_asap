<?php namespace App\Models\Entities;

class Log extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'log';


	/**
	 * Define relationship: Log belongs to a user, User has many Log
	 * @return App\Models\Entities\Log Object
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Entities\User', 'created_by');
	}


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get text attribute
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * Get text attribute
	 * @return integer
	 */
	public function getCreatedBy()
	{
		return $this->created_by;
	}	

}