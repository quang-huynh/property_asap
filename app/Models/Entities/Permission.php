<?php namespace App\Models\Entities;

class Permission extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'permission';

	/**
	 * Define relationship: Permissions have many Roles, Roles have many Permissions.
	 * 
	 * @return App\Models\Entities\Role Collection 
	 */
	public function roles()
	{
		return $this->belongsToMany('App\Models\Entities\Role', 'permission_role', 'permission_id', 'role_id');
	}

    /**
     * Get title attribute
     * 
     * @return string
     */
    public function getTitle()
    {
    	return $this->title;
    }

    /**
     * Get route_name attribute
     * 
     * @return string
     */
    public function getRouteName()
    {
    	return $this->route_name;
    }

    /**
     * Get description attribute
     * 
     * @return string
     */
    public function getDescription()
    {
    	return $this->description;
    }
}