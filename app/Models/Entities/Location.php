<?php namespace App\Models\Entities;

class Location extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'location';


	/**
	 * Define relationship: Post has many Locations , Location has many Posts
	 * @return App\Models\Entities\Post Object
	 */
	public function posts()
	{
		return $this->belongsToMany('App\Models\Entities\Post', 'post_location', 'location_id', 'post_id')->withPivot('noted');
	}


	/**
     * Get parent category
     *
     * @return  App\Models\Entities\Category Object
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Entities\Category', 'parent_id', 'id');
    }

    /**
     * Get children category
     * @return App\Models\Entities\Category Collection
     */
    public function children()
    {
        return $this->hasMany('App\Models\Entities\Category', 'id', 'parent_id');
    } 


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Get parent id attribute
	 * @return integer
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

}