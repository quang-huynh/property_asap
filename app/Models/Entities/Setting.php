<?php namespace App\Models\Entities;

class Location extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'setting';


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Get code attribute
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}


	/**
	 * Get value attribute
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}


	/**
	 * Get value attribute
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}


	/**
	 * Get status attribute
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}


}