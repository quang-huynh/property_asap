<?php namespace App\Models\Entities;

class Property extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'property';


	/**
	 * Define relationship: Post has many Properties , Property has many Posts
	 * @return Post Object
	 */
	public function posts()
	{
		return $this->belongsToMany(Post::class, 'post_property', 'property_id', 'post_id')->withPivot('text','value');
	}


	/**
     * Get parent Property
     *
     * @return  Property Object
     */
    public function parent()
    {
        return $this->belongsTo(Property::class, 'parent_id', 'id');
    }


    /**
     * Get children Property
     * @return App\Models\Entities\Property Collection
     */
    public function children()
    {
        return $this->hasMay(Property::class, 'id', 'parent_id');
    } 


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Get status attribute
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}


	/**
	 * Get parent id attribute
	 * @return integer
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

}