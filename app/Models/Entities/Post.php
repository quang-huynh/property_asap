<?php namespace App\Models\Entities;

class Post extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'post';


	/**
	 * Define relationship: Post belong to User , User has many Posts
	 * @return App\Models\Entities\User Object
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Entities\User', 'created_by');
	}


	public function media()
    {
        return $this->hasMany('App\Models\Entities\Media', 'post_id', 'id');
    } 


	/**
	 * Define relationship: Post has many Tags , Tags has many Posts
	 * @return App\Models\Entities\Tags Object
	 */
	public function tags()
	{
		return $this->belongsToMany('App\Models\Entities\Tag', 'post_tag', 'post_id', 'tag_id');
	}


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get title attribute
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * Get description attribute
	 * @return text
	 */
	public function getDescription()
	{
		return $this->description;
	}


	/**
	 * Get full location attribute
	 * @return string
	 */
	public function getFullLocation()
	{
		return $this->full_location;
	}


	/**
	 * Get street number attribute
	 * @return string
	 */
	public function getStreetNumber()
	{
		return $this->street_number;
	}


	/**
	 * Get street attribute
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}


	/**
	 * Get city attribute
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}


	/**
	 * Get state attribute
	 * @return string
	 */
	public function getState()
	{
		return $this->state;
	}


	/**
	 * Get country attribute
	 * @return string
	 */
	public function getCountry()
	{
		return $this->country;
	}


	/**
	 * Get type attribute
	 * @return integer
	 */
	public function getType()
	{
		return $this->type;
	}


	/**
	 * Get price attribute
	 * @return integer
	 */
	public function getPrice()
	{
		return $this->price;
	}


	/**
	 * Get name contact attribute
	 * @return string
	 */
	public function getNameContact()
	{
		return $this->name_contact;
	}


	/**
	 * Get email email attribute
	 * @return string
	 */
	public function getEmailContact()
	{
		return $this->email_contact;
	}


	/**
	 * Get address contact attribute
	 * @return string
	 */
	public function getAddressContact()
	{
		return $this->address_contact;
	}


	/**
	 * Get phone number attribute
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phone_number;
	}


	/**
	 * Get home number attribute
	 * @return string
	 */
	public function getHomeNumber()
	{
		return $this->home_number;
	}


	/**
	 * Get package attribute
	 * @return integer
	 */
	public function getPackage()
	{
		return $this->package;
	}


	/**
	 * Get property type attribute
	 * @return integer
	 */
	public function getPropertyType()
	{
		return $this->property_type;
	}


	/**
	 * Get bedrooms attribute
	 * @return integer
	 */
	public function getBedrooms()
	{
		return $this->bedrooms;
	}


	/**
	 * Get bathrooms attribute
	 * @return integer
	 */
	public function getBathrooms()
	{
		return $this->bathrooms;
	}


	/**
	 * Get car spaces attribute
	 * @return integer
	 */
	public function getCarSpaces()
	{
		return $this->car_spaces;
	}


	/**
	 * Get size attribute
	 * @return integer
	 */
	public function getSize()
	{
		return $this->size;
	}


	/**
	 * Get video link attribute
	 * @return string
	 */
	public function getVideoLink()
	{
		return $this->video_link;
	}


	/**
	 * Get status attribute
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}


	/**
	 * Get published_date attribute
	 * @return integer
	 */
	public function getPublishedDate()
	{
		return $this->published_date;
	}


	/**
	 * Get published_date_count attribute
	 * @return integer
	 */
	public function getPublishedDateCount()
	{
		return $this->published_date_count;
	}


	/**
	 * Get setting attribute
	 * @return string
	 */
	public function getSetting()
	{
		return $this->setting;
	}


	/**
	 * Get viewers attribute
	 * @return string
	 */
	public function getViewers()
	{
		return $this->viewers;
	}



	/**
	 * Get user id attribute
	 * @return integer
	 */
	public function getCreatedBy()
	{
		return $this->created_by;
	}

}