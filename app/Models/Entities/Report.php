<?php namespace App\Models\Entities;

class Report extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'report';


	/**
	 * Define relationship: Report belong to a User. User has many Reports.
	 * 
	 * @return App\Models\Entities\User Collection
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Entities\User', 'user_id');
	}


	/**
	 * Get slug attribute
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}


	/**
	 * Get title attribute
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * Get content attribute
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}


	/**
	 * Get target attribute
	 * @return integer
	 */
	public function getTarget()
	{
		return $this->target;
	}


	/**
	 * Get status attribute
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->target;
	}


	/**
	 * Get created by attribute
	 * @return integer
	 */
	public function getCreatedBy()
	{
		return $this->created_by;
	}

}