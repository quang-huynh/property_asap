<?php namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class PostFormRequest extends FormRequest 
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'email' => 'required|max:255',
			'location' => 'required|max:255',
			'title' => 'required|max:255',
			'description' => 'required',
			'phone_number' => 'required|max:255',
			'home_phone' => 'required|max:255',
			'address' => 'required|max:255',
			'price' => 'required|max:255',

		];
	}

	/**
	 * Customize error message content
	 * @return array 
	 */
	public function messages()
    {
        return [
            'name.required' => 'Name is not empty',
            'name.max' => 'Name is max 255 character',
            'location.required' => 'Location is not empty',
            'location.max' => 'Location is max 255 character',
            'email.required' => 'Email is empty',
            'email.max' => 'Email is max 255 character',
            'title.required' => 'Title is empty',
            'title.max' => 'Title is max 255 character',
            'description.required' => 'Description is not empty',
            'phone_number.required' => 'Phone number is not empty',
            'phone_number.max' => 'Phone number is max 255 character',
            'home_phone.required' => 'Home phone is not empty',
            'home_phone.max' => 'Home phone is max 255 character',
            'address.required' => 'Address is not empty',
            'address.max' => 'Address is max 255 character',
            'price.required' => 'Price is not empty',
            'price.max' => 'Price is max 255 character',
        ];
    }

}