<?php namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class EditMeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'fullname' => 'required|max:255',
          'address' => 'required|max:255',
          'birthday' => 'required',
          'phone_number' => 'required',
          'gender' => 'required|in:1,2',
        ];
    }

}