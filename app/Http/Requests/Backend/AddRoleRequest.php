<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class AddRoleRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (\Request::isMethod('post')) {
            return [
                'name' => 'required|max:255',
            ];
        } else {
            return [];
        }
        
    


        
    }

}
