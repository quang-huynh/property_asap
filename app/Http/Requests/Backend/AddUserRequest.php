<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class AddUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if (\Request::isMethod('post')) {
			return [
				'fullname' => 'required|max:255',
				'email' => 'required|email|max:255|unique:user',
				'password' => 'required|confirmed|min:4',
				'role_id' => 'required',
				'status'	=> 'required'

			];
		} else {
			return [];
		}
		
	


		
	}

}
