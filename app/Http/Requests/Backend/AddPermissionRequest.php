<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class AddPermissionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (\Request::isMethod('post')) {
            return [
                'title' => 'required|max:255',
                'route_name' => 'required',
            ];
        } else {
            return [];
        }
        
    


        
    }

}
