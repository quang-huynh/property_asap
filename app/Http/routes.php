<?php

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Route::get('/kill-sessions', function() {
    $request->session()->flush();
});


Route::get('refresh-csrf', function(){
    return csrf_token();
});

// Route::auth();

// FRONT END

Route::any( '/', [
	'as'   => 'frontend.index',
	'uses' => '\App\Http\Controllers\Frontend\HomeController@index',
] );


Route::any( '/about', [
	'as'   => 'about',
	'uses' => '\App\Http\Controllers\Frontend\HomeController@about',
] );

Route::any( '/contact', [
	'as'   => 'contact',
	'uses' => '\App\Http\Controllers\Frontend\HomeController@contact',
] );


Route::any( '/privacy', [
	'as'   => 'privacy',
	'uses' => '\App\Http\Controllers\Frontend\HomeController@privacy',
] );

Route::any( '/terms', [
	'as'   => 'terms',
	'uses' => '\App\Http\Controllers\Frontend\HomeController@terms',
] );


Route::any('/search', [
        'as' => 'post.search', 
        'uses' => '\App\Http\Controllers\Frontend\PostController@search',
]);

Route::any('/api/post/search', [
        'as' => 'api.post.search', 
        'uses' => '\App\Http\Controllers\Frontend\PostController@apiSearch',
]);


Route::any('/property/{slug}', [
        'as' => 'post.detail', 
        'uses' => '\App\Http\Controllers\Frontend\PostController@detail',
]);


Route::get( '/register', [
	'as'   => 'register',
	'uses' => '\App\Http\Controllers\Frontend\UserController@getRegister',
] );


Route::post( '/register', [
    'as'   => 'register',
    'uses' => '\App\Http\Controllers\Frontend\UserController@postRegister',
] );

Route::any( '/login', [
    'as'   => 'login',
    'uses' => '\App\Http\Controllers\Frontend\AuthenticateController@login',
] );

Route::any( '/logout', [
    'as'   => 'logout',
    'uses' => '\App\Http\Controllers\Frontend\AuthenticateController@logout',
] );



// Route::any( '/resetpassword', [
// 	'as'   => 'resetpassword',
// 	'uses' => '\App\Http\Controllers\Frontend\UserController@resetpassword',
// ] );




Route::group(['middleware' => 'auth'], function () {

	Route::any('/create', [
        'as' => 'post.create', 
        'uses' => '\App\Http\Controllers\Frontend\PostController@create',
	]);


	Route::any('/property/{slug}/edit', [
	        'as' => 'post.edit', 
	        'uses' => '\App\Http\Controllers\Frontend\PostController@edit',
	]);
    Route::any('/property/{slug}/close', [
            'as' => 'post.close', 
            'uses' => '\App\Http\Controllers\Frontend\PostController@close',
    ]);

	Route::any('api/property/create', [
	        'as' => 'api.post.create', 
	        'uses' => '\App\Http\Controllers\Frontend\PostController@apiCreatePost',
	]);
	Route::any('api/property/update', [
	        'as' => 'api.post.update', 
	        'uses' => '\App\Http\Controllers\Frontend\PostController@apiUpdatePost',
	]);
	Route::any('/property/{id}/delete', [
	        'as' => 'post.delete', 
	        'uses' => '\App\Http\Controllers\Frontend\PostController@delete',
	]);

    Route::any('/api/wishlist/addwishlist', [
        'as' => 'api.wishlist.add_wishlist', 
        'uses' => '\App\Http\Controllers\Frontend\WishlistController@apiAdd',
    ]);

    Route::any('/api/wishlist/removewishlist', [
            'as' => 'api.wishlist.remove_wishlist', 
            'uses' => '\App\Http\Controllers\Frontend\WishlistController@apiRemove',
    ]);

});



// FRONT END
Route::group(['as' => 'frontend.auth.', 'namespace' => 'Frontend', 'middleware' => 'guest'], function () {
    // Route::get('/register/success', ['as' => 'register.success', 'uses' => 'AuthenticateController@getRegisterSuccess']);
    // Route::get('/register', ['as' => 'register', 'uses' => 'AuthenticateController@register']);
    // Route::post('/register', ['as' => 'register', 'uses' => 'AuthenticateController@postRegister']);
    Route::post('/ajax/login', ['as' => 'login', 'uses' => 'AuthenticateController@postLogin']);
    Route::post('/ajax/logout', ['as' => 'logout', 'uses' => 'AuthenticateController@postLogout']);
    Route::get('/auth/token/{email}/{token}',
      ['as' => 'emailConfirm', 'uses' => 'AuthenticateController@confirmEmail']);
    Route::get('/password/reset', ['as' => 'password.reset', 'uses' => 'AuthenticateController@getPasswordReset']);
    Route::post('/password/reset', ['as' => 'password.reset', 'uses' => 'AuthenticateController@postPasswordReset']);
    Route::get('/password/{email}/{token}/new',
      ['as' => 'password.new', 'uses' => 'AuthenticateController@getPasswordNew']);
    Route::post('/password/{email}/{token}/new',
      ['as' => 'password.new', 'uses' => 'AuthenticateController@postPasswordNew']);
    // social;
    Route::get('/auth/facebook',
      ['as' => 'facebook.callback', 'uses' => 'AuthenticateController@authFacebook']);
    Route::get('auth/facebook/callback', ['as' => 'facebook', 'uses' => 'AuthenticateController@authFacebookSocial']);

    Route::get('/auth/google',
      ['as' => 'google.callback', 'uses' => 'AuthenticateController@authGoogle']);
    Route::get('auth/google/callback', ['as' => 'google', 'uses' => 'AuthenticateController@authGoogleSocial']);
});

Route::group(['as' => 'frontend.profile.', 'namespace' => 'Frontend', 'middleware' => 'auth'], function () {
    Route::get('/edit', ['as' => 'edit', 'uses' => 'ProfileController@edit']);
    Route::post('/edit', ['as' => 'edit', 'uses' => 'ProfileController@postEdit']);
    Route::get('/change-password', ['as' => 'change-password', 'uses' => 'ProfileController@changePassword']);
    Route::post('/change-password', ['as' => 'change-password', 'uses' => 'ProfileController@postChangePassword']);
    Route::post('/me/upload-picture', ['as' => 'me.upload-picture', 'uses' => 'ProfileController@uploadPicture']);
    Route::get('/me', ['as' => 'me', 'uses' => 'ProfileController@me']);
    Route::get('/wishlist', ['as' => 'wishlist', 'uses' => 'ProfileController@wishlist']);
    Route::get('/properties', ['as' => 'properties', 'uses' => 'ProfileController@properties']);
});


Route::get('/callback', '\App\Http\Controllers\Frontend\SocialAuthController@callback');


Route::any('/resetpassword', [
  'as' => 'resetpassword',
  'uses' => '\App\Http\Controllers\Frontend\UserController@resetpassword',
]);





/* ---------------------BACK END ---------------------------------*/


Route::any('/admin/login', [
    'as' => 'admin.login', 
    'uses' => '\App\Http\Controllers\Backend\Auth\AuthController@login',
        
]);

Route::any('/admin/logout', [
    'as' => 'admin.logout', 
    'uses' => '\App\Http\Controllers\Backend\HomeController@logout',
    
]);



Route::group(['middleware' => ['auth', 'permissions'], 'prefix' => 'admin'], function()
{
    Route::any('/', [
        'as' => 'admin.index', 
        'uses' => '\App\Http\Controllers\Backend\HomeController@index',
        
    ]);

    Route::any('user/', [
        'as' => 'user.index', 
        'uses' => '\App\Http\Controllers\Backend\UserController@index',
        
    ]);
    Route::get('user/detail/{id}', [
        'as' => 'user.detail', 
        'uses' => '\App\Http\Controllers\Backend\UserController@detail',

    ]);
    Route::any('user/add', [
        'as' => 'user.add', 
        'uses' => '\App\Http\Controllers\Backend\UserController@add',

    ]);
    Route::any('user/update/{id}', [
        'as' => 'user.update', 
        'uses' => '\App\Http\Controllers\Backend\UserController@update',

    ]);
    Route::get('user/delete/{id}', [
        'as' => 'user.delete', 
        'uses' => '\App\Http\Controllers\Backend\UserController@delete',

    ]);

    Route::any('user/searchingbytypeahead', [
        'as' => 'user.ajax.searching', 
        'uses' => '\App\Http\Controllers\Backend\UserController@searchingByAjax',

    ]);

    Route::any('user/resetpassword/{id}', [
        'as' => 'user.resetpassword', 
        'uses' => '\App\Http\Controllers\Backend\UserController@resetPassword',

    ]);



    Route::any('role/', [
        'as' => 'role.index', 
        'uses' => '\App\Http\Controllers\Backend\RoleController@index',
        
    ]);
    Route::get('role/detail/{id}', [
        'as' => 'role.detail', 
        'uses' => '\App\Http\Controllers\Backend\RoleController@detail',

    ]);
    Route::any('role/add', [
        'as' => 'role.add', 
        'uses' => '\App\Http\Controllers\Backend\RoleController@add',

    ]);
    Route::any('role/update/{id}', [
        'as' => 'role.update', 
        'uses' => '\App\Http\Controllers\Backend\RoleController@update',

    ]);
    Route::get('role/delete/{id}', [
        'as' => 'role.delete', 
        'uses' => '\App\Http\Controllers\Backend\RoleController@delete',

    ]);

    

    Route::any('permission/', [
        'as' => 'permission.index', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@index',
        
    ]);
    Route::get('permission/detail/{id}', [
        'as' => 'permission.detail', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@detail',

    ]);
    Route::any('permission/add', [
        'as' => 'permission.add', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@add',

    ]);
    Route::any('permission/update/{id}', [
        'as' => 'permission.update', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@update',

    ]);
    Route::get('permission/delete/{id}', [
        'as' => 'permission.delete', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@delete',

    ]);
    Route::any('role/{roleId}/permission', [
        'as' => 'permission.set', 
        'uses' => '\App\Http\Controllers\Backend\PermissionController@setPermissionForRole',

    ]);


    //Post
    Route::any('properties/', [
        'as' => 'admin.post.index', 
        'uses' => '\App\Http\Controllers\Backend\PostController@index',

    ]);
    Route::any('properties/pending', [
        'as' => 'admin.post.pending', 
        'uses' => '\App\Http\Controllers\Backend\PostController@pending',

    ]);
    Route::any('property/detail/{id}', [
        'as' => 'admin.post.detail', 
        'uses' => '\App\Http\Controllers\Backend\PostController@detail',

    ]);
    Route::any('property/update/{id}', [
        'as' => 'admin.post.edit', 
        'uses' => '\App\Http\Controllers\Backend\PostController@edit',

    ]);
    Route::any('api/deletepost/{id}', [
        'as' => 'admin.api.post.delete', 
        'uses' => '\App\Http\Controllers\Backend\PostController@apiDeletePost',
    ]);
    Route::any('property/status/{id}', [
        'as' => 'admin.post.status', 
        'uses' => '\App\Http\Controllers\Backend\PostController@changeStatus',
    ]);
    Route::any('property/media/{id}', [
        'as' => 'admin.post.media', 
        'uses' => '\App\Http\Controllers\Backend\PostController@media',
    ]);
    Route::any('api/uploadmedia/{id}', [
        'as' => 'admin.api.post.uploadmedia', 
        'uses' => '\App\Http\Controllers\Backend\PostController@apiUploadMedia',
    ]);
    Route::any('api/deletemedia/', [
        'as' => 'admin.api.post.deletemedia', 
        'uses' => '\App\Http\Controllers\Backend\PostController@apiDeleteMedia',
    ]);
    Route::any('api/featuredimage/', [
        'as' => 'admin.api.post.featuredimage', 
        'uses' => '\App\Http\Controllers\Backend\PostController@apiSetFeaturedImage',
    ]);
    

});