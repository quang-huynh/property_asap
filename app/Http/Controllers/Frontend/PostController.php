<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Http\Request;
use Auth;
use App\Helpers\Slugify;

use App\Models\Repositories\PostRepository;
use App\Models\Repositories\LocationRepository;

use App\Http\Services\Frontend\PostService;
use App\Http\Services\Frontend\TagService;
use App\Http\Services\Frontend\LocationService;
use App\Http\Requests\Frontend\PostFormAjaxRequest;

use Session;

use ChannelLog as Log;

class PostController extends BaseController {

	protected $postRepository;
	protected $LocationRepository;
	protected $postService;
	protected $locationService;
	protected $tagService;

	public function __construct(PostRepository $repo, LocationRepository $locationRepository, PostService $service, LocationService $locationService, TagService $tagService) 
	{
		$this->postRepository = $repo;
		$this->LocationRepository = $locationRepository;
		$this->postService = $service;
		$this->locationService = $locationService;
		$this->tagService = $tagService;
	}

	/**
	 * Create Post
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('frontend.post.create');
	}


	/**
	 * Search page
	 * @param  Request $request
	 * @return 
	 */
	public function search(Request $request)
	{
		$tags = $this->tagService->getPopularTags(15);
		$tagsSelected = [];

		if (!empty($request->all()) && $request->isMethod('get')) {
			$input = $request->all();

			if (!empty($input['tags'])) {
				$tagsSelected = explode(',', $input['tags']);
			}

			$posts = $this->postService->search($input);

		} else {
			$posts = $this->postService->search();
		}

		$postsTags = [];
		$arrTagsOfPost = [];
		foreach ($posts as $key => $post) {
			$tags_posts = $post->tags()->getResults();
			$arrTagsOfPost = [];
			foreach ($tags as $key => $tag) {
				$arrTagsOfPost[$tag['attributes']['slug']] = $tag;
			}
			$postsTags[] = $arrTagsOfPost;
		}

		return view('frontend.post.search')->with('postService', $this->postService)->with('posts', $posts)->with('postsTags', $postsTags)->with('tagsSelected', $tagsSelected)->with('request', $request->all())->with('tags', $tags);
	}


	/**
	 * Edit Post
	 *
	 * @return Response
	 */
	public function edit($slug)
	{
		$post = $this->postRepository->getByAttribute('slug', $slug);
		if (empty($post)) {
			abort(404);
		}
		if (!$this->postService->checkOwnerBySlug($slug)) {
			abort(503);
		}


		$tags = $post->tags()->getResults();
		$arrTagsOfPost = [];
		foreach ($tags as $key => $tag) {
				$arrTagsOfPost[] = $tag->slug;
		}


		return view('frontend.post.edit')->with('post', $post)->with('tags', $arrTagsOfPost);
	}



	/**
	 * Close Post
	 * @param  string $slug slug of post
	 * @return 
	 */
	public function close($slug)
	{
		$post = $this->postRepository->getByAttribute('slug', $slug);
		if (empty($post)) {
			abort(404);
		}
		if (!$this->postService->checkOwnerBySlug($slug)) {
			abort(503);
		}

		$result = $this->postService->closePostById($post->id);

		if ($result) {
			Log::write('user', 'Closed Post ID['.$post->id.']', ['user' => Auth::user(), 'post' => $post, 'result' => $result]);
			return redirect()->route('post.detail', ['slug' => $slug]);
		} else {
			abort(503);
		}

	}


	/**
	 * Delete Post
	 * @param  string $slug slug of post
	 * @return 
	 */
	public function delete($id)
	{
		if (!empty($id)) {
			if (!$this->postService->checkOwnerById($id)) {
				abort(503);
			}
			$result = $this->postService->delete($id);
			if ($result) {
				return route('frontend.index');
			} else {
				abort(503);
			}
		}
		abort(404);
	}


	/**
	 * 
	 * @param  string $slug 
	 * @return      
	 */
	public function detail(Request $request, $slug)
	{
		$post = $this->postService->getPostBySlug($slug);
		if (!empty($post)) {
			$featurePosts = $this->postService->getFeaturePosts(3);
			$nearlyPosts = $this->postService->getNearlyPosts($post);

			//save viewed post and display.
			$viewedPosts = [];
			if (!empty(session('loggedUser'))) {
				$loggedUser = session('loggedUser');
				if (!empty($loggedUser['viewedPosts'])) {
					$arrViewedPost = $loggedUser['viewedPosts'];
				    if (!in_array($post->id, $arrViewedPost)) {
				    	$arrViewedPost[$post->id] = $post->id;
			    		$loggedUser['viewedPosts'] = $arrViewedPost;
						$request->session()->forget('loggedUser');
						$request->session()->put('loggedUser', $loggedUser );
						$this->postService->refreshViewers($post);
				    }
				} else {
					$arrViewedPost = [$post->id => $post->id];
					$loggedUser['viewedPosts'] = $arrViewedPost;
					$request->session()->forget('loggedUser');
					$request->session()->put('loggedUser', $loggedUser );
					$this->postService->refreshViewers($post);
				}

				unset($arrViewedPost[$post->id]); //remove current post into recently viewed
				$viewedPosts = $this->postService->getRecentlyViewedPosts($arrViewedPost);

			}

			$tags = $post->tags()->getResults();
			$arrTagsOfPost = [];
			foreach ($tags as $key => $tag) {
				$arrTagsOfPost[$tag['attributes']['slug']] = $tag;
			}


			return view('frontend.post.detail')->with('postService', $this->postService)->with('featurePosts', $featurePosts)->with('nearlyPosts', $nearlyPosts)->with('viewedPosts', $viewedPosts)->with('post', $post)->with('tags', $arrTagsOfPost);
		} else {
			abort(404);
		}
	}


	/**
	 * 
	 * @param  Request $request 
	 * @return 
	 */
	public function apiCreatePost(Request $request)
	{
		$input = $request->all();
		//dd($input);
		$post = $this->postService->save($input);
		if (!empty($post)) {
			$refreshPost = $this->postRepository->getById($post->id);

			return json_encode(array('code' => 1, 'mess' => 'Create pending post success', 'url_preview_post' => route('post.detail', ['slug' => $refreshPost->slug])));
		}

		return json_encode(array('code' => 0, 'mess' => 'Create pending post fail'));
	}


	/**
	 * 
	 * @param  Request $request
	 * @return
	 */
	public function apiUpdatePost(Request $request)
	{
		if (!$this->postService->checkOwnerById($request['id'])) {
			abort(503);
		}
		$input = $request->all();
		if (!empty($input['id'])) {
			if (!$this->postService->checkOwnerById($input['id'])) {
				abort(503);
			}
			$post = $this->postService->update($input);
			if (!empty($post)) {
				$refreshPost = $this->postRepository->getById($input['id']);

				return json_encode(array('code' => 1, 'mess' => 'Update post success', 'url_preview_post' => route('post.detail', ['slug' => $refreshPost->slug])));
			}
		}
			

		return json_encode(array('code' => 0, 'mess' => 'Update post fail'));
	}


	/**
	 * 
	 * @return
	 */
	public function apiSearch(Request $request)
	{

		$posts = '';
		if ($request->isMethod('post')) {

			$posts = $this->postService->search($request->all());
			if (count($posts) == 0) {
				return json_encode(array('code' => 'end', 'mess' => 'End'));
			}

			$html = '';

			foreach ($posts as $key => $post) {
				$html .= view('frontend.post.partials.post_item_search')->with('postService', $this->postService)->with('post', $post);
			}
		}
			
		return json_encode(array('code' => 1, 'mess' => 'Search success', 'html' => $html, 'count' => count($posts)));
	}


	public function pinFeatureOnSearchPage($id)
	{
		return $this->postService->pinFeatureOnSearchPage($id);
	}

}
