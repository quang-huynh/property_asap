<?php
/*
 * @author vungpv;
 * @datetime : 2017-06-18 07:00 pm
 * @filename : AuthenticateController
 * @description : login
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ChangePasswordRequest;
use App\Http\Requests\Frontend\EditMeRequest;
use App\Models\Entities\Base;
use App\Models\Entities\User;
use App\Models\Repositories\UserRepository;
use App\Http\Services\Frontend\PostService;
use App\Models\Repositories\PostRepository;
use App\Http\Services\Frontend\WishlistService;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Route;
use Socialite;
use Validator;


use ChannelLog as Log;

class ProfileController extends Controller
{

    protected $viewProfile;
    protected $routeName;
    protected $user_id;
    protected $userRepository;
    protected $wishlistService;
    protected $postService;
    protected $postRepository;

    public function __construct(PostService $postService, WishlistService $wishlistService, PostRepository $postRepository)
    {
        $this->viewProfile = "frontend.profile.profile";
        $this->routeName = !empty(Route::current()->getName()) ? Route::current()->getName() : "";
        $this->user_id = !empty(Auth::guard()->check()) ? Auth::guard()->user()->id : 0;
        $this->userRepository = new UserRepository(new User());
        $this->wishlistService = $wishlistService;
        $this->postService = $postService;
        $this->postRepository = $postRepository;
    }

    /*
     * |---------------------------------------------------------------------
     * | SHOW PROFILES ME
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */
    public function me(Request $request)
    {
        $me = User::where('id', $this->user_id)->first();
        if (!empty($me)) {
            return view('frontend.profile.me', ['routeName' => $this->routeName, 'me' => $me]);
        }
        return abort(404);
    }


    /*
     * |---------------------------------------------------------------------
     * | SHOW FORM EDIT PROFILE
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */
    public function edit(Request $request)
    {
        $genders = User::$genders;
        $me = $this->userRepository->getById($this->user_id);
        if (!empty($me)) {
            return view('frontend.profile.edit', ['routeName' => $this->routeName, 'me' => $me, 'genders' => $genders]);
        }
        return abort(404);
    }


    /*
     * |---------------------------------------------------------------------
     * | EDIT PROFILE
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */

    public function postEdit(EditMeRequest $request)
    {
        $parmas = $request->only('fullname', 'address', 'birthday', 'phone_number', 'gender');

        $me = User::where('id', $this->user_id)->first();
        if (!empty($me)) {
            $me->fullname = $parmas['fullname'];
            $me->address = $parmas['address'];

            $birthday = strtotime( $parmas['birthday'] );
            $birhtdayUpdate = date( 'Y-m-d', $birthday );

            $me->birthday = $birhtdayUpdate;
            $me->phone_number = $parmas['phone_number'];
            $me->gender = $parmas['gender'];
            if ($me->save()) {
                Log::write('user', 'User ID['.$id.'] updated profile', ['user' => Auth::user()]);
                return redirect()
                  ->route('frontend.profile.me')
                  ->with('message', ['type' => 'success', 'content' => 'Editting success']);
            }
            return redirect()
              ->back()
              ->with('message', ['type' => 'warning', 'content' => 'Edit errors !'])
              ->withInput($parmas);
        }
        return abort(404);
    }


    /*
     * |---------------------------------------------------------------------
     * | SHOW FORM CHANGE PASSWORD
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */
    public function changePassword(Request $request)
    {
        return view('frontend.profile.change_password', ['routeName' => $this->routeName]);
    }

    /*
     * |---------------------------------------------------------------------
     * | POST FORM CHANGE PASSWORD
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */
    public function postChangePassword(ChangePasswordRequest $request)
    {
        $me = User::where('id', $this->user_id)->first();
        $params = $request->only('current_password', 'password', 'password_confirmation');
        if (!empty($me)) {
            $me->password = Hash::make($request->password);
            if ($me->save()) {
                Log::write('user', 'User ID['.$id.'] changed password', ['user' => Auth::user()]);
                return redirect()
                  ->route('frontend.profile.me')
                  ->with('message', ['type' => 'success', 'content' => 'Editting success']);
            }
            return redirect()
              ->back()
              ->with('message', ['type' => 'warning', 'content' => 'Edit errors !']);
        }
        return abort(404);
    }

    /*
     * |---------------------------------------------------------------------
     * | POST UPLOAD PICTURE
     * |---------------------------------------------------------------------
     * | @author 
     * |---------------------------------------------------------------------
     */
    public function uploadPicture(Request $request)
    {
        $file = $request->file('picture');
        if ($file) {
            $filename = md5($this->user_id) . time() . '.png';
            $user = User::find($this->user_id);
            $user->picture = $filename;
            $user->save();
            $file->move(config('asap.upload_profile_picture_path'), $filename);
            Log::write('user', 'User ID['.$id.'] updated profile picture. File name: '.$filename, ['user' => Auth::user()]);
            return redirect()->route('frontend.profile.me');
        }
        return redirect()->route('frontend.profile.me');
    }



    public function wishlist()
    {
        $wishlists = $this->wishlistService->getWishlistByUser();
        // dd($wishlists);
        $posts = [];
        foreach ($wishlists as $key => $wl) {
            $posts[] = $this->postRepository->getById($wl->post_id);
        }
        return view('frontend.profile.wishlist', ['routeName' => $this->routeName, 'posts' => $posts, 'postService' => $this->postService]);
    }


    public function properties()
    {
        $properties = $this->postService->getMyProperties();
        $pendingProperties = $this->postService->getMyPendingProperties();

        return view('frontend.profile.properties', ['routeName' => $this->routeName, 'properties' => $properties, 'pendingProperties' => $pendingProperties, 'postService' => $this->postService]);
    }
}
