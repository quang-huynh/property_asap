<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseController;

use App\Http\Services\Frontend\TagService;

use ChannelLog as Log;

class HomeController extends BaseController {

	protected $tagService;

	public function __construct(TagService $tagService) 
	{
		$this->tagService = $tagService;

	}


	
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('frontend.home.index');
	}

	public function about()
	{
		return view('frontend.about');
	}

	public function terms()
	{
		return view('frontend.terms');
	}

	public function privacy()
	{
		return view('frontend.privacy');
	}

	public function contact()
	{
		return view('frontend.contact');
	}

}
