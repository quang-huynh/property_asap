<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Services\Frontend\SocialService;
use App\Models\Entities\Base;
use App\Models\Entities\PasswordReset;
use App\Models\Entities\User;
use App\Models\Repositories\UserRepository;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Socialite;
use Validator;

use Auth;

use Session;

use App\Http\Requests\Frontend\RegisterRequest;

use ChannelLog as Log;

class AuthenticateController extends Controller
{

    public $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository(new User());
    }


    public function login(Request $request)
    {
        if (!empty(Auth::user())) {
            return redirect('/');
        }

        if (!empty($request)) {
            $params = $request->only('email', 'password');
            $validator = Validator::make(
              $params,
              [
                'email' => 'required|email',
                'password' => 'required',
              ]
            );

            if ($validator->fails()) {
                $code = 1;
            }
            $credentials = $params;
            if (!auth()->attempt($credentials, true)) {
                $code = 1;
            } else {
                $request->session()->put('loggedUser', ['user' => auth()->user()] );

                return back();
            }
        }
            
        
        return view('frontend.auth.login')->with('code', $code);
    }


    public function Logout(Request $request)
    {
        auth()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    /*
     * |--------------------------------------------------------------
     * | POST LOGIN
     * |--------------------------------------------------------------
     * | @author vungpv
     * | @method : post + ajax
     * |--------------------------------------------------------------
     */
    public function ajaxLogin(Request $request)
    {

        $params = $request->only('email', 'password');
        $validator = Validator::make(
          $params,
          [
            'email' => 'required|email',
            'password' => 'required',
          ]
        );
        // dd($params);
        if ($validator->fails()) {
            return response()->json(['status' => 199, 'msg' => 'Login faild ', 'error' => $validator->messages()]);
        }
        $credentials = $params;
        $credentials['status'] = 1;
        if (!auth()->attempt($credentials, true)) {
            return response()->json(['status' => 199, 'msg' => 'Login faild ', 'error' => ['User not found !']]);
        }
        return response()->json(['status' => 200, 'msg' => 'Login success!', 'data' => auth()->user()]);
    }

    /*
     * |--------------------------------------------------------------
     * | POST LOGIN
     * |--------------------------------------------------------------
     * | @author vungpv
     * | @method : post + ajax
     * |--------------------------------------------------------------
     */
    public function postLogout(Request $request)
    {
        auth()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }


    /**
     * Email Confirm Proccessing
     *
     * @param $userId
     * @param $activationCode
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmEmail($email, $activationCode)
    {
        $user = $this->userRepository->getUserMailConfirm($email, $activationCode);
        if ($user) {
            $user->activation_code = null;
            $user->status = 1;
            $user->save();
            flash()->overlay('Your account has been confirmed! Please Login to use.', 'Confirm email successfull');
            return redirect()->to('/');
        }
        flash()->overlay('Confirm email failed, Please check email to confirm again !', 'Confirm email failed');
        return redirect()->to('/');
    }


    /*
     * |----------------------------------------------------------------
     * | Password Reset !
     * |----------------------------------------------------------------
     * | @author 
     * |----------------------------------------------------------------
     */

    public function getPasswordReset()
    {
        return view('frontend.password.reset');
    }

    /*
     * |----------------------------------------------------------------
     * | Password Reset !
     * |----------------------------------------------------------------
     * | @author 
     * |----------------------------------------------------------------
     */

    public function postPasswordReset(Requests\Frontend\ResetPasswordRequest $request)
    {
        $email = $request->email;
        $token = str_random(32);
        $passwordReset = new PasswordReset();
        $passwordReset->email = $request->email;
        $passwordReset->token = $token;
        if ($passwordReset->save()) {
            try {
                Mail::send('emails.pasword_reset', [
                  'email' => $email,
                  'token' => $token
                ], function ($m) use ($email, $token) {
                    $m->to($email, $email)->sender(config('mail.username'))->subject('Password Reset.');
                });
                flash()->overlay('Please check email get token !', 'Password Reset Success');
                return redirect()->route('frontend.auth.password.reset');
            } catch (Exception $e) {
                return abort(500);
            }
                
            
        }
        flash()->overlay('Password reset faild, Please again !', 'Password Reset Faild');
        return redirect()->to('/');
    }


    public function getPasswordNew(Request $request, $email, $token)
    {
        $passwordReset = PasswordReset::getLastPasswordReset($email, $token);
        if ($passwordReset) {
            return view('frontend.password.new');
        }
        return abort(404);
    }

    public function postPasswordNew(Requests\Frontend\SetNewPasswordRequest $request, $email, $token)
    {
        if ($email && $token) {
            $passwordReset = PasswordReset::getLastPasswordReset($email, $token);
            if ($passwordReset) {
                $user = User::where('email', $email)->first();
                if ($user) {
                    $user->password = Hash::make($request->password);
                    $user->save();
                    flash()->overlay('Password has changed', 'Password has changed !');
                    return redirect('/');
                }
            }
        }
        return redirect()->back()->with('message', ['type' => 'single', 'content' => 'Please again !']);
    }

    /*
     * |----------------------------------------------------------------
     * | Authenticate Facebook && Google;
     * |----------------------------------------------------------------
     * | @author 
     * |----------------------------------------------------------------
     */
    public function authFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function authFacebookSocial()
    {
        $userFB = Socialite::driver('facebook')->user();
        $fbid = !empty($userFB->id) ? $userFB->id : null;
        $email = !empty($userFB->email) ? $userFB->email : null;
        $token = !empty($userFB->token) ? $userFB->token : null;
        // 2 check social
        $userDB = SocialService::getUserSocial('facebook', $fbid);
        if (!empty($userDB)) {
            $userDB->facebook_token = $token;
            $userDB->facebook_json = json_encode($userFB);
            $userDB->save();
            SocialService::setLogin($userDB);
            return redirect()->route('frontend.profile.edit');
        } else {
            $userDB = SocialService::getUserByEmail($email);
            // Register;
            if (empty($userDB)) {
                $userDB = new User();
                $userDB->email = $userFB->email;
                $userDB->is_member = 1;
                $userDB->status = config('asap.user_status')['active'];
            }
            $userDB->facebook_id = $fbid;
            $userDB->facebook_token = $token;
            $userDB->facebook_json = json_encode($userFB);
            $userDB->save();
            SocialService::setLogin($userDB);
            return redirect()->route('frontend.profile.edit');
        }
        return abort(404);
    }


    public function authGoogle()
    {
        return Socialite::driver('google')->redirect();
    }


    public function authGoogleSocial()
    {
        $userGG = Socialite::driver('google')->user();
        $ggid = !empty($userGG->id) ? $userGG->id : null;
        $email = !empty($userGG->email) ? $userGG->email : null;
        $token = !empty($userGG->token) ? $userGG->token : null;
        // 2 check social
        $userDB = SocialService::getUserSocial('google', $ggid);
        if (!empty($user)) {
            $userDB->facebook_token = $token;
            $userDB->facebook_json = json_encode($userGG);
            $userDB->save();
            SocialService::setLogin($userDB);
            return redirect()->route('frontend.profile.edit');
        } else {
            $userDB = SocialService::getUserByEmail($email);
            // Register;
            if (empty($userDB)) {
                $userDB = new User();
                $userDB->email = $userGG->email;
                $userDB->is_member = 1;
                $userDB->status = 1;
            }
            $userDB->google_id = $ggid;
            $userDB->google_token = $token;
            $userDB->google_json = json_encode($userGG);
            $userDB->save();
            SocialService::setLogin($userDB);
            return redirect()->route('frontend.profile.edit');
        }
        return abort(404);
    }
}
