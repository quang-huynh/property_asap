<?php namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Frontend\BaseController;

use App\Http\Requests\Frontend\RegisterRequest;
use App\Models\Entities\Base;
use App\Models\Entities\User;
use App\Models\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

use Hash;

use Auth;

use ChannelLog as Log;

class UserController extends BaseController {
	public $userRepository;

	public function __construct() {
		$this->userRepository = new UserRepository( new User() );
	}


	/**
	 * Register User
	 *
	 * @return Response
	 */
	public function getRegister() {
		return view( 'frontend.user.register' );
	}

	/**Store Register Information
	 *
	 * @param Request $request
	 */
	public function postRegister( Request $request ) {
		$registerRules = [
			'email'        => 'required|unique:user,email|max:255',
			'password'     => 'required|confirmed|max:60',
			'phone_number' => 'required|max:255',
			'address'	   => 'required|max:255',
			'fullname'     => 'required|max:255',
		];

		$validator = validator( $request->all(), $registerRules );

		if ( $validator->fails() ) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		//Create new User
		$user = $this->userRepository->add( $request->all() );


		// Send Confirm email if  config On
		if ( config( 'asap.email_confirm' ) ) {
			$activationCode        = str_random( 12 );
			$user->activation_code = $activationCode;
			$user->status          = config('asap.user_status')['inactive'];
			$user->save();
			try {
				Mail::send( 'emails.user_confirm', [
					'user'           => $user,
					'activationCode' => $activationCode
				], function ( $m ) use ( $user ) {
					$m->to( $user->email, $user->name )->subject( 'Email confirm.' );
				} );
				flash()->overlay( 'We has been to you a Email to confirm. Please check it!', 'Email Confirm' );
			} catch (Exception $e) {
				return abort(500);
			}
				
		} else {
			$user->status = config('asap.user_status')['active'];
			$user->save();
			if (config( 'asap.email_confirm' )) {
				try {
					Mail::send( 'emails.thank_for_register', [
						'user' => $user,
					], function ( $m ) use ( $user ) {
						$m->to( $user->email, $user->name )->subject( 'Thank you.' );
					} );
				} catch (Exception $e) {
					return abort(500);
				}
			}
			flash()->overlay( 'Your account has been created! Please Login to use.', 'Register Successfull' );

		}

		return redirect()->to( '/' );

	}

	/**
	 * Email Confirm Proccessing
	 *
	 * @param $userId
	 * @param $activationCode
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function confirmEmail( $userId, $activationCode ) {
		$user = $this->userRepository->getById( $userId );
		if ( $user ) {
			if ( $user->activation_code == $activationCode ) {
				$user->status       = 1;
				$user->activated_at = Carbon::now();
				$user->save();
				flash()->overlay( 'Your account has been confirmed! Please Login to use.', 'Confirm email successfull' );

				return redirect()->to( '/' );
			} else {
				flash()->overlay( 'Confirm email failed, Please check email to confirm again !', 'Confirm email failed' );

				return redirect()->to( '/' );
			}
		} else {
			flash()->overlay( 'User not found', 'Confirm email failed, Please check email to confirm again!' );
		}

	}

}
