<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseController;

use App\Http\Services\Frontend\WishlistService;
use App\Models\Repositories\WishlistRepository;
use App\Http\Services\Frontend\PostService;
use App\Models\Repositories\PostRepository;

use Illuminate\Http\Request;

use ChannelLog as Log;

class WishListController extends BaseController {

	protected $wishlistService;
	protected $wishlistRepository;
	protected $postService;
	protected $postRepository;

	public function __construct(WishlistService $wishlistService, WishlistRepository $wishlistRepository, PostService $postService, PostRepository $postRepository) 
	{
		$this->wishlistService = $wishlistService;
		$this->wishlistRepository = $wishlistRepository;
		$this->postService = $postService;
		$this->postRepository = $postRepository;
	}


	
	public function apiAdd(Request $request)
	{
		$input = $request->all();
		if (!empty($input)) {
			if (!$this->wishlistService->hasWishList($input['id'])) {
				$wl = $this->wishlistService->save($input['id']);
				return json_encode(array('code' => 1, 'mess' => 'Add wishlist success'));
			} else {
				return json_encode(array('code' => 1, 'mess' => 'Already added wishlist'));
			}
		}	

		return json_encode(array('code' => 0, 'mess' => 'Add to wishlist fail'));
	}



	public function apiRemove(Request $request)
	{
		$input = $request->all();
		if (!empty($input)) {
			if ($this->wishlistService->hasWishList($input['id'])) {
				$wl = $this->wishlistService->remove($input['id']);
				return json_encode(array('code' => 1, 'mess' => 'Remove wishlist success'));
			} else {
				return json_encode(array('code' => 1, 'mess' => 'Already removed wishlist'));
			}
		}

		return json_encode(array('code' => 0, 'mess' => 'Remove wishlist fail'));
	}


}
