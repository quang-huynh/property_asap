<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Entities\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Hash;

use ChannelLog as Log;

class SocialAuthController extends Controller
{
    public function index()
    {
        return view('social.index');
    }

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        //dd(Socialite::driver('facebook')->user());
    }
}
