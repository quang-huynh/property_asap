<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseController;

use ChannelLog as Log;


class SearchController extends BaseController {


	public function __construct() 
	{
		
	}

	/**
	 * Show the result
	 *
	 * @return Response
	 */
	public function result()
	{
		return view('frontend.post.list');
	}


	/**
	 * Show the search of tag page
	 * @return [type] [description]
	 */
	public function searchTag($slug)
	{
		return view('frontend.post.list');
	}

}
