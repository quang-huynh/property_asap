<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class BaseController extends Controller {

    protected function buildBreadcrumb($arr) {
        \View::share('breadcrumb', $arr);
    }

}
