<?php namespace App\Http\Controllers\Backend;

use Auth;

use App\Http\Services\Backend\NotificationService;

use ChannelLog as Log;

class NotificationController extends BaseController {

    protected $notificationService;
    
    public function __construct(NotificationService $notificationService)
    {   
        $this->notificationService = $notificationService;
    }

    public function readed($id = null)
    {
        if ($id) {
            $result = $this->notificationService->readed($id);
        }
    }

}
