<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Services\Backend\PostService;
use App\Http\Services\Backend\MediaService;
use App\Models\Repositories\PostRepository;
use App\Models\Repositories\MediaRepository;

use ChannelLog as Log;


class PostController extends BaseController {


	protected $postService;
	protected $mediaService;
	protected $postRepository;
	protected $mediaRepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PostService $postService, MediaService $mediaService, PostRepository $postRepository, MediaRepository $mediaRepository)
	{
		$this->middleware('auth');

		$this->postService = $postService;
		$this->mediaService = $mediaService;
		$this->postRepository = $postRepository;
		$this->mediaRepository = $mediaRepository;
	}


	/**
	 * Show the application dashboard to the admin.post.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.index'))
        ));

        if ($request->isMethod('post')) {

            $posts = $this->postService->search($request->all());

            return view('backend.post.index')->with('request', $request->all())->with('posts', $posts)->with('search', 'true');
        } else {
            $posts = $this->postRepository->getPaginate(100);

			return view('backend.post.index')->with('posts', $posts)->with('search', 'false');
        }

        
	}


	/**
	 * Show the posts pending index.
	 *
	 * @return Response
	 */
	public function pending()
	{
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.pending'))
        ));

        $posts = $this->postRepository->getPendingPaginate(100);

		return view('backend.post.pending')->with('posts', $posts);
	}


	public function detail($id)
	{
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.index'))
        ));

		if (!$id) {
			return route('admin.post.index');
		}

		$post = $this->postRepository->getById($id);
        if (empty($post)) {
        	return route('admin.post.index');
        }

		$tags = $post->tags()->getResults();
		$arrTagsOfPost = [];
		foreach ($tags as $key => $tag) {
				$arrTagsOfPost[] = $tag->slug;
		}

        return view('backend.post.detail')->with('post', $post)->with('tags', $arrTagsOfPost);
	}


	public function edit(Request $request, $id)
	{
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.index'), 'edit' => \URL::route('admin.post.edit', ['id' => $id]))
        ));


        if (!$id) {
			return route('admin.post.index');
		}

		$post = $this->postRepository->getById($id);
        if (empty($post)) {
        	return route('admin.post.index');
        }

		$tags = $post->tags()->getResults();
		$arrTagsOfPost = [];
		foreach ($tags as $key => $tag) {
				$arrTagsOfPost[] = $tag->slug;
		}


		$input = $request->all();

		if (!empty($input['id'])) {
			if (!$this->postService->checkOwnerById($input['id'])) {
				abort(503);
			}
			$result = $this->postService->update($input);

			$post = $this->postRepository->getById($id);
			//return result
		}

        return view('backend.post.edit')->with('post', $post)->with('tags', $arrTagsOfPost);
	}


	public function media(Request $request, $id)
	{
		if(empty($id)) {
			return redirect('admin/');
		}
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.index'), 'media' => \URL::route('admin.post.media', ['id' => $id]))
        ));

		$post = $this->postRepository->getById($id);

		$media = $this->mediaService->getByPostId($id);

       	return view('backend.post.media')->with('post', $post)->with('media', $media);

	}


	public function apiUploadMedia(Request $request, $postId)
	{
		$post = $this->postRepository->getById($postId);
		$media = $post->media()->getResults();
		if (count($media) < config('asap.max_images_of_package')[$post->package]) {
			$result = $this->mediaService->addImage($request->all(), $postId);
			if ($result) {
				$path = config('asap.upload_post_path');
				$request->file('image')->move($path, $result->name);
				return json_encode(array('code' => 1, 'mess' => 'Add images success!', 'path' => asset(config('asap.upload_post_url') . $result->name), 'id' => $result->id));
			} else {
				return json_encode(array('code' => 0, 'mess' => 'An error occurred during add image. Please refresh and try again'));
			}
		} else {
			return json_encode(array('code' => 0, 'mess' => 'The post (ID:' . $post->id . ') has full images (' . config('asap.max_images_of_package')[$post->package] . ' images).'));
		}
	}


	public function apiDeleteMedia(Request $request)
	{
		$postId = $request->all()['id'];
		if ($postId) {
			$media = $this->mediaRepository->getById($postId);
			$result = $this->mediaRepository->delete($media);
			if ($result) {
				unlink(config('asap.upload_post_path') . $media->name);
				return json_encode(array('code' => 1, 'mess' => 'Delete image success!'));
			} else {
				return json_encode(array('code' => 0, 'mess' => 'An error occurred during delete image. Please refresh and try again'));
			}
		}
			
	}


	public function apiDeletePost($id)
	{
		if ($id) {
			$post = $this->postRepository->getById($id);
			$post->tags()->detach();
			$result = $this->postRepository->delete($post);
			if ($result) {
				$deleteMedia = $this->mediaService->deleteByPostId($id);
				return json_encode(array('code' => 1, 'mess' => 'Delete post success!'));
			} else {
				return json_encode(array('code' => 0, 'mess' => 'An error occurred during delete post. Please refresh and try again'));
			}
		}
	}


	public function apiSetFeaturedImage(Request $request)
	{
		$input = $request->all();
		if (!empty($input['postId']) && !empty($input['id']) ) {
			$media = $this->mediaService->update($input['postId'],$input['id']);
			
			if ($media) {
				return json_encode(array('code' => 1, 'mess' => 'Set featured image success!'));
			} else {
				return json_encode(array('code' => 0, 'mess' => 'An error occurred during set featured image. Please refresh and try again'));
			}
		}
	}


	public function changeStatus(Request $request, $id)
	{
		if(empty($id)) {
			return redirect('admin/');
		}
		$this->buildBreadcrumb(array(
            array('name' => 'Post', 'url' => \URL::route('admin.post.index'), 'status' => \URL::route('admin.post.status', ['id' => $id]))
        ));

        $post = $this->postRepository->getById($id);

        if (!$id || !$post) {
            abort(404);
        }

        if ($request->isMethod('post')) {
        	$input = $request->all();

        	if (!empty($input['published_btn'])) {
        		$input['status'] = config('asap.post_status')['published'];
        	}

        	$result = $this->postService->updateStatus($input);  

        	$post = $this->postRepository->getById($id);    
        }   

        return view('backend.post.status')->with('post', $post);
	}

}
