<?php namespace App\Http\Controllers\Backend;


use Request;

use App\Http\Services\Backend\RoleService;
use App\Http\Services\Backend\PermissionService;
use App\Models\Entities\Role;
use App\Models\Repositories\RoleRepository;
use App\Models\Entities\Permission;
use App\Models\Repositories\PermissionRepository;
use App\Http\Requests\Backend\AddPermissionRequest;
use App\Http\Requests\Backend\UpdatePermissionRequest;
use App\Http\Requests\Backend\SetPermissionOfRoleRequest;

use Auth;
use ChannelLog as Log;


class PermissionController extends BaseController {

    protected $permissionRepository;

    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoleRepository $roleRepository ,PermissionRepository $permissionRepository)
    {
        //$this->middleware('guest');
        $this->permissionRepository = $permissionRepository;
        $this->roleRepository = $roleRepository;
        
    }


    /**
     * Update Permission for role
     * @param interger                   $roleId  Role Selected
     * @param SetPermissionOfRoleRequest $request List Permission
     */
    public function setPermissionForRole($roleId,  SetPermissionOfRoleRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Permission', 'url' => \URL::route('permission.index')),
        ));

        $role = $this->roleRepository->getById($roleId);

        if (!$roleId || !$role) {
            abort(404);
        }

        if ($request->isMethod('post'))
        {

            $permissionsOfRole = $role->permissions()->getResults();

            $result = RoleService::updatePermissionOfSelectedRole($role, $permissionsOfRole, $request);
            Log::write('admin_access', 'Access Permission Management. Set permission for role', ['admin' => Auth::user()]);
            return redirect()->route('role.index');
            
        } else {
            $permissionsOfRole = $role->permissions()->getResults();
            $permissions = $this->permissionRepository->getAll();

            $arrPermissionOfRole = RoleService::convertCollectionToArray($permissionsOfRole);

            return view('backend.permission.set_permission')->with('role', $role)->with('arrPermissionOfRole',$arrPermissionOfRole)->with('permissions', $permissions);
        }
    }


    /**
     * Show the application dashboard to the permission.
     *
     * @return 
     */
    public function index()
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Permission', 'url' => \URL::route('permission.index')),
        ));

        $permissions = $this->permissionRepository->getPaginated(10);
        return view('backend.permission.index')->with('permissions',$permissions);
    }

    /**
     * Detail Permission
     * @return 
     */
    public function detail($id)
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Permission', 'url' => \URL::route('permission.index')),
            array('name' => 'Detail', 'url' => \URL::route('permission.detail', array('id' => $id)))
        ));

        $permission = $this->permission->getById($id);
        if (!$id || !$permission) {
            abort(404);
        }

        return view('backend.permission.detail')->with('permission',$permission);
    }

    /**
     * Add Permission
     */
    public function add(AddPermissionRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Permission', 'url' => \URL::route('permission.index')),
            array('name' => 'Add', 'url' => \URL::route('permission.add'))
        ));

        if ($request->isMethod('post'))
        {

            $result = $this->permissionRepository->add();

            if ($result) {
                Log::write('admin_access', 'Access Permission Management. Added Permission', ['admin' => Auth::user()]);
                return redirect()->route('permission.index');
            } else {
                abort(500);
            }

        } else {
            return view('backend.permission.add')->with('routes', \Route::getRoutes());
        }
        
    }

    /**
     * Update Permission
     * @return id Permission id
     */
    public function update($id, UpdatePermissionRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Permission', 'url' => \URL::route('permission.index')),
            array('name' => 'Update', 'url' => \URL::route('permission.update', array('id' => $id)))
        ));

        $permission = $this->permissionRepository->getById($id);

        if (!$id || !$permission) {
            abort(404);
        }

        if ($request->isMethod('post'))
        {
            $result = $this->permissionRepository->update($id);

            if ($result) {
                Log::write('admin_access', 'Access Permission Management. Updated Permission ID['.$id.']', ['admin' => Auth::user(), 'result' => $result]);
                return redirect()->route('permission.index');
            } else {
                abort(500);
            }

        } else {
            return view('backend.permission.update')->with('permission',$permission)->with('routes', \Route::getRoutes());
        }
    }

    /**
     * Delete Role
     * @return id Permission id
     */
    public function delete($id) 
    {
        $permission = $this->permissionRepository->getById($id);

        if (!$id || !$permission) {
            abort(404);
        }

        $result = $this->permissionRepository->delete($permission);

        if ($result) {
            Log::write('admin_access', 'Access Permission Management. Deleted Permission ID['.$id.']', ['admin' => Auth::user(), 'permission' => $permission]);
            return redirect()->route('permission.index');
        } else {
            abort(500);
        }

    }

}
