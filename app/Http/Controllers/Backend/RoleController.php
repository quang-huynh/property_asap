<?php namespace App\Http\Controllers\Backend;


use Request;

use App\Http\Services\Backend\UserService;
use App\Http\Services\Backend\RoleService;
use App\Models\Entities\Role;
use App\Models\Repositories\RoleRepository;
use App\Http\Requests\Backend\AddRoleRequest;
use App\Http\Requests\Backend\UpdateRoleRequest;

use ChannelLog as Log;

use Auth;


class RoleController extends BaseController {

    protected $roleService;
    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoleService $roleService, RoleRepository $roleRepository)
    {
        $this->roleService = $roleService;
        $this->roleRepository = $roleRepository;
        
    }

    /**
     * Show the application dashboard to the role.
     *
     * @return 
     */
    public function index()
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
        ));

        $roles = $this->roleRepository->getPaginated(config('site.paginator'));
        return view('backend.role.index')->with('roles',$roles);
    }

    /**
     * Detail Role
     * @return 
     */
    public function detail($id)
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Detail', 'url' => \URL::route('role.detail', array('id' => $id)))
        ));

        $role = $this->role->getById($id);
        if (!$id || !$role) {
            abort(404);
        }

        return view('backend.role.detail')->with('role',$role);
    }


    /**
     * Add Role
     */
    public function add(AddRoleRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Add', 'url' => \URL::route('role.add'))
        ));

        if ($request->isMethod('post'))
        {

            $result = $this->roleRepository->add();

            if ($result) {
                Log::write('admin_access', 'Add Role Management.', ['admin' => Auth::user(), 'request' => $request ,'result' => $result]);
                return redirect()->route('role.index');
            } else {
                abort(500);
            }

        } else {
            return view('backend.role.add');
        }
        
    }


    /**
     * Update Role
     * @return View
     */
    public function update($id, UpdateRoleRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Update', 'url' => \URL::route('role.update', array('id' => $id)))
        ));

        $role = $this->roleRepository->getById($id);

        if (!$id || !$role) {
            abort(404);
        }

        if ($request->isMethod('post'))
        {

            $result = $this->roleRepository->update($id);

            if ($result) {
                Log::write('admin_access', 'Update Role Management. Update Role ID['.$id.']', ['admin' => Auth::user(), 'request' => $request ,'result' => $result]);
                return redirect()->route('role.index');
            } else {
                abort(500);
            }            

        } else {
            return view('backend.role.update')->with('role',$role);
        }
    }

    /**
     * Delete Role
     * @return View
     */
    public function delete($id) 
    {
        $role = $this->roleRepository->getById($id);

        if (!$id || !$role) {
            abort(404);
        }

        $role->permissions()->detach();

        $result = $this->roleRepository->delete($role);

        if ($result) {
            Log::write('admin_access', 'Update Role Management. Update Role ID['.$id.']', ['admin' => Auth::user(), 'role' => $role ,'result' => $result]);
            return redirect()->route('role.index');
        } else {
            abort(500);
        }
        
    }

}
