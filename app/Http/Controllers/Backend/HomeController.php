<?php namespace App\Http\Controllers\Backend;

use Auth;

use App\Models\Repositories\UserRepository;

use ChannelLog as Log;


class HomeController extends BaseController {


	protected $userRepository;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->middleware('auth');

		$this->userRepository = $userRepository;
	}


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('backend.index');
	}



	public function logout() 
	{
		Log::write('admin_auth', 'Logout admin.', ['user' => Auth::user()]);
		Auth::logout();

		return redirect()->route('admin.login');
	}


}
