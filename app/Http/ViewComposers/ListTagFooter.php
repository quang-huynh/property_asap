<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Http\Services\Frontend\TagService;

class ListTagFooter
{

    protected $tagService;


    public function __construct(TagService $tagService)
    {
        $this->tagService = $tagService;
    }


    public function compose(View $view)
    {
        $tags = $this->tagService->getPopularTags(15);
        $view->with('popularTags', $tags);
    }
}