<?php 
namespace App\Http\Services\Common;


class Common {

    public static function shorten($text, $length = 50, $more = '...') 
    {
        $text = str_replace(array("\n","</br>","<br>","<br />","<br/>"), ' ', $text);

        if (strlen($text) <= $length) {
            return $text;
        }

        preg_match("/(.{1,$length})\b/", $text, $matches);

        if(substr($matches[1],-1) != ' ') {
            return \app\helpers\Common::shorten($matches[1],strlen($matches[1])-1);
        }
        
        return $matches[1] . ($more ? $more : '');
    }

    public static function readExcel($filename)
    {
        require(app_path() . DIRECTORY_SEPARATOR . 'libraries'. DIRECTORY_SEPARATOR . 'PHPExcel'. DIRECTORY_SEPARATOR . 'IOFactory.php');
        $inputFileType = 'Excel5';

        $inputFileName = $filename;

        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        return $sheetData;

    }

    public static function getArrAlphabet()
    {
        return range('A', 'Z');
    }


}