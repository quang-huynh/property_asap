<?php namespace App\Http\Services\Common;

use App\Models\Repositories\MediaRepository;

use Illuminate\Http\Request;

use Auth;

use App\Helpers\Slugify;

class MediaService {

    protected $mediaRepository;

    public function __construct(MediaRepository $mediaRepository) 
    {
        $this->mediaRepository = $mediaRepository;
    }


    public function getByPostId($postId)
    {
        $media =  $this->mediaRepository->getByPostId($postId);
        if (!empty($media)) {
        	return $media;
        }
        return false;
    }


    public function deleteByPostId($postId)
    {
        $mediaOfPost = $this->getByPostId($postId);
        foreach ($mediaOfPost as $key => $media) {
            $result = $this->mediaRepository->delete($media);
            unlink($media->slug);
            Log::write('admin_post', 'Deleted Media of Post ID['.$postId.']', ['admin' => Auth::user(), 'media' => $media, 'result' => $result]);
        }

        return true;
    }


}


?>
