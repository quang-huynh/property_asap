<?php namespace App\Http\Services\Frontend;

use App\Models\Repositories\PostRepository;

use App\Models\Repositories\LocationRepository;
use App\Models\Repositories\UserRepository;
use App\Http\Services\Frontend\LocationService;
use App\Http\Services\Frontend\TagService;

use App\Http\Services\Common\MediaService;

use App\Http\Services\Frontend\WishlistService;

use App\Helpers\Slugify;

use Validator;

use Auth;

class PostService 
{
	protected $postRepository;
	protected $locationRepository;
	protected $userRepository;
	protected $locationService;
	protected $tagService;
	protected $wishlistService;
	protected $mediaService;

	public function __construct(PostRepository $postRepository,LocationService $locationService , LocationRepository $locationRepository, TagService $tagService, UserRepository $userRepository, WishlistService $wishlistService, MediaService $mediaService) 
	{
		$this->postRepository = $postRepository;
		$this->locationRepository = $locationRepository;
		$this->locationService = $locationService;
		$this->tagService = $tagService;
		$this->userRepository = $userRepository;
		$this->wishlistService = $wishlistService;
		$this->mediaService = $mediaService;
	}


	/**
	 * Create a new post
	 * @param  array  		$input
	 * @return boolean      
	 */
	public function save($input)
	{
		try {
			if ( empty($input['title']) || empty($input['description']) || empty($input['type']) || empty($input['price']) ) {
				return false;
			}
			$slugify = new Slugify();
			$slug = $slugify->slugify($input['title']);
			$data = [];
			$data['slug'] = $slug;
			$data['title'] = addslashes($input['title']);
			$data['description'] = addslashes($input['description']);
			$data['full_location'] = $input['full_location'];

			$arrLocationsOfPost = $this->locationService->addFullLocation($input['location_number_street'], $input['location_street'], $input['location_city'], $input['location_state'], $input['location_country']);
			$data['street_number'] = $arrLocationsOfPost['street_number']->id;
			$data['street'] = $arrLocationsOfPost['street']->id;
			$data['city'] = $arrLocationsOfPost['city']->id;
			$data['state'] = $arrLocationsOfPost['state']->id;
			$data['country'] = $arrLocationsOfPost['country']->id;

			$data['status'] = config('asap.post_status')['pending'];
			$data['type'] = $input['type'];
			$data['price'] = $input['price'];
			$data['name_contact'] = $input['name_contact'];
			$data['email_contact'] = $input['email_contact'];
			$data['address_contact'] = $input['address_contact'];
			$data['phone_number'] = $input['phone_number'];
			$data['home_number'] = $input['home_number'];

			$data['package'] = $input['package'];
			$data['property_type'] = $input['property_type'];
			$data['bedrooms'] = $input['bedrooms'];
			$data['bathrooms'] = $input['bathrooms'];
			$data['car_spaces'] = $input['car_spaces'];
			$data['size'] = $input['size'];

			$data['setting'] = '';
			$data['created_by'] = Auth::user()->id;
			$post = $this->postRepository->savePost($data);


			$this->postRepository->updateAttributeById($post->id, 'slug', $post->slug.'-'.$post->id);


			// $properties = $this->propertyRepository->getAllActive();
			// foreach ($properties as $key => $property) {
			// 	if (!empty($input[$property->slug])) {
			// 		$post->properties()->attach($property->id, [ 'text' => $input[$property->slug], 'value' => $input[$property->slug] ] );
			// 	}
			// }

			//$arrLocationsOfPost = $this->locationService->addFullLocation($input['location_number_street'], $input['location_street'], $input['location_city'], $input['location_state'], $input['location_country']);
			// foreach ($arrLocationsOfPost as $key => $location) {
			// 	$post->locations()->attach($location->id);
			// }

			if (!empty($input['tags'])) {
				$arrTags = $this->tagService->addTags($input['tags']);

				foreach ($arrTags as $key => $tag) {
					$post->tags()->attach($tag->id);
				}
			}
				
			

			return $post;
		} catch (Exception $e) {
			return false;
		}
			
	}



	/**
	 * Update post by Id
	 * @param  int  	$id    		post ID
	 * @param  array  	$input 		Form Data
	 * @return boolean        		
	 */
	public function update($input)
	{
		if (!empty($input['id'])) {
			$id = $input['id'];
			$post = $this->postRepository->getById($id);
			$changeLocation = false;
			if (!empty($post)) {
				$slugify = new Slugify();
				$slug = $slugify->slugify($input['title']);
				$post->title = addslashes($input['title']);
				$post->description = addslashes($input['description']);

				if (!empty($input['full_location'])) {
					$post->full_location = !empty($input['full_location']) ? $input['full_location'] : $post->full_location;
					$arrLocationsOfPost = $this->locationService->addFullLocation($input['location_number_street'], $input['location_street'], $input['location_city'], $input['location_state'], $input['location_country']);
					$post->street_number = $arrLocationsOfPost['street_number']->id;
					$post->street = $arrLocationsOfPost['street']->id;
					$post->city = $arrLocationsOfPost['city']->id;
					$post->state = $arrLocationsOfPost['state']->id;
					$post->country = $arrLocationsOfPost['country']->id;
				}
					

				$post->type = !empty($input['type']) ? $input['type'] : $post->type;
				$post->price = !empty($input['price']) ? $input['price'] : $post->price;
				$post->name_contact = !empty($input['name_contact']) ? $input['name_contact'] : $post->name_contact;
				$post->email_contact = !empty($input['email_contact']) ? $input['email_contact'] : $post->email_contact;
				$post->address_contact = !empty($input['address_contact']) ? $input['address_contact'] : $post->address_contact;
				$post->phone_number = !empty($input['phone_number']) ? $input['phone_number'] : $post->phone_number;
				$post->home_number = !empty($input['home_number']) ? $input['home_number'] : $post->home_number;

				$post->package = !empty($input['package']) ? $input['package'] : $post->package;
				$post->property_type = !empty($input['property_type']) ? $input['property_type'] : $post->property_type;
				$post->bedrooms = !empty($input['bedrooms']) ? $input['bedrooms'] : $post->bedrooms;
				$post->bathrooms = !empty($input['bathrooms']) ? $input['bathrooms'] : $post->bathrooms;
				$post->car_spaces = !empty($input['car_spaces']) ? $input['car_spaces'] : $post->car_spaces;
				$post->size = !empty($input['size']) ? $input['size'] : $post->size;
				
				$result = $this->postRepository->save($post);	

				if (!$result) return false;

				Log::write('user', 'Updated Post ID['.$post->id.']', ['user' => Auth::user(), 'post' => $post, 'result' => $result]);

				$this->postRepository->updateAttributeById($post->id, 'slug', $slug.'-'.$post->id);
					

				$post->tags()->detach(); //detach all tag of post
				if (!empty($input['tags'])) {
					$arrTags = $this->tagService->addTags($input['tags'], true);

					foreach ($arrTags as $key => $tag) {
						$post->tags()->attach($tag->id);
					}
				}

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}


	public function refreshViewers($post)
	{
		$post->viewers = intval($post->viewers) + 1;

		return $this->postRepository->save($post);	
	}


	public function getPostBySlug($slug)
	{
		return $this->postRepository->getByAttribute('slug', $slug);
	}


	/**
	 * Delete post by Id
	 * @param  int 		$id 		post Id
	 * @return boolean     			
	 */
	public function delete($id)
	{

		$post = $this->postRepository->getById($id);

		$result = $this->postRepository->changeStatus($id, $status);

		if ($result) {
			$tags = $post->tags()->detach();
			Log::write('user', '------------------POST CHANGE STATUS----------------------------');
			Log::write('user', 'Deleted Post ID['.$post->id.']', ['user' => Auth::user(), 'post' => $post, 'result' => $result]);
			Log::write('user', 'Deleted all tag of Post ID['.$post->id.']', ['user' => Auth::user(), 'tags' => $tags]);
			$deleteMedia = $this->mediaService->deleteByPostId($post->id);
			Log::write('user', 'Deleted all media of Post ID['.$post->id.']', ['user' => Auth::user(), 'deleteMedia' => $deleteMedia]);
			Log::write('user', '----------------------------------------------------------------');
		}

		// $post = $this->postRepository->getById($id);

		// if ($post) {
		// 	return $this->postRepository->delete($post);
		// } else {
		// 	return false;
		// }
	}


	public function checkOwnerById($id)
	{
		$post = $this->postRepository->getById($id);

		if ($post->getCreatedBy() != Auth::user()->id) 
			return false;

		return true;
	}

	public function checkOwnerBySlug($slug)
	{
		$post = $this->postRepository->getByAttribute('slug', $slug);

		if ($post->getCreatedBy() != Auth::user()->id) 
			return false;

		return true;
	}



	public function search($request = [])
	{
		$where = [];

		//offset
		$offset = !empty($request['offset']) ? $request['offset'] : 0;
		
		//location
		$where['full_location'] = !empty($request['full_location']) ? trim($request['full_location']) : null;

		if (!empty($request['location_number_street'])) {
			$where['location_number_street'] = !empty($this->locationRepository->getByAttribute('name', $request['location_number_street'])) ? $this->locationRepository->getByAttribute('name', $request['location_number_street'])->id : null;
		} else {
			$where['location_number_street'] = null;
		}
		if (!empty($request['location_street'])) {
			$where['location_street'] = !empty($this->locationRepository->getByAttribute('name', $request['location_street'])) ? $this->locationRepository->getByAttribute('name', $request['location_street'])->id : null;
		} else {
			$where['location_street'] = null;
		}
		if (!empty($request['location_city'])) {
			$where['location_city'] = !empty($this->locationRepository->getByAttribute('name', $request['location_city'])) ? $this->locationRepository->getByAttribute('name', $request['location_city'])->id : null;
		} else {
			$where['location_city'] = null;
		}
		if (!empty($request['location_state'])) {
			$where['location_state'] = !empty($this->locationRepository->getByAttribute('name', $request['location_state'])) ? $this->locationRepository->getByAttribute('name', $request['location_state'])->id : null;
		} else {
			$where['location_state'] = null;
		}
		if (!empty($request['location_country'])) {
			$where['location_country'] = !empty($this->locationRepository->getByAttribute('name', $request['location_country'])) ? $this->locationRepository->getByAttribute('name', $request['location_country'])->id : null;
		} else {
			$where['location_country'] = null;
		}

		//properties post
		$where['property_type'] = !empty($request['property_type']) ? $request['property_type'] : null;
		if (is_array($where['property_type'])) {
			$where['property_type'] = implode(',', $where['property_type']);
		}

		//process with bedrooms, bathrooms, car_sapces
		if (!empty($request['bedrooms'])) {
			$where['bedrooms'] = '';
			$flag = false;
			foreach (config('asap.bedrooms') as $key => $value) {
				if ($flag) {
					$where['bedrooms'] .= $value . ",";
					continue;
				}
				if ($value == $request['bedrooms']) {
					$flag = true;
					$where['bedrooms'] .= $value . ",";
				}
			}
			$where['bedrooms'] = rtrim($where['bedrooms'], ',');
		} else {
			$where['bedrooms'] = null;
		}
		if (!empty($request['bathrooms'])) {
			$where['bathrooms'] = '';
			$flag = false;
			foreach (config('asap.bathrooms') as $key => $value) {
				if ($flag) {
					$where['bathrooms'] .= $value . ",";
					continue;
				}
				if ($value == $request['bathrooms']) {
					$flag = true;
					$where['bathrooms'] .= $value . ",";
				}
			}
			$where['bathrooms'] = rtrim($where['bathrooms'], ',');
		} else {
			$where['bathrooms'] = null;
		}
		if (!empty($request['car_spaces'])) {
			$where['car_spaces'] = '';
			$flag = false;
			foreach (config('asap.car_spaces') as $key => $value) {
				if ($flag) {
					$where['car_spaces'] .= $value . ",";
					continue;
				}
				if ($value == $request['car_spaces']) {
					$flag = true;
					$where['car_spaces'] .= $value . ",";
				}
			}
			$where['car_spaces'] = rtrim($where['car_spaces'], ',');
		} else {
			$where['car_spaces'] = null;
		}

		//size
		if (!empty($request['size'])) {
			$where['size'] = $request['size'];
			$where['size_compare'] = !empty($request['size_compare']) ? $request['size_compare'] : '>=';
		} else {
			$where['size'] = null;
		}

		//price
		if (!empty($request['minPrice']) && !empty($request['maxPrice']) ) {
			$where['min_price'] = $request['minPrice'];
			$where['max_price'] = $request['maxPrice'];
		} else {
			if ( !empty($request['price']) && $request['price'] != (config('asap.min_price') .','. config('asap.max_price')) ) {
				$price = explode(',', $request['price']);
				$where['min_price'] = intval($price[0]);
				$where['max_price'] = intval($price[1]);
			} else {
				$where['min_price'] = null;
				$where['max_price'] = null;
			}
		}
			

		//tags
		$where['tags'] = !empty($request['tags']) ? $request['tags'] : null;

		//sort by
		if (!empty($request['sort_by'])) {
			$sortBy = [];
			switch ($request['sort_by']) {
				case 1:
					$sortBy['value'] = 1;
					$sortBy['column'] = 'published_date';
					$sortBy['sort'] = 'DESC';
					break;
				case 2:
					$sortBy['value'] = 2;
					$sortBy['column'] = 'price';
					$sortBy['sort'] = 'ASC';
					break;
				case 3:
					$sortBy['value'] = 3;
					$sortBy['column'] = 'price';
					$sortBy['sort'] = 'DESC';
					break;
				case 4:
					$sortBy['top'] = true;
					$sortBy['value'] = 4;
					$sortBy['column'] = 'package';
					$sortBy['sort'] = 'DESC';
					break;
				default:
					$sortBy['value'] = 1;
					$sortBy['column'] = 'published_date';
					$sortBy['sort'] = 'DESC';
					break;
			};
		} else {
			$sortBy['value'] = 1;
			$sortBy['column'] = 'published_date';
			$sortBy['sort'] = 'DESC';
		}


		//search by full location
		if (isset($where['full_location'])) {
			$result = $this->postRepository->getByAttribute('full_location', $where['full_location']);
			if (!empty($result)) {
				return $result;
			}
		}
			

		$limit = config('asap.limit_post_search');

		//search by location and properties
		$result = $this->callSearchAdvanced($where, $sortBy, $limit, $offset);
		if (!empty($result)) {
			return $result;
		}

		return false;
	}


	public function callSearchAdvanced($where, $sortBy, $limit, $offset = 0)
	{
		return $this->postRepository->callSearchAdvanced($where, $sortBy, $limit, $offset);
	}


	/**
	 * check expired pin Top Feature On Search Page by Post Id
	 * @param  integer $id post id
	 * @return boolean true/false
	 */
	public function pinFeatureOnSearchPage($id)
	{
		$post = $this->postRepository->getById($id);

		if ($post->package != config('asap.package')['ultimate']) {
			return false;
		}

		$pinFeatureDays = config('asap.pin_feature_days');

		$expiredPin = $post->published_date_count + $pinFeatureDays*24*60*60;

		if ($expiredPin >= time()) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * check expired pin Top New On Search Page by Post Id
	 * @param  integer $id post id
	 * @return boolean true/false
	 */
	public function PinNewOnSearchPage($id)
	{
		$post = $this->postRepository->getById($id);

		$pinNewDays = config('asap.pin_new_days');

		$expiredPin = $post->published_date_count + $pinNewDays*24*60*60;

		if ($expiredPin >= time()) {
			return true;
		} else {
			return false;
		}
	}

	
	public function getFullNameAuthor($userId)
	{
		return $this->userRepository->getById($userId);
	}


	/**
	 * get feature posts
	 * @param  integer $limit
	 * @return collection $posts
	 */
	public function getFeaturePosts($limit)
	{
		$pinTopDays = config('asap.pin_feature_days'); 

		$expiredPin = $pinTopDays*24*60*60;

		return $this->postRepository->getFeaturePosts(time(), $expiredPin, $limit);
	}


	/**
	 * get nearly posts
	 * @param  integer $limit
	 * @return collection $posts
	 */
	public function getNearlyPosts($post)
	{
		if (!empty($post)) {
			
			$where = [];

			$where['id'] = $post->id;

			$where['location_street'] = $post->street;

			$where['location_city'] = $post->city;

			$where['location_state'] = $post->state;

			$where['location_country'] = $post->country;


			$sortBy = [];
			$sortBy['value'] = 1;
			$sortBy['column'] = 'published_date';
			$sortBy['sort'] = 'DESC';

			$limit = config('asap.limit_post_nearly');
			$offset = 0;

			//search by location and properties
			$result = $this->callSearchAdvanced($where, $sortBy, $limit, $offset);

			if (!empty($result)) {
				return $result;
			}
		}

		return false;
	}


	/**
	 * get nearly posts
	 * @param  integer $limit
	 * @return collection $posts
	 */
	public function getRecentlyViewedPosts($arrPostIds)
	{
		if (!empty($arrPostIds)) {
			
			$where['ids'] = implode(',', $arrPostIds);

			$sortBy = [];
			$sortBy['value'] = 1;
			$sortBy['column'] = 'published_date';
			$sortBy['sort'] = 'DESC';

			$limit = config('asap.limit_post_viewed');
			$offset = 0;

			//search by location and properties
			$result = $this->callSearchAdvanced($where, $sortBy, $limit, $offset);

			if (!empty($result)) {
				return $result;
			}
		}

		return false;
	}


	public function hasWishList($postId)
	{
		return $this->wishlistService->hasWishList($postId);
	}



	public function getMyProperties()
	{
		return $this->postRepository->getMyProperties();
	}


	public function getMyPendingProperties()
	{
		return $this->postRepository->getMyPendingProperties();
	}


	public function closePostById($id)
	{
		if (!empty($id)) {
			$post = $this->postRepository->getById($id);

			$result = $this->postRepository->changeStatus($id, config('asap.post_status')['closed']);

			if ($result) {
				return true;
			}
		}
		return false;
	}

}
