<?php namespace App\Http\Services\Frontend;

use App\Models\Repositories\TagRepository;

use Validator;

use Auth;

use App\Helpers\Slugify;

class TagService 
{
	protected $tagRepository;

	protected $slugify;

	public function __construct(TagRepository $tagRepository) 
	{
		$this->tagRepository = $tagRepository;

		$this->slugify = new Slugify();
	}

	/**
	 * Create a new tag
	 * @param  array  		$input
	 * @return boolean      
	 */
	public function addTags($tags, $isUpdate = false)
	{

		$arrTags = explode(',', $tags);
		$arrTagsInstance = array();
		foreach ($arrTags as $key => $ob) {
			$slugTag = $this->slugify->slugify($ob);
			$tag = $this->tagRepository->findbySlug($slugTag);
			if ( empty($tag) ) {
				$data = [];
				$data['slug'] = $slugTag;
				$data['name'] = $ob;
				$data['parent_id'] = '';
				$tag = $this->tagRepository->saveTag($data);
			}
			else {
				if ($isUpdate == false) {
					$this->tagRepository->updateAttributeById($tag->id, 'popular', intval($tag->popular) + 1);
				}
			}
			$arrTagsInstance[] = $tag;
		}
		return $arrTagsInstance;
		
	}

	public function getPopularTags($limit = null)
	{
		return $this->tagRepository->getPopularTags();
	}



}