<?php namespace App\Http\Services\Frontend;

use App\Models\Repositories\LocationRepository;

use Validator;

use Auth;

use App\Helpers\Slugify;

class LocationService 
{
	protected $locationRepository;

	protected $slugify;

	public function __construct(LocationRepository $locationRepository) 
	{
		$this->locationRepository = $locationRepository;

		$this->slugify = new Slugify();
	}

	/**
	 * Create a new location
	 * @param  array  		$input
	 * @return boolean      
	 */
	public function addFullLocation($location_number_street, $location_street, $location_city, $location_state, $location_country)
	{

		$arrLocations = array();

		$slugLocationCountry = $this->slugify->slugify($location_country);
		$country = $this->locationRepository->findbySlugTypeParentId($slugLocationCountry, 5);
		if ( empty($country) ) {
			$data = [];
			$data['slug'] = $slugLocationCountry;
			$data['name'] = $location_country;
			$data['type'] = config('asap.location_type')['location_country'];
			$data['parent_id'] = 0;
			$country = $this->locationRepository->saveLocation($data);
			$countryId = $country->id;
		} else {
			$countryId = $country->id;
		}
		$arrLocations['country'] = $country;


			

		$slugLocationState = $this->slugify->slugify($location_state);
		$state = $this->locationRepository->findbySlugTypeParentId($slugLocationState, 4, $countryId);
		if ( empty($state) ) {
			$data = [];
			$data['slug'] = $slugLocationState;
			$data['name'] = $location_state;
			$data['type'] = config('asap.location_type')['location_state'];
			$data['parent_id'] = $countryId;
			$state = $this->locationRepository->saveLocation($data);
			$stateId = $state->id;
		} else {
			$stateId = $state->id;
		}
		$arrLocations['state'] = $state;

		$slugLocationCity = $this->slugify->slugify($location_city);
		$city = $this->locationRepository->findbySlugTypeParentId($slugLocationCity, 3, $stateId);
		if ( empty($city) ) {
			$data = [];
			$data['slug'] = $slugLocationCity;
			$data['name'] = $location_city;
			$data['type'] = config('asap.location_type')['location_city'];
			$data['parent_id'] = $stateId;
			$city = $this->locationRepository->saveLocation($data);
			$cityId = $city->id;
		} else {
			$cityId = $city->id;
		}
		$arrLocations['city'] = $city;

		$slugLocationStreet = $this->slugify->slugify($location_street);
		$street = $this->locationRepository->findbySlugTypeParentId($slugLocationStreet, 2, $cityId);
		if ( empty($street) ) {
			$data = [];
			$data['slug'] = $slugLocationStreet;
			$data['name'] = $location_street;
			$data['type'] = config('asap.location_type')['location_street'];
			$data['parent_id'] = $cityId;
			$street = $this->locationRepository->saveLocation($data);
			$streetId = $street->id;
		} else {
			$streetId = $street->id;
		}
		$arrLocations['street'] = $street;


		$slugLocationNumber = $this->slugify->slugify($location_number_street);
		$number = $this->locationRepository->findbySlugTypeParentId($slugLocationNumber, 1, $streetId);
		if ( empty($number) ) {
			$data = [];
			$data['slug'] = $slugLocationNumber;
			$data['name'] = $location_number_street;
			$data['type'] = config('asap.location_type')['location_number_street'];
			$data['parent_id'] = $streetId;
			$number = $this->locationRepository->saveLocation($data);
			$numberId = $number->id;
		} else {
			$numberId = $number->id;
		}
		$arrLocations['street_number'] = $number;


		return $arrLocations;
			

		
	}



}