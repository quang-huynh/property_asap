<?php namespace App\Http\Services\Frontend;

use App\Models\Repositories\WishlistRepository;

use Validator;

use Auth;

class WishlistService 
{
	protected $wishlistRepository;

	public function __construct(WishlistRepository $wishlistRepository) 
	{
		$this->wishlistRepository = $wishlistRepository;
	}

	public function hasWishList($postId)
	{
		$result = $this->wishlistRepository->findByPostId($postId);
		if (!empty($result)) {
			return true;
		}
		return false;
	}


	public function getWishlistByUser()
	{
		return $this->wishlistRepository->findByUserId();
	}


	public function save($postId)
	{
		if (!empty(Auth::user())) {
			$data = [];
			$data['user_id'] = Auth::user()->id;
			$data['post_id'] = $postId;
			$data['status'] = 1;
			$data['created_at'] = date('Y-m-d H:i:s');
	        $data['updated_at'] = date('Y-m-d H:i:s');

			$wishlist = $this->wishlistRepository->saveWishlist($data);

			return true;
		} else {
			return false;
		}
			
	}

	public function remove($postId)
	{
		return $this->wishlistRepository->removeByPostId($postId);
	}
	

}