<?php
/**
 * @author 
 * @datetime : 2017-06-25
 */
namespace App\Http\Services\Frontend;

use App\Models\Entities\User;
use Auth;
use Validator;


class SocialService
{
    const FACEBOOK = 'facebook';
    const GOOGLE = 'google';
    static $socials = [self::FACEBOOK, self::GOOGLE];

    /**
     * |----------------------------------------------------------------
     * | GET USER FROM USER;
     * |----------------------------------------------------------------
     * | @param string $social
     * | @param int $scid
     * | @return null
     * |----------------------------------------------------------------
     */
    static function getUserSocial($social = '', $scid = 0)
    {
        if (!empty($social) && !empty($scid) && in_array($social, self::$socials)) {
            if ($social == self::FACEBOOK) {
                return User::where('facebook_id', $scid)->first();
            } elseif ($social == self::GOOGLE) {
                return User::where('google_id', $scid)->first();
            }
        }
        return null;
    }

    /**
     * |----------------------------------------------------------------
     * | GET USER BY EMAIL;
     * |----------------------------------------------------------------
     * | @param string $social
     * | @param int $scid
     * | @return User
     * |----------------------------------------------------------------
     */
    static function getUserByEmail($email = '')
    {
        if ($email) {
            return User::where('email', $email)->first();
        }
        return null;
    }


    static function setLogin($user = null)
    {
        if ($user) {
            return Auth::guard()->login($user);
        }
        return false;
    }


}