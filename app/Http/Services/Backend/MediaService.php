<?php namespace App\Http\Services\Backend;

use App\Models\Entities\Role;
use App\Models\Entities\Permission;
use App\Models\Repositories\MediaRepository;

use Request;
use Auth;
use ChannelLog as Log;

use App\Helpers\Slugify;

class MediaService {

    protected $mediaRepository;

    public function __construct(MediaRepository $mediaRepository) 
    {
        $this->mediaRepository = $mediaRepository;
    }


    public function getByPostId($postId)
    {
        $media =  $this->mediaRepository->getByPostId($postId);
        if (!empty($media)) {
        	return $media;
        }
        return false;
    }


    public function addImage($request, $postId)
    {
    	$name = $postId . '_' . uniqid() . '.' . $request['image']->getClientOriginalExtension();
    	$slugify = new Slugify();
		$slug = config('asap.upload_post_path') . $name;
		$data = [];
		$data['slug'] = $slug ;
		$data['name'] = $name;
		$data['type'] = 'image';
		$data['extension'] = $request['image']->getClientOriginalExtension();
		$data['size'] = $request['image']->getSize();
		$data['post_id'] = $postId;
		$data['created_by'] = Auth::user()->id;
		$media = $this->mediaRepository->saveMedia($data);

		return $media;
    }


    public function update($postId, $id)
    {
        if (!empty($postId) && !empty($id)) {
            //remove all featured image
            $result = $this->removeFeaturedImages($postId);

            $media = $this->mediaRepository->getById($id);
            
            if (!empty($media)) {
                
                if ($media->is_featured == 1) {
                    $media->is_featured = 0;
                } else {
                    $media->is_featured = 1;    
                }

                $result = $this->mediaRepository->save($media);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function removeFeaturedImages($postId)
    {
        return $this->mediaRepository->removeFeaturedImages($postId);
    }


}


?>
