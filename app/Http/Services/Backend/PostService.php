<?php namespace App\Http\Services\Backend;

use App\Models\Repositories\PostRepository;
use App\Models\Repositories\LocationRepository;
use App\Models\Repositories\TagRepository;

use App\Http\Services\Common\MediaService;
use App\Http\Services\Frontend\TagService;

use Illuminate\Http\Request;

use Auth;
use ChannelLog as Log;

use App\Helpers\Slugify;

class PostService {

    protected $postRepository;
    protected $locationRepository;
    protected $tagService;
    protected $tagRepository;
    protected $mediaService;

    public function __construct(PostRepository $postRepository, LocationRepository $locationRepository, TagService $tagService, TagRepository $tagRepository, MediaService $mediaService) 
    {
        $this->postRepository = $postRepository;
        $this->locationRepository = $locationRepository;
        $this->tagService = $tagService;
        $this->tagRepository = $tagRepository;
        $this->mediaService = $mediaService;
    }



    /**
     * Search Post
     * @param  array  $params
     * @param
     * @return collection
     */
    public function search($request)
    {
    	$where = [];

    	if (!empty($request['full_location'])) {
			$where['location_street'] = !empty($this->locationRepository->getByAttribute('name', $request['location_street'])) ? $this->locationRepository->getByAttribute('name', $request['location_street'])->id : null;
			$where['location_city'] = !empty($this->locationRepository->getByAttribute('name', $request['location_city'])) ? $this->locationRepository->getByAttribute('name', $request['location_city'])->id : null;
			$where['location_state'] = !empty($this->locationRepository->getByAttribute('name', $request['location_state'])) ? $this->locationRepository->getByAttribute('name', $request['location_state'])->id : null;
			$where['location_country'] = !empty($this->locationRepository->getByAttribute('name', $request['location_country'])) ? $this->locationRepository->getByAttribute('name', $request['location_country'])->id : null;
		} else {
			$where['full_location'] = null;
		}

		if (!empty($request['id_title'])) {
			$where['id_title'] = $request['id_title'];
		} else {
			$where['id_title'] = null;
		}

		if (!empty($request['user_id'])) {
			$where['user_id'] = $request['user_id'];
		} else {
			$where['user_id'] = null;
		}

		if (!empty($request['status'])) {
			$where['status'] = $request['status'];
		} else {
			$where['status'] = null;
		}

		if (!empty($request['package'])) {
			$where['package'] = $request['package'];
		} else {
			$where['package'] = null;
		}

		//search by full location
		if (isset($where['full_location'])) {
			$result = $this->postRepository->getByAttribute('full_location', $where['full_location']);
			if (!empty($result)) {
				return $result;
			}
		}


        return $this->postRepository->callSearchAdmin($where);
    }


    /**
	 * Update post by Id
	 * @param  int  	$id    		post ID
	 * @param  array  	$input 		Form Data
	 * @return boolean        		
	 */
	public function updateStatus($input)
	{
		if (!empty($input['id'])) {
			$id = $input['id'];
			$post = $this->postRepository->getById($id);
			
			if (!empty($post)) {
				

				$post->status = $input['status'];
				if ($input['status'] == config('asap.post_status')['published']) {
					$post->published_date = date('Y-m-d H:i:s');
					$post->published_date_count = time();
				}
				
				$result = $this->postRepository->save($post);

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}



	/**
	 * Update post by Id
	 * @param  int  	$id    		post ID
	 * @param  array  	$input 		Form Data
	 * @return boolean        		
	 */
	public function update($input)
	{
		if (!empty($input['id'])) {
			$id = $input['id'];
			$post = $this->postRepository->getById($id);
			$changeLocation = false;
			if (!empty($post)) {
				$slugify = new Slugify();
				$slug = $slugify->slugify($input['title']);
				$post->title = addslashes($input['title']);
				$post->description = addslashes($input['description']);

				if (!empty($input['full_location'])) {
					$post->full_location = !empty($input['full_location']) ? $input['full_location'] : $post->full_location;
					$arrLocationsOfPost = $this->locationService->addFullLocation($input['location_number_street'], $input['location_street'], $input['location_city'], $input['location_state'], $input['location_country']);
					$post->street_number = $arrLocationsOfPost['street_number']->id;
					$post->street = $arrLocationsOfPost['street']->id;
					$post->city = $arrLocationsOfPost['city']->id;
					$post->state = $arrLocationsOfPost['state']->id;
					$post->country = $arrLocationsOfPost['country']->id;
				}
				
				$post->status = !empty($input['status']) ? $input['status'] : $post->status;
				if (!empty($input['status']) && $input['status'] == config('asap.post_status')['published']) {
					$post->published_date = date('Y-m-d H:i:s');
					$post->published_date_count = time();
				}

				$post->type = !empty($input['type']) ? $input['type'] : $post->type;
				$post->price = !empty($input['price']) ? $input['price'] : $post->price;
				$post->name_contact = !empty($input['name_contact']) ? $input['name_contact'] : $post->name_contact;
				$post->email_contact = !empty($input['email_contact']) ? $input['email_contact'] : $post->email_contact;
				$post->address_contact = !empty($input['address_contact']) ? $input['address_contact'] : $post->address_contact;
				$post->phone_number = !empty($input['phone_number']) ? $input['phone_number'] : $post->phone_number;
				$post->home_number = !empty($input['home_number']) ? $input['home_number'] : $post->home_number;

				$post->package = !empty($input['package']) ? $input['package'] : $post->package;
				$post->property_type = !empty($input['property_type']) ? $input['property_type'] : $post->property_type;
				$post->bedrooms = !empty($input['bedrooms']) ? $input['bedrooms'] : $post->bedrooms;
				$post->bathrooms = !empty($input['bathrooms']) ? $input['bathrooms'] : $post->bathrooms;
				$post->car_spaces = !empty($input['car_spaces']) ? $input['car_spaces'] : $post->car_spaces;
				$post->size = !empty($input['size']) ? $input['size'] : $post->size;
				$post->video_link = !empty($input['video_link']) ? $input['video_link'] : $post->video_link;
				
				$result = $this->postRepository->save($post);	

				if (!$result) return false;

				$this->postRepository->updateAttributeById($post->id, 'slug', $slug.'-'.$post->id);

				$post->tags()->detach(); //detach all tag of post
				if (!empty($input['tags'])) {
					$arrTags = $this->tagService->addTags($input['tags'], true);

					foreach ($arrTags as $key => $tag) {
						$post->tags()->attach($tag->id);
					}
				}

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}


	public function checkOwnerById($id)
	{
		$post = $this->postRepository->getById($id);

		if ($post->getCreatedBy() != Auth::user()->id) 
			return false;

		return true;
	}

	public function checkOwnerBySlug($slug)
	{
		$post = $this->postRepository->getByAttribute('slug', $slug);

		if ($post->getCreatedBy() != Auth::user()->id) 
			return false;

		return true;
	}



	public function changeStatus($id, $status)
	{
		if (!empty($id)) {
			$post = $this->postRepository->getById($id);

			$result = $this->postRepository->changeStatus($id, $status);

			if ($result) {
				$tags = $post->tags()->detach();
				Log::write('admin_post', '------------------POST CHANGE STATUS----------------------------');
				Log::write('admin_post', 'Deleted Post ID['.$post->id.']', ['admin' => Auth::user(), 'post' => $post, 'result' => $result]);
				Log::write('admin_post', 'Deleted all tag of Post ID['.$post->id.']', ['admin' => Auth::user(), 'tags' => $tags]);
				$deleteMedia = $this->mediaService->deleteByPostId($post->id);
				Log::write('admin_post', 'Deleted all media of Post ID['.$post->id.']', ['admin' => Auth::user(), 'deleteMedia' => $deleteMedia]);
				Log::write('admin_post', '----------------------------------------------------------------');

				return true;
			}
		}
		return false;
	}



}


?>
